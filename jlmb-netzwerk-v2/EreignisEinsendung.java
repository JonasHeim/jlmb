import java.util.ArrayList;

/**
 * Diese Klasse speichert Informationen über eine Einsendung für den Newsfeed eines sozialen Netzwerks.
 * Der Hauptteil der Einsendung besteht aus einer (möglicherweise mehrzeiligen) Textnachricht. 
 * Weitere Daten, wie Autor und Datum, werden ebenfalls gespeichert.
 * 
 * @author Michael Kölling und David J. Barnes
 * @version 0.2
 */
public class EreignisEinsendung extends Einsendung
{
    private String ereignis;   // eine beliebig lange, mehrzeilige Nachricht

    /**
     * Konstruktor für Objekte der Klasse EreignisEinsendung.
     * 
     * @param autor  der Benutzername des Einsenders
     * @param text   der Text dieser Einsendung
     */
    public EreignisEinsendung(String autor, String ereignis)
    {
        super(autor);
        this.ereignis = ereignis;
    }

    /**
     * Liefere den Text der Einsendung.
     * 
     * @return  der Nachrichtentext der Einsendung
     */
    public String gibText()
    {
        return ereignis;
    }
    
}
