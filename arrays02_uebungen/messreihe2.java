
/**
 * Beschreiben Sie hier die Klasse messreihe2.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class messreihe2
{
    // Objektvariablen deklarieren
    int anzahl = 45;
    double[] gewicht = new double[anzahl];

    /** Konstruktor fuer Objekte der Klasse Messreihe
     *  jede Messreihe enthaelt eine Reihe von positiven Messdaten (Gewichten)
     */
    public messreihe2() {
        for (int i=0; i<anzahl; i++) {    // Alle Gewichte 
            gewicht[i] = erzeugeZZahl();  // der Reihe nach festlegen
        }
    }

    /** das Element der Reihung mit dem Index i zurueckgeben
     *  Der gewuenschte Index i muss eingegeben werden
     *  Bei Eingabe eines nicht vorhandenen Index wird 
     *  -8.888 als Fehlersignal zurueckgemeldet   */
    public double gibGewicht(int i)   {
        if (i<0 || i>=anzahl) {              //<-- 4.
            return -8.888;   // als Fehlersignal! 
        }
        else {
            return gewicht[i]; 
        }
    } 

    /** Beschreiben Sie in Worten, was diese Methode 
     *  mit den Gewichten der Messreihe macht. */        //<-- 1.
    public void wasDenn()   {       
        for (int i = 8; i<15; i++)  {
            gewicht[i] = 77.7;
        }

        for (int i=20; i<27; i++)  {
            gewicht[i]   = 3*i;
            gewicht[i+1] = 4*i;
        }        
    }

    /** Beschreibe in Worten, was diese Methode 
     *  mit den Gewichten der Messreihe macht. */          //<-- 2.
    public void wasMacheIch(int ab)   {    
        if (ab<40 && ab>30) {
            for (int i=ab; i<=ab+5; i++)  {
                gewicht[i-20] = gewicht[i];
            }
        }
    }

    // ----------- Hilfsfunktionen
    /** dient zum Anzeigen der Reihung am Bildschirm;
     * kann durch INSPECT ersetzt werden */
    public void anzeigen() {
        for (int i=0; i< anzahl; i++) {
            System.out.println(formatiere(i)+" :  "+gewicht[i]); 
        }
    }    

    //------ interne Hilfsfunktionen
    /** interne Methode, um eine Zufallszahl im Bereich 200.0 - 799.999
     *  mit 3 Nachkommastellen zu erzeugen; 
     *  Math.random() liefert eine Zahl von 0 (inkl.) bis 1 (exkl.)  */
    private double erzeugeZZahl()   {
        double zufZahl = 200 + 600*Math.random();
        return Math.round((zufZahl*1000))/1000.0;
    }

    /** interne Hilfsfunktion zum stellenrichtigen 
     *  Ausdruck von ein- bis zweistelligen Zahlen. */
    private String formatiere(int i) {
        String erg = "Index";
        if (i<10) { 
            erg = "Index  " + i;   // Zwei Leerzeichen drin !!
        }
        else  { 
            erg = "Index " + i;    // hier nur eines !!
        }
        return erg;
    }

    /**
     * Eine Methode die in das Element mit Index 4 den Gewichtswert 44.4444 einträgt.
     * 
     */
    public void setzteAn4()
    {
        gewicht[4] = 44.4444;
    }

    /**
     * Eine Methode die die Summe der Gewichte der Elemente an den Indizes 16,17,18 zurückgibt.
     * 
     * @return         die Summe der Gewichte der Elemente an den Indizes 16,17,18
     */
    public double summeVon16und17und18()
    {
        return gewicht[16]+gewicht[17]+gewicht[18];
    }

    /**
     * Eine Methode die die Summe der Gewichte der letzten 33 Elemente zurückgibt.
     * 
     * @return         die Summe der Gewichte der Elemente an den letzten 33 Indizes
     */
    public double summeDerLetzten33Elemente()
    {
        double summe = 0;
        for (int i = anzahl-33; i < anzahl; i++){
            summe = summe + gewicht[i];
        }
        return summe;
    }

    /**
     * Eine Methode die in ein Element, dessen Index man vorgeben kann, den Gewichtswert 55.55555 einträgt.
     * 
     */
    public void setzteAnEinemIndex(int i)
    {
        gewicht[i] = 55.55555;
    }

    /**
     * Eine Methode die in ein Element, dessen Index man vorgeben kann, einen Gewichtswert einträgt, den man vorgeben kann.
     * 
     */
    public void setzteAnEinemIndex(int i, double gewichtswert)
    {
        gewicht[i] = gewichtswert;
    }

    /**
     * Eine Methode die in drei (17) aufeinanderfolgende Elemente, deren Startindex man vorgeben kann, Gewichtswerte eintraegt, die jeweils um 1.2 ansteigen.
     * 
     * @param  s    startindex
     */
    public void werteAnsteigen3mal(int s)
    {
        for(int i = s; i<=s+3; i++){
            gewicht[i] = gewicht[i] * 1.2;
        }
    }

    /**
     * Eine Methode die in drei (17) aufeinanderfolgende Elemente, deren Startindex man vorgeben kann, Gewichtswerte eintraegt, die jeweils um 1.2 ansteigen.
     * 
     * @param  s    startindex
     */
    public void werteAnsteigen17mal(int s)
    {
        if (s>=0 && s+17<anzahl){
            for(int i = s; i<=s+17; i++){
                gewicht[i] = gewicht[i] + 1.2;
            }
        }else{
            System.out.println("start index passt nicht");
        }
    }

    /**
     * Eine Methode die die Gewichtswerte zweier Elemente miteinander vertauscht.
     * 
     * @param  a    die variable, die vertauscht werden sollen
     * @param  b    die variable, die vertauscht werden sollen
     */
    public void werteVertauschen(int a, int b)
    {
        double halter = gewicht[a];
        gewicht[a] = gewicht[b];
        gewicht[b] = halter;
    }
    /**
     * Eine Methode die zwei benachbarte Elemente findet, die sich um nicht mehr als 3.3 unterscheiden.
     * 
     * @param  i	index des elements
     */
    public void benachbarteElemente3()
    {
        for(int i = 0; i < anzahl-1; i++){
            if (gewicht[i+1] <= gewicht[i]+3.3 && gewicht[i+1] >= gewicht[i]-3.3){
                System.out.println(i + " und " + (i+1));
            }
        }
    }
    public void benachbarteElementeGroeste()
    {
        int index = -2;
        double summe = 0;
        for(int i = 0; i < anzahl-1; i++){
            if (gewicht[i] + gewicht[i+1] > summe){
                summe = gewicht[i] + gewicht[i+1];
                index = i;
            }
        }
        System.out.println(index +" und " + (index+1) + " ergeben zusammen " + summe);
    }
    public void benachbarte4ElementeGroeste()
    {
        int index = -2;
        double summe = 0;
        for(int i = 0; i < anzahl-3; i++){
            if (gewicht[i] + gewicht[i+1] + gewicht[i+2] + gewicht[i+3] > summe){
                summe = gewicht[i] + gewicht[i+1] + gewicht[i+2] + gewicht[i+3];
                index = i;
            }
        }
        System.out.println(index +", " + (index+1) +", " + (index+2) + " und " + (index+3) +" ergeben zusammen " + summe);
    }
    public void benachbarte4ElementeWenigsterUnterschied()
    {
        double[] differenz = new double[anzahl-1];
        int index = -2;
        double summe = 100000;
        for(int i = 0; i < anzahl-1; i++){
            if (gewicht[i] < gewicht[i+1] ){
                differenz[i] = gewicht[i+1] -gewicht[i];
            }else{
                differenz[i] = gewicht[i] -gewicht[i+1];
            }
        }
        for(int i = 0; i < anzahl-4; i++){
            if (differenz[i] + differenz[i+1] + differenz[i+2] + differenz[i+3] < summe){
                summe = gewicht[i] + gewicht[i+1] + gewicht[i+2] + gewicht[i+3];
                index = i;
            }
        }
        System.out.println(index +", " + (index+1) +", " + (index+2) + " und " + (index+3));
    }

}
