
/**
 * Beschreiben Sie hier die Klasse zahlenreihe.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class zufallsreihe
{
    private int[] daten;
    int anzahl;

    public zufallsreihe(int anzahl)
    {
        this.anzahl = anzahl;
        daten = new int[anzahl];
        for (int i = 0; i < daten.length; i++)
        {
            // Für manche Aufgaben sollte man die 6 durch z.B. 1000 ersetzen
            daten[i] = getZufallszahl(6);
        }
    }

    public int aufgabe01Summe()
    {
        int summe = 0;
        for(int i = 0; i < anzahl; i++){
            summe = summe + daten[i];
        }
        return summe;
    }

    public int aufgabe02ZaehleNullen()
    {
        int anzahlDerNullen = 0;
        for(int i = 0; i < anzahl; i++){
            if(daten[i] == 0){
                anzahlDerNullen++;
            }
        }
        return anzahlDerNullen;
    }

    public int aufgabe03FindeLetzteNull()
    {
        int indexDerLetztenNull = -1;
        for(int i = 0; i < anzahl; i++){
            if(daten[i] == 0){
                indexDerLetztenNull = i;
            }
        }
        return indexDerLetztenNull;
    }

    public int aufgabe04FindeErsteNull()
    {
        for(int i = 0; i < anzahl; i++){
            if(daten[i] == 0){
                return i;
            }
        }
        return -1;
    }

    public boolean aufgabe05Enthaelt1()
    {
        for(int i = 0; i < anzahl; i++){
            if(daten[i] == 1){
                return true;
            }
        }
        return false;
    }

    public boolean aufgabe06Enthaelt2Und5()
    {
        boolean erhalten2 = false;
        boolean erhalten5 = false;
        for(int i = 0; i < anzahl; i++){
            if(daten[i] == 2){
                erhalten2 = true;
            }
            if(daten[i] == 5){
                erhalten5 = true;
            }
        }
        return erhalten2 && erhalten5;
    }

    public boolean aufgabe07EnthaeltFixpunkt()
    {
        for(int i = 0; i < anzahl; i++){
            if(daten[i] == i){
                return true;
            }
        }
        return false;
    }

    public int aufgabe08ZaehleWiederholungen()
    {
        int anzahlDerWiederholungen = 0;
        int vorherigeZahl = -1;
        for(int i = 0; i < anzahl; i++){
            if(daten[i] == vorherigeZahl){
                anzahlDerWiederholungen++;
            }
            vorherigeZahl = daten[i];
        }
        return anzahlDerWiederholungen;
    }

    public int aufgabe09ZaehleDreierWiederholungen()
    {
        int anzahlDer3erWiederholungen = 0;
        int vorherigeZahl = -1;
        int vorVorherigeZahl = -1;
        for(int i = 0; i < anzahl; i++){
            if(daten[i] == vorherigeZahl && daten[i] == vorVorherigeZahl){
                anzahlDer3erWiederholungen++;
                //System.out.println(i);
            }
            vorVorherigeZahl = vorherigeZahl;
            vorherigeZahl = daten[i];
        }
        return anzahlDer3erWiederholungen;
    }

    public int aufgabe10LaengsteSerie()
    {
        int laenglsteWiederholung = 0;
        int aktuelleWiederholung = 1;
        int startZahl = -1;
        for(int i = 0; i < anzahl; i++){
            if(daten[i] == startZahl){
                aktuelleWiederholung++;
                if(aktuelleWiederholung > laenglsteWiederholung){
                    laenglsteWiederholung = aktuelleWiederholung;
                }
            }else{
                startZahl = daten[i];
                aktuelleWiederholung = 1;
            }

        }
        return laenglsteWiederholung;
    }

    public int aufgabe11Zweitgroesste()
    {
        int groeste = -1;
        int zweitGroesste = -2;
        for(int i = 0; i < anzahl; i++){
            if(daten[i] > groeste){
                zweitGroesste = groeste;
                groeste = daten[i];
            }else if(daten[i] > zweitGroesste && daten[i] < groeste){
                zweitGroesste = daten[i];
            }

        }
        return zweitGroesste;
    }

    public void aufgabe12Plus1()
    {
        for(int i = 0; i < anzahl; i++){
            daten[i] = daten[i] +1;
        }
    }

    public void aufgabe13NullZuHundert()
    {
        for(int i = 0; i < anzahl; i++){
            if(daten[i] == 0){
                daten[i] = 100;
            }
        }
    }

    public void aufgabe14Rotation()
    {
        int halter = daten[0];
        for(int i = 0; i < anzahl-1; i++){
            daten[i] = daten[i+1];
        }
        daten[anzahl - 1] = halter;
    }

    public void aufgabe15Umdrehen()
    {
        int[] halter = new int[anzahl];
        for(int i = 0; i < anzahl; i++){
            halter[i] = daten[i];
        }
        for(int i = 0; i < anzahl; i++){
            daten[i] = halter[anzahl-1-i];
        }
    }

    /** dient zum Anzeigen der Reihung am Bildschirm;
     * kann durch INSPECT ersetzt werden */
    public void anzeigen() {
        for (int i=0; i< anzahl; i++) {
            System.out.println( i + " :  " + daten[i]); 
        }
    }    

    /**
     * Gibt eine Zufallszahl zwischen 0 und grenze-1 zurück.
     */
    private int getZufallszahl(int grenze)
    {
        return (int)(grenze*Math.random());
    }
}
