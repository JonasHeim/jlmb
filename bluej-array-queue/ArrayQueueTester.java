import org.junit.Before;

/**
 * Testklasse für die ArrayQueue
 * 
 * @author Rainer Helfrich
 * @version Oktober 2020
 */
public class ArrayQueueTester extends QueueTester
{
    @Before
    public void setUp()
    {
        theQueue = new ArrayQueue<Integer>();
    }
}
