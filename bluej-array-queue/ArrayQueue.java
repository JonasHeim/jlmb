
/**
 * Queue basierend auf einem Array
 * 
 * @author Rainer Helfrich
 * @version Oktober 2020
 */
public class ArrayQueue<T> extends Queue<T>
{
    private Object[] daten;
    private int first;
    private int last;
    /**
     * Erzeugt eine neue, leere Queue
     */
    public ArrayQueue()
    {
        daten = new Object[10];
        first = 0;
        last = 0;
        
    }

    /**
     * Fügt ein neues Element hinten in der Schlange ein
     * @param x Das neue Element
     */
    public void enqueue(T x)
    {
        if(first == last && !isEmpty()){
            Object[] daten2 = new Object[daten.length*2];
            int i = 0;
            do{
                daten2[i] = daten[first];
                i++;
                first++;
                first = first % daten.length;
            }while(first != last);
            first = 0;
            last = daten.length;
            daten = daten2;
        }
        daten[last] = x;
        last++;
        last = last%daten.length;
    }

    /**
     * Gibt das vorderste Element der Queue zurück (falls sie nicht leer ist)
     * @return Das vorderste Element
     */
    public T front()
    {
        return (T)daten[first];
    }

    /**
     * Entfernt das vorderste Element aus der Queue (falls sie nicht leer ist) und gibt es zurück
     * @return Das bisherige vorderste Element
     */
    public T dequeue()
    {
        T front = front();
        daten[first] = null;
        first++;
        first = first % daten.length;
        return front;
    }

    /**
     * Gibt zurück, ob die Queue leer ist
     * @return true, wenn die Queue leer ist; false sonst
     */
    public boolean isEmpty()
    {
        return front() == null;
    }
}
