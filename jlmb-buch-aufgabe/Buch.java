/**
 * Eine Klasse, deren Objekte Informationen �ber ein Buch halten.
 * Dies k�nnte Teil einer gr��eren Anwendung sein, einer
 * Bibliothekssoftware beispielsweise.
 *
 * @author (Ihren Namen hier eintragen.)
 * @version (das heutige Datum eintragen.)
 */
class Buch
{
    // Die Datenfelder
    private String autor;
    private String titel;
    private int seiten;
    private String refNummer;
    private int ausgeliehen = 0;
    private boolean kursText;

    /**
     * Setze den Autor und den Titel, wenn ein Objekt erzeugt wird.
     */
    public Buch(String buchautor, String buchtitel, int buchseiten, boolean fachbuch)
    {
        autor = buchautor;
        titel = buchtitel;
        seiten = buchseiten;
        kursText = fachbuch;
    }

    // weitere Methoden hier einf�gen ...
    /**
     * Die methode soll den Autor des buches geben
     * 
     * @return        autor des buches
     */
    public String gibAutor()
    {
        return autor;
    }

    /**
     * Die methode soll den Autor des buches geben
     * 
     * @return        titel des buches
     */
    public String gibTitel()
    {
        return titel;
    }

    /**
     * Die methode soll die seitenanzahl des buches geben
     * 
     * @return        seitenanzahl des buches
     */
    public int gibSeiten()
    {
        return seiten;
    }

    /**
     * Die methode soll die seitenanzahl des buches geben
     * 
     * @return        seitenanzahl des buches
     */
    public String gibRefNummer()
    {
        return refNummer;
    }

    /**
     * Die methode soll die seitenanzahl des buches geben
     * 
     * @return        seitenanzahl des buches
     */
    public int gibAusgeliehen()
    {
        return ausgeliehen;
    }
    /**
     * Die methode soll die seitenanzahl des buches geben
     * 
     * @return        seitenanzahl des buches
     */
    public boolean istKursText()
    {
        return kursText;
    }

    /**
     * der Autor soll ausgegeben werden
     * 
     * 
     */
    public void autorAusgeben()
    {
        System.out.println(autor);
    }

    /**
     * der tietel soll ausgegeben werden
     * 
     * 
     */
    public void titelAusgeben()
    {
        System.out.println(titel);
    }

    /**
     * der tietel soll ausgegeben werden
     * 
     * 
     */
    public void informationenAusgeben()
    {
        System.out.print("Titel: " + titel + ", Autor: " + autor + ", Seitenanzahl: " + seiten + ", Referenznummer: ");
        if (refNummer != null){
            System.out.print(refNummer);
        }else{
            System.out.print("ZZZ");
        }
        System.out.println(", Ausgeliehen: " + ausgeliehen);
    }

    /**
     * Eine Methode, die die Referenznummer des buches setzt
     * 
     * @param  y    die referenznummer, die dem buch gegeben werden soll
     * 
     */
    public void setzeRefNummer(String ref)
    {
        if (refNummer.length() >= 3){
            refNummer = ref;
        }else{
            System.out.println("Die Referenznummer muss mindestens 3 zeichen lang sein");
        }
    }
    /**
     * erh�hrt ausgeliehen
     * 
     * @return        (Beschreibung des R�ckgabewertes)
     */
    public void ausleihen()
    {
        ausgeliehen++;
    }

}
