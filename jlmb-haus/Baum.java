/**
 * Diese Klasse definiert ein einfaches Bild. Um das Bild auf
 * dem Bildschirm anzeigen zu lassen, muss die zeichne-Operation auf
 * einem Exemplar aufgerufen werden.
 * Aber hier steckt mehr drin: Da es eine elektronische Zeichnung ist,
 * kann sie ge�ndert werden. Man kann sie schwarz-wei� anzeigen lassen
 * und dann wieder in Farbe (nachdem sie gezeichnet wurde, ist ja klar).
 * 
 * Diese Klasse ist als fr�hes Java-Lehrbeispiel mit BlueJ gedacht.
 * 
 * @author  Michael K�lling und David J. Barnes
 * @version 2016.02.29
 */
public class Baum
{
    private Quadrat stamm;
    private Dreieck blaetter;
    private boolean gezeichnet;

    /**
     * Konstruktor f�r Objekte der Klasse Bild
     */
    public Baum()
    {
        stamm = new Quadrat();
        blaetter = new Dreieck();  
        gezeichnet = false;
        this.einrichten();
    }

    /**
     * Zeichne das Bild.
     */
    public void einrichten()
    {
        if(!gezeichnet) {
            stamm.horizontalBewegen(-100);
            stamm.vertikalBewegen(60);
            stamm.groesseAendern(40);
            stamm.sichtbarMachen();

            blaetter.groesseAendern(120, 180);
            blaetter.horizontalBewegen(20);
            blaetter.vertikalBewegen(-60);
            blaetter.sichtbarMachen();
            
            gezeichnet = true;
        }
    }

}
