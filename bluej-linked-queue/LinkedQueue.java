
/**
 * Eine Queue basierend auf einer verketteten Liste
 * 
 * @author Rainer Helfrich
 * @version Oktober 2020
 */
public class LinkedQueue<T> extends Queue<T>
{
    private Node<T> head;
    private Node<T> tail;
    /**
     * Erzeugt eine neue, leere Queue
     */
    public LinkedQueue()
    {
        head = null;
        tail = null;
    }
    
    /**
     * Gibt das vorderste Element der Queue zurück (falls sie nicht leer ist)
     * @return Das vorderste Element
     */
    public T front()
    {
        if(isEmpty()){
            return null;
        }else{
           return head.getContent();
        }
    }
    
    /**
     * Entfernt das vorderste Element aus der Queue (falls sie nicht leer ist) und gibt es zurück
     * @return Das bisherige vorderste Element
     */
    public T dequeue()
    {
        T ausgabe = front();
        if (head != tail){
            head = head.getNext();
        }else{
            head = null;
            tail = null;
        }
        return ausgabe;
    }
    
    /**
     * Fügt ein neues Element hinten in der Schlange ein
     * @param x Das neue Element
     */
    public void enqueue(T x)
    {
        Node<T> neu = new Node(x, null);
        if(isEmpty()){
            head = neu;
            tail = neu;
        }else{
            tail.setNext(neu);
            tail = neu;
        }
    }
    
    /**
     * Gibt zurück, ob die Queue leer ist
     * @return true, wenn die Queue leer ist; false sonst
     */
    public boolean isEmpty()
    {
        return head==(null);
    }
}
