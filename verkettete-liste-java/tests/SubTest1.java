package tests;
import static org.junit.Assert.*;
import liste.*;
/**
 * Die Test-Klasse SubTest1.
 *
 * @author  Rainer Helfrich
 * @version 03.10.2020
 */
public class SubTest1
{

    public static void test1Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(719);
        Tester.verifyListContents(theTestList, new int[]{719});

        assertFalse("Fehler: Element 84 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(84));
        theTestList.append(320);
        Tester.verifyListContents(theTestList, new int[]{719,320});

        assertTrue("Fehler: Element 719 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(719));
        assertTrue("Fehler: Element 719 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(719));
        assertFalse("Fehler: Element 375 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(375));
        assertFalse("Fehler: Element 691 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(691));
        theTestList.append(112);
        Tester.verifyListContents(theTestList, new int[]{719,320,112});

        theTestList.append(646);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646});

        theTestList.append(608);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608});

        assertFalse("Fehler: Element 408 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(408));
        theTestList.append(718);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718});

        theTestList.append(726);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726});

        assertFalse("Fehler: Element 267 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(267));
        assertTrue("Fehler: Element 112 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(112));
        theTestList.append(241);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241});

        assertTrue("Fehler: Element 646 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(646));
        theTestList.append(536);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536});

        theTestList.append(297);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297});

        assertFalse("Fehler: Element 21 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(21));
        theTestList.append(290);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290});

        theTestList.append(534);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534});

        assertFalse("Fehler: Element 4 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(4));
        assertTrue("Fehler: Element 718 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(718));
        assertFalse("Fehler: Element 710 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(710));
        theTestList.append(671);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671});

        theTestList.append(871);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871});

        theTestList.append(986);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986});

        theTestList.append(958);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958});

        assertTrue("Fehler: Element 534 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(534));
        assertTrue("Fehler: Element 646 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(646));
        theTestList.append(98);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98});

        assertTrue("Fehler: Element 98 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(98));
        assertTrue("Fehler: Element 320 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(320));
        assertTrue("Fehler: Element 671 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(671));
        assertTrue("Fehler: Element 719 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(719));
        assertTrue("Fehler: Element 112 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(112));
        assertTrue("Fehler: Element 608 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(608));
        assertTrue("Fehler: Element 986 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(986));
        assertTrue("Fehler: Element 608 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(608));
        theTestList.append(102);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102});

        theTestList.append(19);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19});

        assertTrue("Fehler: Element 986 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(986));
        theTestList.append(126);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126});

        theTestList.append(771);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771});

        assertFalse("Fehler: Element 5 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(5));
        theTestList.append(780);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780});

        theTestList.append(226);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226});

        assertTrue("Fehler: Element 290 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(290));
        assertFalse("Fehler: Element 8 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(8));
        theTestList.append(936);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936});

        assertFalse("Fehler: Element 357 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(357));
        assertTrue("Fehler: Element 241 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(241));
        theTestList.append(986);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986});

        theTestList.append(335);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335});

        theTestList.append(40);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40});

        theTestList.append(441);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441});

        theTestList.append(254);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254});

        assertFalse("Fehler: Element 580 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(580));
        theTestList.append(526);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526});

        theTestList.append(820);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820});

        theTestList.append(528);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528});

        assertFalse("Fehler: Element 62 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(62));
        assertTrue("Fehler: Element 986 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(986));
        theTestList.append(340);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340});

        theTestList.append(731);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340,731});

        theTestList.append(704);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340,731,704});

        assertFalse("Fehler: Element 242 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(242));
        assertFalse("Fehler: Element 70 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(70));
        assertTrue("Fehler: Element 526 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(526));
        theTestList.append(967);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340,731,704,967});

        assertTrue("Fehler: Element 731 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(731));
        assertFalse("Fehler: Element 229 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(229));
        assertTrue("Fehler: Element 936 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(936));
        assertTrue("Fehler: Element 241 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(241));
        assertFalse("Fehler: Element 764 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(764));
        theTestList.append(83);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340,731,704,967,83});

        assertTrue("Fehler: Element 646 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(646));
        assertFalse("Fehler: Element 544 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(544));
        theTestList.append(308);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340,731,704,967,83,308});

        assertTrue("Fehler: Element 528 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(528));
        theTestList.append(254);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340,731,704,967,83,308,254});

        assertFalse("Fehler: Element 464 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(464));
        assertFalse("Fehler: Element 468 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(468));
        assertTrue("Fehler: Element 986 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(986));
        theTestList.append(794);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340,731,704,967,83,308,254,794});

        theTestList.append(217);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340,731,704,967,83,308,254,794,217});

        assertFalse("Fehler: Element 333 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(333));
        theTestList.append(210);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340,731,704,967,83,308,254,794,217,210});

        theTestList.append(65);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340,731,704,967,83,308,254,794,217,210,65});

        theTestList.append(415);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340,731,704,967,83,308,254,794,217,210,65,415});

        theTestList.append(793);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340,731,704,967,83,308,254,794,217,210,65,415,793});

        assertFalse("Fehler: Element 883 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(883));
        assertFalse("Fehler: Element 463 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(463));
        theTestList.append(551);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340,731,704,967,83,308,254,794,217,210,65,415,793,551});

        assertTrue("Fehler: Element 958 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(958));
        theTestList.append(368);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340,731,704,967,83,308,254,794,217,210,65,415,793,551,368});

        theTestList.append(466);
        Tester.verifyListContents(theTestList, new int[]{719,320,112,646,608,718,726,241,536,297,290,534,671,871,986,958,98,102,19,126,771,780,226,936,986,335,40,441,254,526,820,528,340,731,704,967,83,308,254,794,217,210,65,415,793,551,368,466});

        assertTrue("Fehler: Element 780 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(780));
        assertFalse("Fehler: Element 850 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(850));
    }

    public static void test2Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(642);
        Tester.verifyListContents(theTestList, new int[]{642});

        assertFalse("Fehler: Element 158 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(158));
        theTestList.append(879);
        Tester.verifyListContents(theTestList, new int[]{642,879});

        theTestList.append(770);
        Tester.verifyListContents(theTestList, new int[]{642,879,770});

        theTestList.append(181);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181});

        assertFalse("Fehler: Element 566 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(566));
        assertTrue("Fehler: Element 181 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(181));
        assertFalse("Fehler: Element 70 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(70));
        theTestList.append(25);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25});

        theTestList.append(811);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811});

        assertFalse("Fehler: Element 179 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(179));
        assertFalse("Fehler: Element 230 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(230));
        theTestList.append(857);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857});

        theTestList.append(366);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366});

        theTestList.append(709);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709});

        theTestList.append(290);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290});

        theTestList.append(361);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361});

        theTestList.append(676);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676});

        theTestList.append(285);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285});

        theTestList.append(748);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748});

        theTestList.append(203);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203});

        assertFalse("Fehler: Element 683 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(683));
        theTestList.append(170);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170});

        theTestList.append(47);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47});

        theTestList.append(11);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11});

        theTestList.append(483);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483});

        theTestList.append(345);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345});

        theTestList.append(896);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896});

        theTestList.append(602);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602});

        theTestList.append(467);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467});

        assertFalse("Fehler: Element 609 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(609));
        theTestList.append(531);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531});

        theTestList.append(254);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254});

        assertFalse("Fehler: Element 696 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(696));
        assertTrue("Fehler: Element 602 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(602));
        theTestList.append(26);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26});

        assertTrue("Fehler: Element 483 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(483));
        assertTrue("Fehler: Element 290 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(290));
        theTestList.append(123);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123});

        theTestList.append(0);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0});

        theTestList.append(586);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586});

        assertFalse("Fehler: Element 487 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(487));
        assertFalse("Fehler: Element 643 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(643));
        theTestList.append(558);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558});

        theTestList.append(132);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132});

        theTestList.append(322);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322});

        assertFalse("Fehler: Element 671 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(671));
        theTestList.append(473);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473});

        theTestList.append(275);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275});

        assertTrue("Fehler: Element 709 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(709));
        theTestList.append(65);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65});

        assertFalse("Fehler: Element 427 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(427));
        assertTrue("Fehler: Element 25 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(25));
        assertTrue("Fehler: Element 366 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(366));
        theTestList.append(124);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124});

        theTestList.append(640);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640});

        theTestList.append(704);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704});

        theTestList.append(509);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509});

        theTestList.append(489);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489});

        theTestList.append(329);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329});

        assertTrue("Fehler: Element 47 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(47));
        theTestList.append(521);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521});

        theTestList.append(626);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626});

        assertTrue("Fehler: Element 586 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(586));
        theTestList.append(691);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691});

        assertTrue("Fehler: Element 285 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(285));
        theTestList.append(747);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747});

        theTestList.append(861);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861});

        theTestList.append(353);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353});

        assertTrue("Fehler: Element 65 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(65));
        theTestList.append(32);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32});

        theTestList.append(402);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32,402});

        theTestList.append(912);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32,402,912});

        theTestList.append(590);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32,402,912,590});

        theTestList.append(644);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32,402,912,590,644});

        theTestList.append(13);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32,402,912,590,644,13});

        assertFalse("Fehler: Element 617 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(617));
        assertTrue("Fehler: Element 47 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(47));
        theTestList.append(263);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32,402,912,590,644,13,263});

        theTestList.append(480);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32,402,912,590,644,13,263,480});

        assertFalse("Fehler: Element 844 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(844));
        theTestList.append(969);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32,402,912,590,644,13,263,480,969});

        theTestList.append(31);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32,402,912,590,644,13,263,480,969,31});

        theTestList.append(183);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32,402,912,590,644,13,263,480,969,31,183});

        assertFalse("Fehler: Element 936 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(936));
        theTestList.append(949);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32,402,912,590,644,13,263,480,969,31,183,949});

        theTestList.append(359);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32,402,912,590,644,13,263,480,969,31,183,949,359});

        theTestList.append(42);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32,402,912,590,644,13,263,480,969,31,183,949,359,42});

        assertFalse("Fehler: Element 100 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(100));
        assertTrue("Fehler: Element 770 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(770));
        theTestList.append(601);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32,402,912,590,644,13,263,480,969,31,183,949,359,42,601});

        assertTrue("Fehler: Element 183 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(183));
        assertTrue("Fehler: Element 31 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(31));
        assertFalse("Fehler: Element 177 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(177));
        assertTrue("Fehler: Element 181 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(181));
        assertTrue("Fehler: Element 263 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(263));
        assertTrue("Fehler: Element 345 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(345));
        assertTrue("Fehler: Element 709 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(709));
        assertTrue("Fehler: Element 47 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(47));
        theTestList.append(239);
        Tester.verifyListContents(theTestList, new int[]{642,879,770,181,25,811,857,366,709,290,361,676,285,748,203,170,47,11,483,345,896,602,467,531,254,26,123,0,586,558,132,322,473,275,65,124,640,704,509,489,329,521,626,691,747,861,353,32,402,912,590,644,13,263,480,969,31,183,949,359,42,601,239});

    }

    public static void test3Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(877);
        Tester.verifyListContents(theTestList, new int[]{877});

        theTestList.append(934);
        Tester.verifyListContents(theTestList, new int[]{877,934});

        theTestList.append(897);
        Tester.verifyListContents(theTestList, new int[]{877,934,897});

        theTestList.append(687);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687});

        assertTrue("Fehler: Element 897 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(897));
        theTestList.append(96);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96});

        assertFalse("Fehler: Element 716 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(716));
        theTestList.append(936);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936});

        theTestList.append(531);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531});

        theTestList.append(405);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405});

        theTestList.append(357);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357});

        assertTrue("Fehler: Element 936 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(936));
        assertFalse("Fehler: Element 198 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(198));
        theTestList.append(481);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481});

        theTestList.append(191);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191});

        assertFalse("Fehler: Element 160 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(160));
        theTestList.append(543);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543});

        theTestList.append(706);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706});

        theTestList.append(3);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3});

        theTestList.append(111);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111});

        assertFalse("Fehler: Element 450 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(450));
        assertTrue("Fehler: Element 897 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(897));
        assertFalse("Fehler: Element 319 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(319));
        theTestList.append(844);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844});

        assertFalse("Fehler: Element 62 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(62));
        assertFalse("Fehler: Element 451 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(451));
        theTestList.append(882);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882});

        theTestList.append(847);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847});

        theTestList.append(542);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542});

        theTestList.append(515);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515});

        assertTrue("Fehler: Element 191 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(191));
        assertFalse("Fehler: Element 351 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(351));
        theTestList.append(336);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336});

        assertFalse("Fehler: Element 857 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(857));
        theTestList.append(932);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932});

        assertTrue("Fehler: Element 934 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(934));
        theTestList.append(886);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886});

        theTestList.append(660);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660});

        assertFalse("Fehler: Element 672 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(672));
        theTestList.append(15);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15});

        assertTrue("Fehler: Element 543 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(543));
        theTestList.append(672);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672});

        theTestList.append(525);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525});

        theTestList.append(933);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933});

        theTestList.append(65);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65});

        theTestList.append(620);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620});

        assertTrue("Fehler: Element 531 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(531));
        assertFalse("Fehler: Element 816 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(816));
        assertTrue("Fehler: Element 882 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(882));
        theTestList.append(401);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401});

        theTestList.append(392);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392});

        assertTrue("Fehler: Element 886 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(886));
        theTestList.append(41);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41});

        theTestList.append(314);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314});

        assertTrue("Fehler: Element 687 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(687));
        assertFalse("Fehler: Element 347 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(347));
        theTestList.append(619);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619});

        theTestList.append(900);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900});

        theTestList.append(794);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794});

        theTestList.append(896);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896});

        theTestList.append(480);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480});

        theTestList.append(39);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39});

        theTestList.append(490);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490});

        assertFalse("Fehler: Element 831 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(831));
        theTestList.append(157);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157});

        assertTrue("Fehler: Element 314 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(314));
        assertTrue("Fehler: Element 886 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(886));
        assertTrue("Fehler: Element 882 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(882));
        assertFalse("Fehler: Element 858 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(858));
        theTestList.append(48);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48});

        theTestList.append(359);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48,359});

        assertFalse("Fehler: Element 54 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(54));
        theTestList.append(496);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48,359,496});

        assertFalse("Fehler: Element 298 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(298));
        theTestList.append(231);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48,359,496,231});

        assertFalse("Fehler: Element 759 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(759));
        assertTrue("Fehler: Element 934 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(934));
        assertFalse("Fehler: Element 369 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(369));
        assertFalse("Fehler: Element 621 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(621));
        assertTrue("Fehler: Element 41 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(41));
        theTestList.append(116);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48,359,496,231,116});

        assertFalse("Fehler: Element 67 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(67));
        theTestList.append(108);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48,359,496,231,116,108});

        theTestList.append(811);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48,359,496,231,116,108,811});

        assertTrue("Fehler: Element 620 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(620));
        assertFalse("Fehler: Element 344 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(344));
        assertFalse("Fehler: Element 151 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(151));
        assertTrue("Fehler: Element 897 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(897));
        assertTrue("Fehler: Element 706 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(706));
        theTestList.append(662);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48,359,496,231,116,108,811,662});

        theTestList.append(929);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48,359,496,231,116,108,811,662,929});

        assertFalse("Fehler: Element 234 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(234));
        theTestList.append(686);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48,359,496,231,116,108,811,662,929,686});

        theTestList.append(657);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48,359,496,231,116,108,811,662,929,686,657});

        assertTrue("Fehler: Element 934 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(934));
        theTestList.append(905);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48,359,496,231,116,108,811,662,929,686,657,905});

        theTestList.append(172);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48,359,496,231,116,108,811,662,929,686,657,905,172});

        theTestList.append(25);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48,359,496,231,116,108,811,662,929,686,657,905,172,25});

        theTestList.append(45);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48,359,496,231,116,108,811,662,929,686,657,905,172,25,45});

        theTestList.append(372);
        Tester.verifyListContents(theTestList, new int[]{877,934,897,687,96,936,531,405,357,481,191,543,706,3,111,844,882,847,542,515,336,932,886,660,15,672,525,933,65,620,401,392,41,314,619,900,794,896,480,39,490,157,48,359,496,231,116,108,811,662,929,686,657,905,172,25,45,372});

    }

    public static void test4Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(312);
        Tester.verifyListContents(theTestList, new int[]{312});

        assertTrue("Fehler: Element 312 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(312));
        theTestList.append(725);
        Tester.verifyListContents(theTestList, new int[]{312,725});

        theTestList.append(637);
        Tester.verifyListContents(theTestList, new int[]{312,725,637});

        theTestList.append(576);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576});

        assertTrue("Fehler: Element 312 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(312));
        assertFalse("Fehler: Element 63 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(63));
        assertFalse("Fehler: Element 682 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(682));
        assertFalse("Fehler: Element 97 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(97));
        theTestList.append(361);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361});

        assertTrue("Fehler: Element 361 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(361));
        assertTrue("Fehler: Element 725 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(725));
        theTestList.append(387);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387});

        theTestList.append(66);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66});

        theTestList.append(727);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727});

        theTestList.append(633);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633});

        assertFalse("Fehler: Element 112 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(112));
        theTestList.append(527);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527});

        assertTrue("Fehler: Element 725 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(725));
        theTestList.append(361);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361});

        theTestList.append(890);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890});

        assertFalse("Fehler: Element 946 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(946));
        theTestList.append(742);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742});

        assertFalse("Fehler: Element 228 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(228));
        assertTrue("Fehler: Element 725 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(725));
        theTestList.append(816);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816});

        theTestList.append(142);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142});

        theTestList.append(349);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349});

        assertFalse("Fehler: Element 909 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(909));
        assertTrue("Fehler: Element 142 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(142));
        assertTrue("Fehler: Element 742 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(742));
        assertTrue("Fehler: Element 312 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(312));
        theTestList.append(366);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366});

        assertTrue("Fehler: Element 142 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(142));
        theTestList.append(675);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675});

        theTestList.append(114);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114});

        assertFalse("Fehler: Element 154 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(154));
        assertTrue("Fehler: Element 142 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(142));
        theTestList.append(686);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686});

        assertFalse("Fehler: Element 724 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(724));
        assertFalse("Fehler: Element 778 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(778));
        assertFalse("Fehler: Element 152 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(152));
        theTestList.append(474);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474});

        theTestList.append(220);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220});

        theTestList.append(28);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28});

        theTestList.append(738);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738});

        theTestList.append(987);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987});

        assertFalse("Fehler: Element 644 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(644));
        assertFalse("Fehler: Element 496 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(496));
        assertTrue("Fehler: Element 637 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(637));
        assertTrue("Fehler: Element 987 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(987));
        theTestList.append(650);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650});

        assertTrue("Fehler: Element 650 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(650));
        assertTrue("Fehler: Element 349 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(349));
        theTestList.append(436);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436});

        theTestList.append(71);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71});

        assertFalse("Fehler: Element 333 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(333));
        assertFalse("Fehler: Element 563 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(563));
        theTestList.append(259);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259});

        theTestList.append(850);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850});

        assertFalse("Fehler: Element 984 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(984));
        assertTrue("Fehler: Element 987 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(987));
        assertTrue("Fehler: Element 725 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(725));
        assertFalse("Fehler: Element 634 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(634));
        theTestList.append(993);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993});

        assertFalse("Fehler: Element 377 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(377));
        theTestList.append(712);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712});

        assertFalse("Fehler: Element 130 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(130));
        theTestList.append(226);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712,226});

        assertTrue("Fehler: Element 675 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(675));
        assertTrue("Fehler: Element 650 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(650));
        assertTrue("Fehler: Element 142 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(142));
        theTestList.append(890);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712,226,890});

        theTestList.append(401);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712,226,890,401});

        theTestList.append(355);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712,226,890,401,355});

        assertTrue("Fehler: Element 142 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(142));
        theTestList.append(191);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712,226,890,401,355,191});

        assertFalse("Fehler: Element 186 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(186));
        assertTrue("Fehler: Element 637 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(637));
        theTestList.append(2);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712,226,890,401,355,191,2});

        theTestList.append(88);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712,226,890,401,355,191,2,88});

        assertTrue("Fehler: Element 738 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(738));
        assertFalse("Fehler: Element 614 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(614));
        assertTrue("Fehler: Element 361 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(361));
        assertFalse("Fehler: Element 58 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(58));
        theTestList.append(699);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712,226,890,401,355,191,2,88,699});

        assertFalse("Fehler: Element 813 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(813));
        theTestList.append(117);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712,226,890,401,355,191,2,88,699,117});

        assertTrue("Fehler: Element 71 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(71));
        assertTrue("Fehler: Element 675 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(675));
        assertTrue("Fehler: Element 637 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(637));
        assertTrue("Fehler: Element 114 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(114));
        assertFalse("Fehler: Element 40 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(40));
        theTestList.append(869);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712,226,890,401,355,191,2,88,699,117,869});

        theTestList.append(590);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712,226,890,401,355,191,2,88,699,117,869,590});

        theTestList.append(161);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712,226,890,401,355,191,2,88,699,117,869,590,161});

        theTestList.append(740);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712,226,890,401,355,191,2,88,699,117,869,590,161,740});

        assertFalse("Fehler: Element 610 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(610));
        theTestList.append(344);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712,226,890,401,355,191,2,88,699,117,869,590,161,740,344});

        theTestList.append(381);
        Tester.verifyListContents(theTestList, new int[]{312,725,637,576,361,387,66,727,633,527,361,890,742,816,142,349,366,675,114,686,474,220,28,738,987,650,436,71,259,850,993,712,226,890,401,355,191,2,88,699,117,869,590,161,740,344,381});

    }

    public static void test5Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(317);
        Tester.verifyListContents(theTestList, new int[]{317});

        assertTrue("Fehler: Element 317 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(317));
        theTestList.append(256);
        Tester.verifyListContents(theTestList, new int[]{317,256});

        assertFalse("Fehler: Element 698 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(698));
        theTestList.append(255);
        Tester.verifyListContents(theTestList, new int[]{317,256,255});

        assertFalse("Fehler: Element 131 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(131));
        assertFalse("Fehler: Element 109 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(109));
        theTestList.append(3);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3});

        theTestList.append(229);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229});

        theTestList.append(764);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764});

        theTestList.append(67);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67});

        theTestList.append(231);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231});

        assertTrue("Fehler: Element 764 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(764));
        theTestList.append(312);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312});

        theTestList.append(517);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517});

        theTestList.append(969);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969});

        assertFalse("Fehler: Element 164 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(164));
        theTestList.append(621);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621});

        assertFalse("Fehler: Element 267 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(267));
        theTestList.append(688);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688});

        assertFalse("Fehler: Element 529 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(529));
        assertFalse("Fehler: Element 369 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(369));
        assertFalse("Fehler: Element 749 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(749));
        theTestList.append(305);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305});

        assertFalse("Fehler: Element 325 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(325));
        assertTrue("Fehler: Element 517 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(517));
        theTestList.append(656);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656});

        theTestList.append(279);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279});

        assertFalse("Fehler: Element 400 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(400));
        assertTrue("Fehler: Element 305 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(305));
        assertTrue("Fehler: Element 256 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(256));
        assertFalse("Fehler: Element 958 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(958));
        assertFalse("Fehler: Element 88 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(88));
        assertFalse("Fehler: Element 922 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(922));
        theTestList.append(760);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760});

        assertFalse("Fehler: Element 978 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(978));
        theTestList.append(286);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286});

        assertFalse("Fehler: Element 185 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(185));
        theTestList.append(901);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901});

        theTestList.append(780);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780});

        assertFalse("Fehler: Element 364 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(364));
        assertTrue("Fehler: Element 286 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(286));
        theTestList.append(949);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949});

        theTestList.append(629);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629});

        theTestList.append(0);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0});

        assertFalse("Fehler: Element 750 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(750));
        assertFalse("Fehler: Element 156 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(156));
        theTestList.append(178);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178});

        theTestList.append(487);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487});

        assertTrue("Fehler: Element 656 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(656));
        theTestList.append(225);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225});

        assertFalse("Fehler: Element 306 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(306));
        assertTrue("Fehler: Element 0 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(0));
        assertFalse("Fehler: Element 876 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(876));
        assertFalse("Fehler: Element 432 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(432));
        theTestList.append(736);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736});

        theTestList.append(59);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59});

        theTestList.append(427);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427});

        theTestList.append(327);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327});

        assertTrue("Fehler: Element 279 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(279));
        assertFalse("Fehler: Element 676 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(676));
        assertFalse("Fehler: Element 701 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(701));
        theTestList.append(388);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388});

        theTestList.append(9);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9});

        assertTrue("Fehler: Element 312 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(312));
        assertTrue("Fehler: Element 225 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(225));
        theTestList.append(521);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521});

        theTestList.append(206);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206});

        assertFalse("Fehler: Element 565 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(565));
        theTestList.append(411);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411});

        assertTrue("Fehler: Element 621 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(621));
        theTestList.append(156);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411,156});

        assertTrue("Fehler: Element 231 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(231));
        theTestList.append(610);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411,156,610});

        assertTrue("Fehler: Element 688 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(688));
        assertFalse("Fehler: Element 881 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(881));
        theTestList.append(767);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411,156,610,767});

        assertTrue("Fehler: Element 764 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(764));
        theTestList.append(592);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411,156,610,767,592});

        assertFalse("Fehler: Element 197 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(197));
        theTestList.append(95);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411,156,610,767,592,95});

        assertTrue("Fehler: Element 3 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(3));
        theTestList.append(636);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411,156,610,767,592,95,636});

        theTestList.append(264);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411,156,610,767,592,95,636,264});

        assertTrue("Fehler: Element 156 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(156));
        theTestList.append(914);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411,156,610,767,592,95,636,264,914});

        theTestList.append(411);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411,156,610,767,592,95,636,264,914,411});

        theTestList.append(852);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411,156,610,767,592,95,636,264,914,411,852});

        assertTrue("Fehler: Element 388 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(388));
        assertFalse("Fehler: Element 759 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(759));
        theTestList.append(653);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411,156,610,767,592,95,636,264,914,411,852,653});

        theTestList.append(224);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411,156,610,767,592,95,636,264,914,411,852,653,224});

        assertTrue("Fehler: Element 0 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(0));
        assertTrue("Fehler: Element 231 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(231));
        theTestList.append(398);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411,156,610,767,592,95,636,264,914,411,852,653,224,398});

        assertFalse("Fehler: Element 729 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(729));
        theTestList.append(754);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411,156,610,767,592,95,636,264,914,411,852,653,224,398,754});

        assertTrue("Fehler: Element 95 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(95));
        assertTrue("Fehler: Element 901 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(901));
        theTestList.append(237);
        Tester.verifyListContents(theTestList, new int[]{317,256,255,3,229,764,67,231,312,517,969,621,688,305,656,279,760,286,901,780,949,629,0,178,487,225,736,59,427,327,388,9,521,206,411,156,610,767,592,95,636,264,914,411,852,653,224,398,754,237});

    }

    public static void test6Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(800);
        Tester.verifyListContents(theTestList, new int[]{800});

        theTestList.append(437);
        Tester.verifyListContents(theTestList, new int[]{800,437});

        theTestList.append(254);
        Tester.verifyListContents(theTestList, new int[]{800,437,254});

        assertFalse("Fehler: Element 255 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(255));
        assertTrue("Fehler: Element 254 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(254));
        theTestList.append(638);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638});

        assertTrue("Fehler: Element 800 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(800));
        theTestList.append(189);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189});

        assertFalse("Fehler: Element 426 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(426));
        theTestList.append(571);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571});

        assertFalse("Fehler: Element 339 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(339));
        assertTrue("Fehler: Element 437 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(437));
        theTestList.append(975);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975});

        theTestList.append(465);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465});

        theTestList.append(413);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413});

        assertTrue("Fehler: Element 800 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(800));
        theTestList.append(116);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116});

        theTestList.append(741);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741});

        theTestList.append(870);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870});

        theTestList.append(323);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323});

        assertTrue("Fehler: Element 741 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(741));
        theTestList.append(859);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859});

        theTestList.append(847);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847});

        assertFalse("Fehler: Element 941 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(941));
        theTestList.append(759);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759});

        assertFalse("Fehler: Element 914 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(914));
        assertTrue("Fehler: Element 437 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(437));
        theTestList.append(218);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218});

        theTestList.append(189);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189});

        assertTrue("Fehler: Element 218 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(218));
        theTestList.append(26);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26});

        theTestList.append(862);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862});

        theTestList.append(540);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540});

        theTestList.append(580);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580});

        assertTrue("Fehler: Element 741 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(741));
        assertTrue("Fehler: Element 465 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(465));
        assertFalse("Fehler: Element 813 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(813));
        assertFalse("Fehler: Element 103 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(103));
        theTestList.append(370);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370});

        assertTrue("Fehler: Element 254 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(254));
        theTestList.append(293);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293});

        assertFalse("Fehler: Element 618 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(618));
        assertFalse("Fehler: Element 820 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(820));
        theTestList.append(291);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291});

        assertTrue("Fehler: Element 218 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(218));
        theTestList.append(786);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786});

        theTestList.append(536);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536});

        theTestList.append(824);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824});

        assertFalse("Fehler: Element 215 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(215));
        assertFalse("Fehler: Element 283 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(283));
        theTestList.append(622);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622});

        assertFalse("Fehler: Element 22 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(22));
        assertFalse("Fehler: Element 954 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(954));
        theTestList.append(91);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91});

        theTestList.append(422);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422});

        theTestList.append(301);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301});

        assertFalse("Fehler: Element 614 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(614));
        assertTrue("Fehler: Element 824 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(824));
        theTestList.append(636);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636});

        assertTrue("Fehler: Element 847 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(847));
        assertTrue("Fehler: Element 26 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(26));
        assertFalse("Fehler: Element 24 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(24));
        theTestList.append(661);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661});

        theTestList.append(637);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637});

        theTestList.append(700);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700});

        theTestList.append(651);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651});

        assertFalse("Fehler: Element 239 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(239));
        theTestList.append(22);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22});

        theTestList.append(105);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22,105});

        theTestList.append(299);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22,105,299});

        theTestList.append(714);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22,105,299,714});

        theTestList.append(8);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22,105,299,714,8});

        assertTrue("Fehler: Element 580 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(580));
        assertTrue("Fehler: Element 323 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(323));
        theTestList.append(154);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22,105,299,714,8,154});

        assertTrue("Fehler: Element 862 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(862));
        theTestList.append(749);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22,105,299,714,8,154,749});

        assertTrue("Fehler: Element 465 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(465));
        theTestList.append(440);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22,105,299,714,8,154,749,440});

        theTestList.append(923);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22,105,299,714,8,154,749,440,923});

        assertTrue("Fehler: Element 700 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(700));
        assertTrue("Fehler: Element 800 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(800));
        theTestList.append(603);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22,105,299,714,8,154,749,440,923,603});

        theTestList.append(252);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22,105,299,714,8,154,749,440,923,603,252});

        assertFalse("Fehler: Element 488 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(488));
        theTestList.append(869);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22,105,299,714,8,154,749,440,923,603,252,869});

        assertFalse("Fehler: Element 208 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(208));
        assertTrue("Fehler: Element 749 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(749));
        assertFalse("Fehler: Element 451 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(451));
        assertTrue("Fehler: Element 154 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(154));
        theTestList.append(158);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22,105,299,714,8,154,749,440,923,603,252,869,158});

        assertFalse("Fehler: Element 978 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(978));
        assertFalse("Fehler: Element 416 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(416));
        theTestList.append(459);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22,105,299,714,8,154,749,440,923,603,252,869,158,459});

        assertFalse("Fehler: Element 856 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(856));
        theTestList.append(58);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22,105,299,714,8,154,749,440,923,603,252,869,158,459,58});

        theTestList.append(230);
        Tester.verifyListContents(theTestList, new int[]{800,437,254,638,189,571,975,465,413,116,741,870,323,859,847,759,218,189,26,862,540,580,370,293,291,786,536,824,622,91,422,301,636,661,637,700,651,22,105,299,714,8,154,749,440,923,603,252,869,158,459,58,230});

        assertFalse("Fehler: Element 168 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(168));
        assertTrue("Fehler: Element 540 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(540));
        assertTrue("Fehler: Element 536 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(536));
    }

    public static void test7Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(16);
        Tester.verifyListContents(theTestList, new int[]{16});

        assertFalse("Fehler: Element 785 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(785));
        theTestList.append(607);
        Tester.verifyListContents(theTestList, new int[]{16,607});

        assertFalse("Fehler: Element 61 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(61));
        theTestList.append(583);
        Tester.verifyListContents(theTestList, new int[]{16,607,583});

        assertTrue("Fehler: Element 583 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(583));
        theTestList.append(399);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399});

        assertFalse("Fehler: Element 275 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(275));
        assertTrue("Fehler: Element 583 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(583));
        assertFalse("Fehler: Element 51 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(51));
        assertTrue("Fehler: Element 607 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(607));
        assertTrue("Fehler: Element 583 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(583));
        theTestList.append(490);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490});

        theTestList.append(219);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219});

        assertTrue("Fehler: Element 219 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(219));
        theTestList.append(96);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96});

        assertFalse("Fehler: Element 746 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(746));
        theTestList.append(608);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608});

        assertFalse("Fehler: Element 208 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(208));
        assertTrue("Fehler: Element 16 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(16));
        theTestList.append(831);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831});

        theTestList.append(53);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53});

        theTestList.append(379);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379});

        theTestList.append(280);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280});

        theTestList.append(802);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802});

        theTestList.append(634);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634});

        assertTrue("Fehler: Element 16 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(16));
        assertTrue("Fehler: Element 96 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(96));
        theTestList.append(527);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527});

        assertFalse("Fehler: Element 917 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(917));
        theTestList.append(333);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333});

        theTestList.append(606);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606});

        theTestList.append(713);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713});

        assertFalse("Fehler: Element 434 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(434));
        assertFalse("Fehler: Element 794 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(794));
        theTestList.append(724);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724});

        theTestList.append(244);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244});

        assertFalse("Fehler: Element 965 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(965));
        theTestList.append(6);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6});

        assertTrue("Fehler: Element 6 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(6));
        assertTrue("Fehler: Element 606 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(606));
        assertTrue("Fehler: Element 96 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(96));
        assertTrue("Fehler: Element 608 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(608));
        assertTrue("Fehler: Element 96 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(96));
        assertFalse("Fehler: Element 51 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(51));
        theTestList.append(472);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472});

        theTestList.append(188);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188});

        theTestList.append(637);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637});

        assertTrue("Fehler: Element 637 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(637));
        assertTrue("Fehler: Element 606 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(606));
        assertFalse("Fehler: Element 701 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(701));
        theTestList.append(833);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833});

        theTestList.append(886);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886});

        theTestList.append(895);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895});

        theTestList.append(576);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576});

        theTestList.append(2);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2});

        assertFalse("Fehler: Element 571 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(571));
        theTestList.append(224);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224});

        theTestList.append(2);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2});

        assertFalse("Fehler: Element 868 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(868));
        assertFalse("Fehler: Element 412 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(412));
        assertFalse("Fehler: Element 932 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(932));
        theTestList.append(909);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909});

        assertFalse("Fehler: Element 519 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(519));
        assertTrue("Fehler: Element 831 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(831));
        theTestList.append(456);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456});

        theTestList.append(588);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588});

        assertTrue("Fehler: Element 96 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(96));
        theTestList.append(692);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692});

        assertFalse("Fehler: Element 43 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(43));
        assertFalse("Fehler: Element 880 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(880));
        theTestList.append(342);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692,342});

        assertFalse("Fehler: Element 795 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(795));
        theTestList.append(95);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692,342,95});

        assertFalse("Fehler: Element 876 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(876));
        assertTrue("Fehler: Element 831 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(831));
        assertFalse("Fehler: Element 387 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(387));
        assertTrue("Fehler: Element 576 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(576));
        theTestList.append(386);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692,342,95,386});

        assertFalse("Fehler: Element 846 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(846));
        theTestList.append(398);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692,342,95,386,398});

        assertFalse("Fehler: Element 665 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(665));
        theTestList.append(251);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692,342,95,386,398,251});

        assertFalse("Fehler: Element 875 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(875));
        theTestList.append(535);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692,342,95,386,398,251,535});

        theTestList.append(856);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692,342,95,386,398,251,535,856});

        theTestList.append(602);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692,342,95,386,398,251,535,856,602});

        theTestList.append(921);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692,342,95,386,398,251,535,856,602,921});

        theTestList.append(645);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692,342,95,386,398,251,535,856,602,921,645});

        assertTrue("Fehler: Element 96 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(96));
        assertFalse("Fehler: Element 994 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(994));
        assertFalse("Fehler: Element 783 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(783));
        theTestList.append(82);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692,342,95,386,398,251,535,856,602,921,645,82});

        theTestList.append(971);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692,342,95,386,398,251,535,856,602,921,645,82,971});

        theTestList.append(245);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692,342,95,386,398,251,535,856,602,921,645,82,971,245});

        assertTrue("Fehler: Element 921 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(921));
        assertFalse("Fehler: Element 43 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(43));
        assertFalse("Fehler: Element 653 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(653));
        theTestList.append(955);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692,342,95,386,398,251,535,856,602,921,645,82,971,245,955});

        theTestList.append(978);
        Tester.verifyListContents(theTestList, new int[]{16,607,583,399,490,219,96,608,831,53,379,280,802,634,527,333,606,713,724,244,6,472,188,637,833,886,895,576,2,224,2,909,456,588,692,342,95,386,398,251,535,856,602,921,645,82,971,245,955,978});

    }

    public static void test8Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(557);
        Tester.verifyListContents(theTestList, new int[]{557});

        assertFalse("Fehler: Element 184 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(184));
        theTestList.append(265);
        Tester.verifyListContents(theTestList, new int[]{557,265});

        assertFalse("Fehler: Element 357 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(357));
        theTestList.append(82);
        Tester.verifyListContents(theTestList, new int[]{557,265,82});

        theTestList.append(217);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217});

        theTestList.append(644);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644});

        theTestList.append(366);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366});

        assertFalse("Fehler: Element 596 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(596));
        theTestList.append(743);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743});

        theTestList.append(893);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893});

        theTestList.append(423);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423});

        theTestList.append(29);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29});

        assertTrue("Fehler: Element 423 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(423));
        assertFalse("Fehler: Element 98 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(98));
        theTestList.append(704);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704});

        theTestList.append(957);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957});

        assertTrue("Fehler: Element 557 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(557));
        theTestList.append(816);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816});

        theTestList.append(920);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920});

        assertFalse("Fehler: Element 325 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(325));
        theTestList.append(408);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408});

        assertFalse("Fehler: Element 529 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(529));
        theTestList.append(41);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41});

        theTestList.append(653);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653});

        assertFalse("Fehler: Element 648 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(648));
        theTestList.append(402);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402});

        assertTrue("Fehler: Element 265 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(265));
        theTestList.append(932);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932});

        assertTrue("Fehler: Element 82 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(82));
        assertFalse("Fehler: Element 282 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(282));
        theTestList.append(727);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727});

        assertFalse("Fehler: Element 405 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(405));
        assertTrue("Fehler: Element 644 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(644));
        theTestList.append(393);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393});

        assertFalse("Fehler: Element 21 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(21));
        theTestList.append(19);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19});

        assertTrue("Fehler: Element 957 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(957));
        theTestList.append(547);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547});

        assertFalse("Fehler: Element 33 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(33));
        theTestList.append(494);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494});

        theTestList.append(152);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152});

        assertTrue("Fehler: Element 920 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(920));
        theTestList.append(6);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6});

        assertTrue("Fehler: Element 644 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(644));
        theTestList.append(531);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531});

        assertFalse("Fehler: Element 543 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(543));
        theTestList.append(306);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306});

        assertFalse("Fehler: Element 509 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(509));
        assertTrue("Fehler: Element 932 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(932));
        theTestList.append(444);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444});

        theTestList.append(70);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70});

        assertFalse("Fehler: Element 315 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(315));
        assertFalse("Fehler: Element 363 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(363));
        theTestList.append(464);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464});

        theTestList.append(855);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855});

        theTestList.append(40);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40});

        theTestList.append(716);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716});

        theTestList.append(584);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584});

        assertFalse("Fehler: Element 262 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(262));
        assertTrue("Fehler: Element 704 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(704));
        assertTrue("Fehler: Element 70 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(70));
        theTestList.append(868);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868});

        theTestList.append(845);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845});

        assertTrue("Fehler: Element 494 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(494));
        theTestList.append(196);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196});

        theTestList.append(261);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261});

        theTestList.append(178);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178});

        theTestList.append(232);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232});

        theTestList.append(163);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163});

        theTestList.append(946);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946});

        assertTrue("Fehler: Element 946 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(946));
        assertTrue("Fehler: Element 920 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(920));
        theTestList.append(177);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177});

        assertFalse("Fehler: Element 633 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(633));
        theTestList.append(434);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434});

        assertTrue("Fehler: Element 177 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(177));
        theTestList.append(614);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434,614});

        theTestList.append(743);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434,614,743});

        theTestList.append(186);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434,614,743,186});

        assertFalse("Fehler: Element 339 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(339));
        assertTrue("Fehler: Element 868 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(868));
        theTestList.append(178);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434,614,743,186,178});

        assertTrue("Fehler: Element 82 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(82));
        theTestList.append(242);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434,614,743,186,178,242});

        theTestList.append(808);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434,614,743,186,178,242,808});

        theTestList.append(473);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434,614,743,186,178,242,808,473});

        theTestList.append(623);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434,614,743,186,178,242,808,473,623});

        theTestList.append(673);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434,614,743,186,178,242,808,473,623,673});

        assertTrue("Fehler: Element 531 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(531));
        assertFalse("Fehler: Element 447 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(447));
        assertFalse("Fehler: Element 147 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(147));
        theTestList.append(179);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434,614,743,186,178,242,808,473,623,673,179});

        theTestList.append(52);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434,614,743,186,178,242,808,473,623,673,179,52});

        assertTrue("Fehler: Element 868 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(868));
        theTestList.append(944);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434,614,743,186,178,242,808,473,623,673,179,52,944});

        theTestList.append(88);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434,614,743,186,178,242,808,473,623,673,179,52,944,88});

        theTestList.append(903);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434,614,743,186,178,242,808,473,623,673,179,52,944,88,903});

        theTestList.append(866);
        Tester.verifyListContents(theTestList, new int[]{557,265,82,217,644,366,743,893,423,29,704,957,816,920,408,41,653,402,932,727,393,19,547,494,152,6,531,306,444,70,464,855,40,716,584,868,845,196,261,178,232,163,946,177,434,614,743,186,178,242,808,473,623,673,179,52,944,88,903,866});

        assertFalse("Fehler: Element 728 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(728));
    }

    public static void test9Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(975);
        Tester.verifyListContents(theTestList, new int[]{975});

        theTestList.append(395);
        Tester.verifyListContents(theTestList, new int[]{975,395});

        theTestList.append(147);
        Tester.verifyListContents(theTestList, new int[]{975,395,147});

        theTestList.append(806);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806});

        theTestList.append(192);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192});

        theTestList.append(107);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107});

        assertFalse("Fehler: Element 168 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(168));
        theTestList.append(209);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209});

        assertFalse("Fehler: Element 915 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(915));
        assertFalse("Fehler: Element 250 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(250));
        assertTrue("Fehler: Element 395 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(395));
        assertTrue("Fehler: Element 147 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(147));
        theTestList.append(821);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821});

        assertFalse("Fehler: Element 990 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(990));
        assertTrue("Fehler: Element 806 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(806));
        theTestList.append(572);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572});

        theTestList.append(711);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711});

        theTestList.append(779);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779});

        theTestList.append(333);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333});

        theTestList.append(663);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663});

        assertTrue("Fehler: Element 395 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(395));
        theTestList.append(340);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340});

        theTestList.append(294);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294});

        assertFalse("Fehler: Element 109 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(109));
        assertFalse("Fehler: Element 444 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(444));
        assertFalse("Fehler: Element 280 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(280));
        assertFalse("Fehler: Element 11 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(11));
        assertFalse("Fehler: Element 614 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(614));
        assertFalse("Fehler: Element 470 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(470));
        theTestList.append(931);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931});

        theTestList.append(76);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76});

        assertFalse("Fehler: Element 64 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(64));
        theTestList.append(606);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606});

        assertFalse("Fehler: Element 371 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(371));
        theTestList.append(331);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331});

        assertTrue("Fehler: Element 331 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(331));
        theTestList.append(271);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271});

        assertFalse("Fehler: Element 7 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(7));
        assertTrue("Fehler: Element 821 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(821));
        theTestList.append(793);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793});

        assertFalse("Fehler: Element 701 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(701));
        assertFalse("Fehler: Element 547 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(547));
        assertFalse("Fehler: Element 114 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(114));
        assertTrue("Fehler: Element 663 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(663));
        theTestList.append(570);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570});

        theTestList.append(15);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15});

        theTestList.append(871);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871});

        theTestList.append(631);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631});

        assertFalse("Fehler: Element 490 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(490));
        theTestList.append(328);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328});

        theTestList.append(362);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362});

        theTestList.append(46);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46});

        theTestList.append(259);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259});

        theTestList.append(276);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276});

        assertFalse("Fehler: Element 738 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(738));
        assertTrue("Fehler: Element 271 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(271));
        assertFalse("Fehler: Element 364 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(364));
        assertFalse("Fehler: Element 245 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(245));
        assertFalse("Fehler: Element 314 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(314));
        assertTrue("Fehler: Element 631 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(631));
        theTestList.append(318);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318});

        theTestList.append(21);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21});

        theTestList.append(920);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920});

        assertFalse("Fehler: Element 861 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(861));
        theTestList.append(932);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932});

        assertTrue("Fehler: Element 333 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(333));
        assertTrue("Fehler: Element 46 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(46));
        assertTrue("Fehler: Element 362 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(362));
        assertTrue("Fehler: Element 46 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(46));
        theTestList.append(723);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723});

        theTestList.append(362);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723,362});

        assertFalse("Fehler: Element 842 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(842));
        assertFalse("Fehler: Element 199 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(199));
        assertTrue("Fehler: Element 271 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(271));
        theTestList.append(890);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723,362,890});

        theTestList.append(968);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723,362,890,968});

        theTestList.append(909);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723,362,890,968,909});

        theTestList.append(399);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723,362,890,968,909,399});

        theTestList.append(143);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723,362,890,968,909,399,143});

        assertFalse("Fehler: Element 605 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(605));
        theTestList.append(297);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723,362,890,968,909,399,143,297});

        theTestList.append(788);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723,362,890,968,909,399,143,297,788});

        theTestList.append(407);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723,362,890,968,909,399,143,297,788,407});

        assertTrue("Fehler: Element 15 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(15));
        assertFalse("Fehler: Element 747 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(747));
        assertFalse("Fehler: Element 235 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(235));
        assertTrue("Fehler: Element 572 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(572));
        assertTrue("Fehler: Element 931 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(931));
        assertFalse("Fehler: Element 620 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(620));
        theTestList.append(439);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723,362,890,968,909,399,143,297,788,407,439});

        assertTrue("Fehler: Element 21 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(21));
        theTestList.append(920);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723,362,890,968,909,399,143,297,788,407,439,920});

        theTestList.append(905);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723,362,890,968,909,399,143,297,788,407,439,920,905});

        assertTrue("Fehler: Element 209 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(209));
        assertTrue("Fehler: Element 143 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(143));
        theTestList.append(480);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723,362,890,968,909,399,143,297,788,407,439,920,905,480});

        theTestList.append(327);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723,362,890,968,909,399,143,297,788,407,439,920,905,480,327});

        theTestList.append(124);
        Tester.verifyListContents(theTestList, new int[]{975,395,147,806,192,107,209,821,572,711,779,333,663,340,294,931,76,606,331,271,793,570,15,871,631,328,362,46,259,276,318,21,920,932,723,362,890,968,909,399,143,297,788,407,439,920,905,480,327,124});

        assertTrue("Fehler: Element 723 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(723));
        assertTrue("Fehler: Element 362 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(362));
    }

    public static void test10Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(117);
        Tester.verifyListContents(theTestList, new int[]{117});

        assertFalse("Fehler: Element 732 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(732));
        assertTrue("Fehler: Element 117 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(117));
        assertTrue("Fehler: Element 117 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(117));
        theTestList.append(452);
        Tester.verifyListContents(theTestList, new int[]{117,452});

        theTestList.append(853);
        Tester.verifyListContents(theTestList, new int[]{117,452,853});

        assertFalse("Fehler: Element 384 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(384));
        theTestList.append(622);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622});

        theTestList.append(500);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500});

        theTestList.append(284);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284});

        theTestList.append(81);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81});

        assertFalse("Fehler: Element 174 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(174));
        assertFalse("Fehler: Element 814 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(814));
        theTestList.append(278);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278});

        theTestList.append(401);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401});

        theTestList.append(507);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507});

        theTestList.append(701);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701});

        assertTrue("Fehler: Element 622 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(622));
        theTestList.append(709);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709});

        theTestList.append(756);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756});

        assertFalse("Fehler: Element 387 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(387));
        theTestList.append(870);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870});

        assertFalse("Fehler: Element 203 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(203));
        theTestList.append(931);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931});

        assertFalse("Fehler: Element 448 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(448));
        assertFalse("Fehler: Element 373 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(373));
        theTestList.append(494);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494});

        theTestList.append(543);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543});

        theTestList.append(627);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627});

        theTestList.append(304);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304});

        assertTrue("Fehler: Element 853 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(853));
        assertTrue("Fehler: Element 870 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(870));
        theTestList.append(972);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972});

        theTestList.append(679);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679});

        theTestList.append(297);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297});

        theTestList.append(788);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788});

        theTestList.append(55);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55});

        assertFalse("Fehler: Element 544 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(544));
        assertTrue("Fehler: Element 756 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(756));
        theTestList.append(159);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159});

        assertTrue("Fehler: Element 701 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(701));
        assertFalse("Fehler: Element 859 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(859));
        assertFalse("Fehler: Element 313 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(313));
        theTestList.append(248);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248});

        assertFalse("Fehler: Element 739 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(739));
        theTestList.append(756);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756});

        theTestList.append(383);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383});

        assertFalse("Fehler: Element 902 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(902));
        assertTrue("Fehler: Element 788 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(788));
        assertFalse("Fehler: Element 777 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(777));
        assertFalse("Fehler: Element 602 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(602));
        theTestList.append(375);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375});

        assertTrue("Fehler: Element 55 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(55));
        assertFalse("Fehler: Element 422 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(422));
        assertTrue("Fehler: Element 452 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(452));
        theTestList.append(566);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566});

        assertTrue("Fehler: Element 248 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(248));
        theTestList.append(15);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15});

        assertTrue("Fehler: Element 304 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(304));
        theTestList.append(757);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757});

        assertTrue("Fehler: Element 375 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(375));
        assertTrue("Fehler: Element 788 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(788));
        assertFalse("Fehler: Element 987 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(987));
        assertTrue("Fehler: Element 870 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(870));
        theTestList.append(470);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470});

        theTestList.append(385);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385});

        theTestList.append(10);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385,10});

        assertFalse("Fehler: Element 666 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(666));
        assertTrue("Fehler: Element 55 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(55));
        theTestList.append(337);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385,10,337});

        theTestList.append(490);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385,10,337,490});

        theTestList.append(417);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385,10,337,490,417});

        assertFalse("Fehler: Element 311 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(311));
        theTestList.append(200);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385,10,337,490,417,200});

        assertTrue("Fehler: Element 10 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(10));
        theTestList.append(601);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385,10,337,490,417,200,601});

        assertFalse("Fehler: Element 582 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(582));
        theTestList.append(709);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385,10,337,490,417,200,601,709});

        assertFalse("Fehler: Element 157 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(157));
        assertTrue("Fehler: Element 304 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(304));
        assertFalse("Fehler: Element 725 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(725));
        assertTrue("Fehler: Element 756 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(756));
        assertFalse("Fehler: Element 900 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(900));
        theTestList.append(547);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385,10,337,490,417,200,601,709,547});

        theTestList.append(282);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385,10,337,490,417,200,601,709,547,282});

        assertFalse("Fehler: Element 810 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(810));
        assertTrue("Fehler: Element 756 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(756));
        theTestList.append(433);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385,10,337,490,417,200,601,709,547,282,433});

        theTestList.append(454);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385,10,337,490,417,200,601,709,547,282,433,454});

        assertFalse("Fehler: Element 185 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(185));
        assertTrue("Fehler: Element 55 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(55));
        assertTrue("Fehler: Element 756 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(756));
        theTestList.append(640);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385,10,337,490,417,200,601,709,547,282,433,454,640});

        theTestList.append(480);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385,10,337,490,417,200,601,709,547,282,433,454,640,480});

        assertFalse("Fehler: Element 473 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(473));
        theTestList.append(355);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385,10,337,490,417,200,601,709,547,282,433,454,640,480,355});

        assertTrue("Fehler: Element 788 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(788));
        assertFalse("Fehler: Element 568 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(568));
        assertFalse("Fehler: Element 591 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(591));
        theTestList.append(375);
        Tester.verifyListContents(theTestList, new int[]{117,452,853,622,500,284,81,278,401,507,701,709,756,870,931,494,543,627,304,972,679,297,788,55,159,248,756,383,375,566,15,757,470,385,10,337,490,417,200,601,709,547,282,433,454,640,480,355,375});

}
}
