package tests;

import liste.*;
import tests.*;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Die Test-Klasse Tester.
 *
 * @author Rainer Helfrich
 * @version 03.10.2020
 */
public abstract class Tester {
    protected List<Integer> theTestList;

    protected static void verifyListContents(List<Integer> theTestList, int[] expected) {
        if (expected.length == 0)
            assertTrue(theTestList.isEmpty());
        else
            assertFalse(theTestList.isEmpty());
            assertEquals("Fehler: Länge stimmt nicht.", expected.length, theTestList.length());
        for (int i = 0; i < expected.length; i++) {
            assertEquals("Fehler: Element an Index " + i + " stimmt nicht.", expected[i], theTestList.getValueAtN(i).intValue());
        }
    }

    @Test
    public void test001() {
        SubTest1.test1Durchfuehren(theTestList);
    }

    @Test
    public void test002() {
        SubTest1.test2Durchfuehren(theTestList);
    }

    @Test
    public void test003() {
        SubTest1.test3Durchfuehren(theTestList);
    }

    @Test
    public void test004() {
        SubTest1.test4Durchfuehren(theTestList);
    }

    @Test
    public void test005() {
        SubTest1.test5Durchfuehren(theTestList);
    }

    @Test
    public void test006() {
        SubTest1.test6Durchfuehren(theTestList);
    }

    @Test
    public void test007() {
        SubTest1.test7Durchfuehren(theTestList);
    }

    @Test
    public void test008() {
        SubTest1.test8Durchfuehren(theTestList);
    }

    @Test
    public void test009() {
        SubTest1.test9Durchfuehren(theTestList);
    }

    @Test
    public void test010() {
        SubTest1.test10Durchfuehren(theTestList);
    }

    @Test
    public void test011() {
        SubTest2.test1Durchfuehren(theTestList);
    }

    @Test
    public void test012() {
        SubTest2.test2Durchfuehren(theTestList);
    }

    @Test
    public void test013() {
        SubTest2.test3Durchfuehren(theTestList);
    }

    @Test
    public void test014() {
        SubTest2.test4Durchfuehren(theTestList);
    }

    @Test
    public void test015() {
        SubTest2.test5Durchfuehren(theTestList);
    }

    @Test
    public void test016() {
        SubTest2.test6Durchfuehren(theTestList);
    }

    @Test
    public void test017() {
        SubTest2.test7Durchfuehren(theTestList);
    }

    @Test
    public void test018() {
        SubTest2.test8Durchfuehren(theTestList);
    }

    @Test
    public void test019() {
        SubTest2.test9Durchfuehren(theTestList);
    }

    @Test
    public void test020() {
        SubTest2.test10Durchfuehren(theTestList);
    }

    @Test
    public void test021() {
        SubTest3.test1Durchfuehren(theTestList);
    }

    @Test
    public void test022() {
        SubTest3.test2Durchfuehren(theTestList);
    }

    @Test
    public void test023() {
        SubTest3.test3Durchfuehren(theTestList);
    }

    @Test
    public void test024() {
        SubTest3.test4Durchfuehren(theTestList);
    }

    @Test
    public void test025() {
        SubTest3.test5Durchfuehren(theTestList);
    }

    @Test
    public void test026() {
        SubTest3.test6Durchfuehren(theTestList);
    }

    @Test
    public void test027() {
        SubTest3.test7Durchfuehren(theTestList);
    }

    @Test
    public void test028() {
        SubTest3.test8Durchfuehren(theTestList);
    }

    @Test
    public void test029() {
        SubTest3.test9Durchfuehren(theTestList);
    }

    @Test
    public void test030() {
        SubTest3.test10Durchfuehren(theTestList);
    }

    @Test
    public void test031() {
        SubTest4.test1Durchfuehren(theTestList);
    }

    @Test
    public void test032() {
        SubTest4.test2Durchfuehren(theTestList);
    }

    @Test
    public void test033() {
        SubTest4.test3Durchfuehren(theTestList);
    }

    @Test
    public void test034() {
        SubTest4.test4Durchfuehren(theTestList);
    }

    @Test
    public void test035() {
        SubTest4.test5Durchfuehren(theTestList);
    }

    @Test
    public void test036() {
        SubTest4.test6Durchfuehren(theTestList);
    }

    @Test
    public void test037() {
        SubTest4.test7Durchfuehren(theTestList);
    }

    @Test
    public void test038() {
        SubTest4.test8Durchfuehren(theTestList);
    }

    @Test
    public void test039() {
        SubTest4.test9Durchfuehren(theTestList);
    }

    @Test
    public void test040() {
        SubTest4.test10Durchfuehren(theTestList);
    }

    @Test
    public void test041() {
        SubTest5.test1Durchfuehren(theTestList);
    }

    @Test
    public void test042() {
        SubTest5.test2Durchfuehren(theTestList);
    }

    @Test
    public void test043() {
        SubTest5.test3Durchfuehren(theTestList);
    }

    @Test
    public void test044() {
        SubTest5.test4Durchfuehren(theTestList);
    }

    @Test
    public void test045() {
        SubTest5.test5Durchfuehren(theTestList);
    }

    @Test
    public void test046() {
        SubTest5.test6Durchfuehren(theTestList);
    }

    @Test
    public void test047() {
        SubTest5.test7Durchfuehren(theTestList);
    }

    @Test
    public void test048() {
        SubTest5.test8Durchfuehren(theTestList);
    }

    @Test
    public void test049() {
        SubTest5.test9Durchfuehren(theTestList);
    }

    @Test
    public void test050() {
        SubTest5.test10Durchfuehren(theTestList);
    }

    @Test
    public void test051() {
        SubTest6.test1Durchfuehren(theTestList);
    }

    @Test
    public void test052() {
        SubTest6.test2Durchfuehren(theTestList);
    }

    @Test
    public void test053() {
        SubTest6.test3Durchfuehren(theTestList);
    }

    @Test
    public void test054() {
        SubTest6.test4Durchfuehren(theTestList);
    }

    @Test
    public void test055() {
        SubTest6.test5Durchfuehren(theTestList);
    }

    @Test
    public void test056() {
        SubTest6.test6Durchfuehren(theTestList);
    }

    @Test
    public void test057() {
        SubTest6.test7Durchfuehren(theTestList);
    }

    @Test
    public void test058() {
        SubTest6.test8Durchfuehren(theTestList);
    }

    @Test
    public void test059() {
        SubTest6.test9Durchfuehren(theTestList);
    }

    @Test
    public void test060() {
        SubTest6.test10Durchfuehren(theTestList);
    }

    @Test
    public void test061() {
        SubTest7.test1Durchfuehren(theTestList);
    }

    @Test
    public void test062() {
        SubTest7.test2Durchfuehren(theTestList);
    }

    @Test
    public void test063() {
        SubTest7.test3Durchfuehren(theTestList);
    }

    @Test
    public void test064() {
        SubTest7.test4Durchfuehren(theTestList);
    }

    @Test
    public void test065() {
        SubTest7.test5Durchfuehren(theTestList);
    }

    @Test
    public void test066() {
        SubTest7.test6Durchfuehren(theTestList);
    }

    @Test
    public void test067() {
        SubTest7.test7Durchfuehren(theTestList);
    }

    @Test
    public void test068() {
        SubTest7.test8Durchfuehren(theTestList);
    }

    @Test
    public void test069() {
        SubTest7.test9Durchfuehren(theTestList);
    }

    @Test
    public void test070() {
        SubTest7.test10Durchfuehren(theTestList);
    }

    @Test
    public void test071() {
        SubTest8.test1Durchfuehren(theTestList);
    }

    @Test
    public void test072() {
        SubTest8.test2Durchfuehren(theTestList);
    }

    @Test
    public void test073() {
        SubTest8.test3Durchfuehren(theTestList);
    }

    @Test
    public void test074() {
        SubTest8.test4Durchfuehren(theTestList);
    }

    @Test
    public void test075() {
        SubTest8.test5Durchfuehren(theTestList);
    }

    @Test
    public void test076() {
        SubTest8.test6Durchfuehren(theTestList);
    }

    @Test
    public void test077() {
        SubTest8.test7Durchfuehren(theTestList);
    }

    @Test
    public void test078() {
        SubTest8.test8Durchfuehren(theTestList);
    }

    @Test
    public void test079() {
        SubTest8.test9Durchfuehren(theTestList);
    }

    @Test
    public void test080() {
        SubTest8.test10Durchfuehren(theTestList);
    }

    @Test
    public void test081() {
        SubTest9.test1Durchfuehren(theTestList);
    }

    @Test
    public void test082() {
        SubTest9.test2Durchfuehren(theTestList);
    }

    @Test
    public void test083() {
        SubTest9.test3Durchfuehren(theTestList);
    }

    @Test
    public void test084() {
        SubTest9.test4Durchfuehren(theTestList);
    }

    @Test
    public void test085() {
        SubTest9.test5Durchfuehren(theTestList);
    }

    @Test
    public void test086() {
        SubTest9.test6Durchfuehren(theTestList);
    }

    @Test
    public void test087() {
        SubTest9.test7Durchfuehren(theTestList);
    }

    @Test
    public void test088() {
        SubTest9.test8Durchfuehren(theTestList);
    }

    @Test
    public void test089() {
        SubTest9.test9Durchfuehren(theTestList);
    }

    @Test
    public void test090() {
        SubTest9.test10Durchfuehren(theTestList);
    }

    @Test
    public void test091() {
        SubTest10.test1Durchfuehren(theTestList);
    }

    @Test
    public void test092() {
        SubTest10.test2Durchfuehren(theTestList);
    }

    @Test
    public void test093() {
        SubTest10.test3Durchfuehren(theTestList);
    }

    @Test
    public void test094() {
        SubTest10.test4Durchfuehren(theTestList);
    }

    @Test
    public void test095() {
        SubTest10.test5Durchfuehren(theTestList);
    }

    @Test
    public void test096() {
        SubTest10.test6Durchfuehren(theTestList);
    }

    @Test
    public void test097() {
        SubTest10.test7Durchfuehren(theTestList);
    }

    @Test
    public void test098() {
        SubTest10.test8Durchfuehren(theTestList);
    }

    @Test
    public void test099() {
        SubTest10.test9Durchfuehren(theTestList);
    }

    @Test
    public void test100() {
        SubTest10.test10Durchfuehren(theTestList);
    }

}
