package tests;

import static org.junit.Assert.*;

import liste.*;

/**
 * Die Test-Klasse SubTest2.
 *
 * @author Rainer Helfrich
 * @version 03.10.2020
 */
public class SubTest2 {


    public static void test1Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(624);
        Tester.verifyListContents(theTestList, new int[]{624});

        theTestList.append(753);
        Tester.verifyListContents(theTestList, new int[]{624, 753});

        theTestList.append(927);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927});

        assertTrue("Fehler: Element 927 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(927));
        theTestList.append(342);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342});

        assertTrue("Fehler: Element 753 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(753));
        assertTrue("Fehler: Element 753 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(753));
        theTestList.append(564);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564});

        theTestList.append(926);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926});

        assertFalse("Fehler: Element 520 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(520));
        theTestList.append(344);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344});

        theTestList.append(167);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167});

        theTestList.append(269);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269});

        theTestList.append(625);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625});

        assertTrue("Fehler: Element 753 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(753));
        assertFalse("Fehler: Element 985 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(985));
        assertFalse("Fehler: Element 21 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(21));
        assertTrue("Fehler: Element 927 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(927));
        assertTrue("Fehler: Element 564 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(564));
        assertFalse("Fehler: Element 766 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(766));
        theTestList.append(672);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672});

        assertFalse("Fehler: Element 365 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(365));
        assertTrue("Fehler: Element 926 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(926));
        assertTrue("Fehler: Element 625 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(625));
        assertFalse("Fehler: Element 685 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(685));
        theTestList.append(389);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389});

        assertTrue("Fehler: Element 389 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(389));
        assertFalse("Fehler: Element 955 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(955));
        theTestList.append(988);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988});

        theTestList.append(101);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101});

        assertTrue("Fehler: Element 269 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(269));
        theTestList.append(994);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994});

        assertTrue("Fehler: Element 624 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(624));
        theTestList.append(709);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709});

        assertFalse("Fehler: Element 336 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(336));
        theTestList.append(547);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547});

        assertFalse("Fehler: Element 881 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(881));
        theTestList.append(709);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709});

        assertFalse("Fehler: Element 339 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(339));
        theTestList.append(454);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454});

        assertTrue("Fehler: Element 101 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(101));
        theTestList.append(149);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149});

        assertFalse("Fehler: Element 141 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(141));
        assertFalse("Fehler: Element 450 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(450));
        theTestList.append(253);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253});

        assertFalse("Fehler: Element 281 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(281));
        assertTrue("Fehler: Element 547 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(547));
        assertTrue("Fehler: Element 454 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(454));
        theTestList.append(772);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772});

        assertFalse("Fehler: Element 596 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(596));
        assertTrue("Fehler: Element 564 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(564));
        theTestList.append(310);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310});

        theTestList.append(781);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781});

        assertFalse("Fehler: Element 774 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(774));
        theTestList.append(57);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57});

        theTestList.append(28);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28});

        theTestList.append(861);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861});

        theTestList.append(668);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668});

        assertTrue("Fehler: Element 772 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(772));
        assertTrue("Fehler: Element 861 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(861));
        assertTrue("Fehler: Element 994 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(994));
        theTestList.append(813);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668, 813});

        assertFalse("Fehler: Element 128 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(128));
        assertFalse("Fehler: Element 524 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(524));
        assertTrue("Fehler: Element 772 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(772));
        assertFalse("Fehler: Element 289 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(289));
        theTestList.append(887);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668, 813, 887});

        assertTrue("Fehler: Element 772 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(772));
        assertFalse("Fehler: Element 626 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(626));
        theTestList.append(944);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668, 813, 887, 944});

        theTestList.append(828);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668, 813, 887, 944, 828});

        assertFalse("Fehler: Element 314 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(314));
        assertFalse("Fehler: Element 46 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(46));
        theTestList.append(762);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668, 813, 887, 944, 828, 762});

        assertFalse("Fehler: Element 346 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(346));
        assertFalse("Fehler: Element 485 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(485));
        assertTrue("Fehler: Element 762 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(762));
        theTestList.append(9);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668, 813, 887, 944, 828, 762, 9});

        theTestList.append(360);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668, 813, 887, 944, 828, 762, 9, 360});

        assertTrue("Fehler: Element 709 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(709));
        theTestList.append(564);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668, 813, 887, 944, 828, 762, 9, 360, 564});

        theTestList.append(20);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668, 813, 887, 944, 828, 762, 9, 360, 564, 20});

        theTestList.append(564);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668, 813, 887, 944, 828, 762, 9, 360, 564, 20, 564});

        assertTrue("Fehler: Element 927 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(927));
        theTestList.append(85);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668, 813, 887, 944, 828, 762, 9, 360, 564, 20, 564, 85});

        theTestList.append(987);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668, 813, 887, 944, 828, 762, 9, 360, 564, 20, 564, 85, 987});

        assertFalse("Fehler: Element 900 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(900));
        assertFalse("Fehler: Element 98 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(98));
        assertFalse("Fehler: Element 888 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(888));
        theTestList.append(963);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668, 813, 887, 944, 828, 762, 9, 360, 564, 20, 564, 85, 987, 963});

        assertTrue("Fehler: Element 709 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(709));
        assertFalse("Fehler: Element 874 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(874));
        assertFalse("Fehler: Element 937 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(937));
        assertFalse("Fehler: Element 607 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(607));
        assertTrue("Fehler: Element 887 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(887));
        assertFalse("Fehler: Element 972 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(972));
        theTestList.append(693);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668, 813, 887, 944, 828, 762, 9, 360, 564, 20, 564, 85, 987, 963, 693});

        assertFalse("Fehler: Element 975 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(975));
        theTestList.append(897);
        Tester.verifyListContents(theTestList, new int[]{624, 753, 927, 342, 564, 926, 344, 167, 269, 625, 672, 389, 988, 101, 994, 709, 547, 709, 454, 149, 253, 772, 310, 781, 57, 28, 861, 668, 813, 887, 944, 828, 762, 9, 360, 564, 20, 564, 85, 987, 963, 693, 897});

        assertTrue("Fehler: Element 828 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(828));
    }

    public static void test2Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(116);
        Tester.verifyListContents(theTestList, new int[]{116});

        theTestList.append(649);
        Tester.verifyListContents(theTestList, new int[]{116, 649});

        assertFalse("Fehler: Element 823 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(823));
        assertFalse("Fehler: Element 62 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(62));
        theTestList.append(529);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529});

        theTestList.append(636);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636});

        theTestList.append(198);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198});

        assertTrue("Fehler: Element 116 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(116));
        theTestList.append(319);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319});

        assertFalse("Fehler: Element 139 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(139));
        theTestList.append(658);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658});

        assertTrue("Fehler: Element 649 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(649));
        assertTrue("Fehler: Element 636 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(636));
        assertFalse("Fehler: Element 450 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(450));
        theTestList.append(869);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869});

        assertFalse("Fehler: Element 978 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(978));
        theTestList.append(664);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664});

        theTestList.append(659);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659});

        theTestList.append(900);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900});

        theTestList.append(607);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607});

        theTestList.append(276);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276});

        theTestList.append(321);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321});

        assertFalse("Fehler: Element 745 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(745));
        assertFalse("Fehler: Element 415 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(415));
        theTestList.append(92);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92});

        assertTrue("Fehler: Element 659 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(659));
        assertFalse("Fehler: Element 929 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(929));
        assertTrue("Fehler: Element 658 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(658));
        assertTrue("Fehler: Element 649 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(649));
        theTestList.append(923);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923});

        theTestList.append(965);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965});

        assertFalse("Fehler: Element 292 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(292));
        theTestList.append(799);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799});

        theTestList.append(619);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619});

        assertFalse("Fehler: Element 85 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(85));
        theTestList.append(798);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798});

        theTestList.append(397);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397});

        assertTrue("Fehler: Element 869 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(869));
        assertFalse("Fehler: Element 766 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(766));
        theTestList.append(247);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247});

        theTestList.append(67);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67});

        theTestList.append(272);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272});

        theTestList.append(477);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477});

        assertFalse("Fehler: Element 299 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(299));
        assertFalse("Fehler: Element 267 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(267));
        assertTrue("Fehler: Element 965 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(965));
        theTestList.append(159);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159});

        assertTrue("Fehler: Element 116 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(116));
        assertTrue("Fehler: Element 664 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(664));
        theTestList.append(421);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421});

        assertTrue("Fehler: Element 92 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(92));
        assertFalse("Fehler: Element 931 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(931));
        assertTrue("Fehler: Element 869 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(869));
        theTestList.append(940);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940});

        theTestList.append(865);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865});

        theTestList.append(248);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248});

        theTestList.append(698);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698});

        theTestList.append(199);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199});

        theTestList.append(735);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735});

        assertFalse("Fehler: Element 78 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(78));
        theTestList.append(814);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814});

        assertFalse("Fehler: Element 9 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(9));
        theTestList.append(744);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744});

        assertFalse("Fehler: Element 985 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(985));
        assertFalse("Fehler: Element 465 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(465));
        assertTrue("Fehler: Element 698 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(698));
        assertFalse("Fehler: Element 781 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(781));
        assertFalse("Fehler: Element 165 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(165));
        assertFalse("Fehler: Element 26 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(26));
        assertFalse("Fehler: Element 308 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(308));
        assertTrue("Fehler: Element 636 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(636));
        theTestList.append(503);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744, 503});

        assertTrue("Fehler: Element 659 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(659));
        theTestList.append(629);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744, 503, 629});

        theTestList.append(848);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744, 503, 629, 848});

        theTestList.append(765);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744, 503, 629, 848, 765});

        theTestList.append(738);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744, 503, 629, 848, 765, 738});

        assertFalse("Fehler: Element 500 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(500));
        assertTrue("Fehler: Element 900 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(900));
        assertFalse("Fehler: Element 156 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(156));
        theTestList.append(366);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744, 503, 629, 848, 765, 738, 366});

        theTestList.append(915);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744, 503, 629, 848, 765, 738, 366, 915});

        assertTrue("Fehler: Element 421 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(421));
        assertTrue("Fehler: Element 659 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(659));
        assertFalse("Fehler: Element 717 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(717));
        assertTrue("Fehler: Element 247 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(247));
        theTestList.append(479);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744, 503, 629, 848, 765, 738, 366, 915, 479});

        assertTrue("Fehler: Element 798 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(798));
        theTestList.append(198);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744, 503, 629, 848, 765, 738, 366, 915, 479, 198});

        theTestList.append(189);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744, 503, 629, 848, 765, 738, 366, 915, 479, 198, 189});

        theTestList.append(594);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744, 503, 629, 848, 765, 738, 366, 915, 479, 198, 189, 594});

        assertFalse("Fehler: Element 230 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(230));
        assertFalse("Fehler: Element 901 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(901));
        theTestList.append(118);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744, 503, 629, 848, 765, 738, 366, 915, 479, 198, 189, 594, 118});

        theTestList.append(480);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744, 503, 629, 848, 765, 738, 366, 915, 479, 198, 189, 594, 118, 480});

        assertFalse("Fehler: Element 4 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(4));
        assertFalse("Fehler: Element 313 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(313));
        theTestList.append(383);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744, 503, 629, 848, 765, 738, 366, 915, 479, 198, 189, 594, 118, 480, 383});

        assertTrue("Fehler: Element 765 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(765));
        theTestList.append(720);
        Tester.verifyListContents(theTestList, new int[]{116, 649, 529, 636, 198, 319, 658, 869, 664, 659, 900, 607, 276, 321, 92, 923, 965, 799, 619, 798, 397, 247, 67, 272, 477, 159, 421, 940, 865, 248, 698, 199, 735, 814, 744, 503, 629, 848, 765, 738, 366, 915, 479, 198, 189, 594, 118, 480, 383, 720});

    }

    public static void test3Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(302);
        Tester.verifyListContents(theTestList, new int[]{302});

        assertTrue("Fehler: Element 302 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(302));
        assertFalse("Fehler: Element 558 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(558));
        theTestList.append(444);
        Tester.verifyListContents(theTestList, new int[]{302, 444});

        assertTrue("Fehler: Element 302 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(302));
        assertFalse("Fehler: Element 114 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(114));
        theTestList.append(33);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33});

        theTestList.append(231);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231});

        theTestList.append(429);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429});

        assertTrue("Fehler: Element 231 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(231));
        assertTrue("Fehler: Element 231 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(231));
        assertFalse("Fehler: Element 502 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(502));
        assertTrue("Fehler: Element 429 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(429));
        theTestList.append(336);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336});

        assertTrue("Fehler: Element 336 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(336));
        assertFalse("Fehler: Element 792 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(792));
        assertTrue("Fehler: Element 231 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(231));
        theTestList.append(838);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838});

        theTestList.append(81);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81});

        theTestList.append(180);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180});

        theTestList.append(922);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922});

        assertTrue("Fehler: Element 336 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(336));
        assertFalse("Fehler: Element 232 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(232));
        assertTrue("Fehler: Element 81 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(81));
        assertTrue("Fehler: Element 81 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(81));
        assertFalse("Fehler: Element 522 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(522));
        assertFalse("Fehler: Element 1 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(1));
        theTestList.append(15);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15});

        assertFalse("Fehler: Element 85 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(85));
        assertTrue("Fehler: Element 922 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(922));
        assertTrue("Fehler: Element 231 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(231));
        assertTrue("Fehler: Element 838 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(838));
        theTestList.append(120);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120});

        theTestList.append(58);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58});

        theTestList.append(901);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901});

        assertTrue("Fehler: Element 15 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(15));
        theTestList.append(285);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285});

        theTestList.append(897);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897});

        theTestList.append(51);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51});

        theTestList.append(155);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155});

        assertFalse("Fehler: Element 451 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(451));
        assertFalse("Fehler: Element 348 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(348));
        assertFalse("Fehler: Element 88 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(88));
        assertFalse("Fehler: Element 776 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(776));
        assertFalse("Fehler: Element 484 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(484));
        assertFalse("Fehler: Element 673 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(673));
        theTestList.append(363);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363});

        theTestList.append(400);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400});

        theTestList.append(618);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618});

        assertFalse("Fehler: Element 679 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(679));
        assertFalse("Fehler: Element 200 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(200));
        theTestList.append(776);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776});

        theTestList.append(239);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239});

        assertTrue("Fehler: Element 302 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(302));
        assertFalse("Fehler: Element 472 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(472));
        assertTrue("Fehler: Element 155 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(155));
        assertFalse("Fehler: Element 226 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(226));
        assertFalse("Fehler: Element 174 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(174));
        assertFalse("Fehler: Element 316 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(316));
        theTestList.append(395);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395});

        assertTrue("Fehler: Element 58 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(58));
        theTestList.append(871);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871});

        theTestList.append(65);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65});

        theTestList.append(26);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26});

        theTestList.append(761);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761});

        assertTrue("Fehler: Element 120 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(120));
        theTestList.append(411);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411});

        theTestList.append(846);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846});

        theTestList.append(512);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512});

        assertFalse("Fehler: Element 378 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(378));
        assertTrue("Fehler: Element 81 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(81));
        assertFalse("Fehler: Element 737 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(737));
        assertFalse("Fehler: Element 1 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(1));
        theTestList.append(258);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258});

        theTestList.append(392);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258, 392});

        assertFalse("Fehler: Element 252 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(252));
        theTestList.append(153);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258, 392, 153});

        assertTrue("Fehler: Element 239 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(239));
        assertFalse("Fehler: Element 272 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(272));
        theTestList.append(377);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258, 392, 153, 377});

        theTestList.append(138);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258, 392, 153, 377, 138});

        assertTrue("Fehler: Element 411 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(411));
        assertTrue("Fehler: Element 180 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(180));
        theTestList.append(925);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258, 392, 153, 377, 138, 925});

        assertFalse("Fehler: Element 647 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(647));
        theTestList.append(59);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258, 392, 153, 377, 138, 925, 59});

        theTestList.append(986);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258, 392, 153, 377, 138, 925, 59, 986});

        theTestList.append(197);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258, 392, 153, 377, 138, 925, 59, 986, 197});

        assertFalse("Fehler: Element 709 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(709));
        theTestList.append(980);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258, 392, 153, 377, 138, 925, 59, 986, 197, 980});

        theTestList.append(812);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258, 392, 153, 377, 138, 925, 59, 986, 197, 980, 812});

        theTestList.append(747);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258, 392, 153, 377, 138, 925, 59, 986, 197, 980, 812, 747});

        assertFalse("Fehler: Element 358 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(358));
        theTestList.append(107);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258, 392, 153, 377, 138, 925, 59, 986, 197, 980, 812, 747, 107});

        theTestList.append(336);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258, 392, 153, 377, 138, 925, 59, 986, 197, 980, 812, 747, 107, 336});

        assertFalse("Fehler: Element 70 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(70));
        assertFalse("Fehler: Element 701 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(701));
        assertFalse("Fehler: Element 666 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(666));
        theTestList.append(793);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258, 392, 153, 377, 138, 925, 59, 986, 197, 980, 812, 747, 107, 336, 793});

        theTestList.append(635);
        Tester.verifyListContents(theTestList, new int[]{302, 444, 33, 231, 429, 336, 838, 81, 180, 922, 15, 120, 58, 901, 285, 897, 51, 155, 363, 400, 618, 776, 239, 395, 871, 65, 26, 761, 411, 846, 512, 258, 392, 153, 377, 138, 925, 59, 986, 197, 980, 812, 747, 107, 336, 793, 635});

    }

    public static void test4Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(764);
        Tester.verifyListContents(theTestList, new int[]{764});

        assertTrue("Fehler: Element 764 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(764));
        theTestList.append(327);
        Tester.verifyListContents(theTestList, new int[]{764, 327});

        theTestList.append(737);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737});

        assertTrue("Fehler: Element 764 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(764));
        assertTrue("Fehler: Element 764 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(764));
        assertFalse("Fehler: Element 882 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(882));
        theTestList.append(778);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778});

        theTestList.append(200);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200});

        theTestList.append(222);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222});

        theTestList.append(316);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316});

        theTestList.append(977);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977});

        assertFalse("Fehler: Element 999 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(999));
        theTestList.append(466);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466});

        assertFalse("Fehler: Element 99 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(99));
        theTestList.append(546);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546});

        assertFalse("Fehler: Element 618 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(618));
        assertTrue("Fehler: Element 222 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(222));
        theTestList.append(839);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839});

        theTestList.append(683);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683});

        theTestList.append(780);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780});

        assertTrue("Fehler: Element 222 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(222));
        assertFalse("Fehler: Element 20 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(20));
        theTestList.append(619);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619});

        assertFalse("Fehler: Element 256 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(256));
        assertFalse("Fehler: Element 434 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(434));
        theTestList.append(634);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634});

        assertTrue("Fehler: Element 200 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(200));
        assertTrue("Fehler: Element 327 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(327));
        assertTrue("Fehler: Element 316 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(316));
        assertFalse("Fehler: Element 367 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(367));
        assertFalse("Fehler: Element 625 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(625));
        theTestList.append(344);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344});

        theTestList.append(835);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835});

        theTestList.append(968);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968});

        assertFalse("Fehler: Element 497 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(497));
        assertFalse("Fehler: Element 965 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(965));
        theTestList.append(387);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387});

        theTestList.append(390);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390});

        theTestList.append(889);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889});

        assertTrue("Fehler: Element 390 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(390));
        theTestList.append(233);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233});

        assertTrue("Fehler: Element 390 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(390));
        assertFalse("Fehler: Element 657 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(657));
        theTestList.append(82);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82});

        theTestList.append(703);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703});

        assertFalse("Fehler: Element 170 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(170));
        assertTrue("Fehler: Element 835 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(835));
        assertTrue("Fehler: Element 390 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(390));
        assertFalse("Fehler: Element 969 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(969));
        assertTrue("Fehler: Element 390 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(390));
        assertTrue("Fehler: Element 737 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(737));
        assertFalse("Fehler: Element 207 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(207));
        assertFalse("Fehler: Element 289 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(289));
        theTestList.append(651);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651});

        assertTrue("Fehler: Element 889 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(889));
        theTestList.append(960);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960});

        assertTrue("Fehler: Element 778 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(778));
        theTestList.append(33);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33});

        assertTrue("Fehler: Element 466 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(466));
        theTestList.append(100);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100});

        theTestList.append(652);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652});

        theTestList.append(908);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908});

        assertTrue("Fehler: Element 390 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(390));
        theTestList.append(311);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311});

        assertFalse("Fehler: Element 622 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(622));
        theTestList.append(593);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593});

        theTestList.append(803);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803});

        theTestList.append(545);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803, 545});

        assertTrue("Fehler: Element 33 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(33));
        assertTrue("Fehler: Element 390 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(390));
        assertTrue("Fehler: Element 311 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(311));
        assertFalse("Fehler: Element 361 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(361));
        theTestList.append(616);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803, 545, 616});

        theTestList.append(265);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803, 545, 616, 265});

        theTestList.append(819);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803, 545, 616, 265, 819});

        theTestList.append(323);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803, 545, 616, 265, 819, 323});

        assertFalse("Fehler: Element 306 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(306));
        theTestList.append(598);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803, 545, 616, 265, 819, 323, 598});

        assertTrue("Fehler: Element 327 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(327));
        assertFalse("Fehler: Element 377 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(377));
        assertTrue("Fehler: Element 593 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(593));
        theTestList.append(174);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803, 545, 616, 265, 819, 323, 598, 174});

        theTestList.append(619);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803, 545, 616, 265, 819, 323, 598, 174, 619});

        theTestList.append(678);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803, 545, 616, 265, 819, 323, 598, 174, 619, 678});

        theTestList.append(656);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803, 545, 616, 265, 819, 323, 598, 174, 619, 678, 656});

        theTestList.append(892);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803, 545, 616, 265, 819, 323, 598, 174, 619, 678, 656, 892});

        assertTrue("Fehler: Element 892 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(892));
        assertFalse("Fehler: Element 995 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(995));
        assertFalse("Fehler: Element 54 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(54));
        assertTrue("Fehler: Element 683 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(683));
        assertFalse("Fehler: Element 85 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(85));
        theTestList.append(160);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803, 545, 616, 265, 819, 323, 598, 174, 619, 678, 656, 892, 160});

        assertFalse("Fehler: Element 872 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(872));
        theTestList.append(433);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803, 545, 616, 265, 819, 323, 598, 174, 619, 678, 656, 892, 160, 433});

        assertFalse("Fehler: Element 133 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(133));
        theTestList.append(17);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803, 545, 616, 265, 819, 323, 598, 174, 619, 678, 656, 892, 160, 433, 17});

        assertTrue("Fehler: Element 233 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(233));
        assertTrue("Fehler: Element 892 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(892));
        theTestList.append(211);
        Tester.verifyListContents(theTestList, new int[]{764, 327, 737, 778, 200, 222, 316, 977, 466, 546, 839, 683, 780, 619, 634, 344, 835, 968, 387, 390, 889, 233, 82, 703, 651, 960, 33, 100, 652, 908, 311, 593, 803, 545, 616, 265, 819, 323, 598, 174, 619, 678, 656, 892, 160, 433, 17, 211});

    }

    public static void test5Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(725);
        Tester.verifyListContents(theTestList, new int[]{725});

        assertTrue("Fehler: Element 725 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(725));
        assertFalse("Fehler: Element 54 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(54));
        theTestList.append(72);
        Tester.verifyListContents(theTestList, new int[]{725, 72});

        assertFalse("Fehler: Element 854 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(854));
        assertFalse("Fehler: Element 619 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(619));
        assertTrue("Fehler: Element 72 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(72));
        assertFalse("Fehler: Element 263 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(263));
        assertFalse("Fehler: Element 601 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(601));
        assertTrue("Fehler: Element 725 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(725));
        theTestList.append(65);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65});

        assertFalse("Fehler: Element 164 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(164));
        assertTrue("Fehler: Element 725 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(725));
        theTestList.append(81);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81});

        assertFalse("Fehler: Element 182 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(182));
        assertTrue("Fehler: Element 725 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(725));
        theTestList.append(898);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898});

        assertFalse("Fehler: Element 239 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(239));
        assertFalse("Fehler: Element 2 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(2));
        assertFalse("Fehler: Element 335 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(335));
        theTestList.append(187);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187});

        theTestList.append(711);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711});

        theTestList.append(6);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6});

        assertFalse("Fehler: Element 873 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(873));
        theTestList.append(421);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421});

        assertTrue("Fehler: Element 711 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(711));
        assertFalse("Fehler: Element 103 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(103));
        theTestList.append(430);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430});

        assertFalse("Fehler: Element 250 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(250));
        assertFalse("Fehler: Element 517 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(517));
        theTestList.append(951);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951});

        assertTrue("Fehler: Element 421 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(421));
        theTestList.append(114);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114});

        assertFalse("Fehler: Element 518 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(518));
        assertTrue("Fehler: Element 725 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(725));
        assertFalse("Fehler: Element 417 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(417));
        theTestList.append(863);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863});

        theTestList.append(951);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951});

        theTestList.append(890);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890});

        theTestList.append(816);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816});

        assertFalse("Fehler: Element 866 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(866));
        theTestList.append(146);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146});

        theTestList.append(304);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304});

        theTestList.append(48);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48});

        theTestList.append(96);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96});

        theTestList.append(187);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187});

        theTestList.append(267);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267});

        assertFalse("Fehler: Element 302 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(302));
        theTestList.append(562);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562});

        assertTrue("Fehler: Element 421 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(421));
        theTestList.append(442);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442});

        assertTrue("Fehler: Element 81 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(81));
        assertFalse("Fehler: Element 291 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(291));
        theTestList.append(944);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944});

        theTestList.append(191);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191});

        assertTrue("Fehler: Element 890 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(890));
        assertFalse("Fehler: Element 7 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(7));
        assertFalse("Fehler: Element 215 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(215));
        assertFalse("Fehler: Element 247 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(247));
        assertFalse("Fehler: Element 602 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(602));
        assertTrue("Fehler: Element 146 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(146));
        assertTrue("Fehler: Element 898 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(898));
        theTestList.append(369);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369});

        theTestList.append(699);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369, 699});

        theTestList.append(864);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369, 699, 864});

        theTestList.append(886);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369, 699, 864, 886});

        assertFalse("Fehler: Element 92 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(92));
        theTestList.append(400);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369, 699, 864, 886, 400});

        assertFalse("Fehler: Element 379 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(379));
        assertFalse("Fehler: Element 566 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(566));
        assertTrue("Fehler: Element 369 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(369));
        assertTrue("Fehler: Element 699 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(699));
        theTestList.append(274);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369, 699, 864, 886, 400, 274});

        assertTrue("Fehler: Element 48 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(48));
        assertFalse("Fehler: Element 396 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(396));
        theTestList.append(50);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369, 699, 864, 886, 400, 274, 50});

        theTestList.append(343);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369, 699, 864, 886, 400, 274, 50, 343});

        assertTrue("Fehler: Element 944 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(944));
        assertTrue("Fehler: Element 114 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(114));
        theTestList.append(964);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369, 699, 864, 886, 400, 274, 50, 343, 964});

        assertFalse("Fehler: Element 27 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(27));
        assertFalse("Fehler: Element 770 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(770));
        assertFalse("Fehler: Element 152 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(152));
        theTestList.append(421);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369, 699, 864, 886, 400, 274, 50, 343, 964, 421});

        assertFalse("Fehler: Element 670 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(670));
        theTestList.append(805);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369, 699, 864, 886, 400, 274, 50, 343, 964, 421, 805});

        assertFalse("Fehler: Element 9 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(9));
        assertTrue("Fehler: Element 72 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(72));
        assertTrue("Fehler: Element 191 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(191));
        theTestList.append(487);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369, 699, 864, 886, 400, 274, 50, 343, 964, 421, 805, 487});

        assertTrue("Fehler: Element 421 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(421));
        assertTrue("Fehler: Element 96 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(96));
        theTestList.append(76);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369, 699, 864, 886, 400, 274, 50, 343, 964, 421, 805, 487, 76});

        assertFalse("Fehler: Element 298 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(298));
        theTestList.append(847);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369, 699, 864, 886, 400, 274, 50, 343, 964, 421, 805, 487, 76, 847});

        assertTrue("Fehler: Element 863 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(863));
        theTestList.append(908);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369, 699, 864, 886, 400, 274, 50, 343, 964, 421, 805, 487, 76, 847, 908});

        theTestList.append(374);
        Tester.verifyListContents(theTestList, new int[]{725, 72, 65, 81, 898, 187, 711, 6, 421, 430, 951, 114, 863, 951, 890, 816, 146, 304, 48, 96, 187, 267, 562, 442, 944, 191, 369, 699, 864, 886, 400, 274, 50, 343, 964, 421, 805, 487, 76, 847, 908, 374});

        assertTrue("Fehler: Element 76 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(76));
        assertFalse("Fehler: Element 290 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(290));
    }

    public static void test6Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(613);
        Tester.verifyListContents(theTestList, new int[]{613});

        assertFalse("Fehler: Element 501 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(501));
        assertFalse("Fehler: Element 341 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(341));
        theTestList.append(454);
        Tester.verifyListContents(theTestList, new int[]{613, 454});

        theTestList.append(708);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708});

        assertFalse("Fehler: Element 755 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(755));
        assertTrue("Fehler: Element 708 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(708));
        theTestList.append(64);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64});

        theTestList.append(581);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581});

        theTestList.append(909);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909});

        assertTrue("Fehler: Element 708 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(708));
        theTestList.append(388);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388});

        theTestList.append(550);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550});

        theTestList.append(761);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761});

        theTestList.append(214);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214});

        assertTrue("Fehler: Element 550 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(550));
        theTestList.append(506);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506});

        theTestList.append(672);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672});

        assertFalse("Fehler: Element 25 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(25));
        assertFalse("Fehler: Element 893 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(893));
        assertFalse("Fehler: Element 406 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(406));
        theTestList.append(553);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553});

        theTestList.append(472);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472});

        assertTrue("Fehler: Element 214 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(214));
        theTestList.append(447);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447});

        assertTrue("Fehler: Element 454 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(454));
        assertTrue("Fehler: Element 506 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(506));
        theTestList.append(790);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790});

        theTestList.append(732);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732});

        assertTrue("Fehler: Element 447 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(447));
        assertTrue("Fehler: Element 214 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(214));
        theTestList.append(250);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250});

        assertFalse("Fehler: Element 904 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(904));
        assertFalse("Fehler: Element 402 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(402));
        assertFalse("Fehler: Element 94 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(94));
        theTestList.append(89);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89});

        assertTrue("Fehler: Element 672 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(672));
        assertTrue("Fehler: Element 250 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(250));
        theTestList.append(308);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308});

        theTestList.append(154);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154});

        assertFalse("Fehler: Element 76 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(76));
        theTestList.append(187);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187});

        assertFalse("Fehler: Element 762 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(762));
        theTestList.append(159);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159});

        theTestList.append(993);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993});

        assertFalse("Fehler: Element 269 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(269));
        theTestList.append(341);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341});

        assertFalse("Fehler: Element 717 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(717));
        assertTrue("Fehler: Element 388 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(388));
        assertFalse("Fehler: Element 74 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(74));
        theTestList.append(300);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300});

        theTestList.append(203);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203});

        theTestList.append(932);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932});

        assertTrue("Fehler: Element 187 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(187));
        assertFalse("Fehler: Element 348 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(348));
        theTestList.append(940);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940});

        assertFalse("Fehler: Element 201 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(201));
        assertTrue("Fehler: Element 472 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(472));
        theTestList.append(836);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836});

        assertFalse("Fehler: Element 889 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(889));
        assertFalse("Fehler: Element 798 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(798));
        theTestList.append(131);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836, 131});

        assertTrue("Fehler: Element 64 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(64));
        theTestList.append(837);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836, 131, 837});

        assertFalse("Fehler: Element 745 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(745));
        theTestList.append(945);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836, 131, 837, 945});

        assertTrue("Fehler: Element 187 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(187));
        assertTrue("Fehler: Element 837 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(837));
        assertFalse("Fehler: Element 162 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(162));
        assertFalse("Fehler: Element 969 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(969));
        theTestList.append(347);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836, 131, 837, 945, 347});

        assertFalse("Fehler: Element 833 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(833));
        theTestList.append(396);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836, 131, 837, 945, 347, 396});

        assertTrue("Fehler: Element 341 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(341));
        theTestList.append(797);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836, 131, 837, 945, 347, 396, 797});

        theTestList.append(79);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836, 131, 837, 945, 347, 396, 797, 79});

        assertTrue("Fehler: Element 672 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(672));
        theTestList.append(697);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836, 131, 837, 945, 347, 396, 797, 79, 697});

        theTestList.append(474);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836, 131, 837, 945, 347, 396, 797, 79, 697, 474});

        assertFalse("Fehler: Element 498 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(498));
        theTestList.append(302);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836, 131, 837, 945, 347, 396, 797, 79, 697, 474, 302});

        assertTrue("Fehler: Element 454 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(454));
        theTestList.append(121);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836, 131, 837, 945, 347, 396, 797, 79, 697, 474, 302, 121});

        assertFalse("Fehler: Element 618 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(618));
        assertFalse("Fehler: Element 265 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(265));
        assertFalse("Fehler: Element 530 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(530));
        assertFalse("Fehler: Element 517 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(517));
        assertTrue("Fehler: Element 214 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(214));
        assertTrue("Fehler: Element 672 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(672));
        theTestList.append(707);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836, 131, 837, 945, 347, 396, 797, 79, 697, 474, 302, 121, 707});

        theTestList.append(861);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836, 131, 837, 945, 347, 396, 797, 79, 697, 474, 302, 121, 707, 861});

        assertTrue("Fehler: Element 836 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(836));
        assertFalse("Fehler: Element 955 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(955));
        assertTrue("Fehler: Element 672 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(672));
        assertFalse("Fehler: Element 270 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(270));
        assertTrue("Fehler: Element 89 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(89));
        theTestList.append(256);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836, 131, 837, 945, 347, 396, 797, 79, 697, 474, 302, 121, 707, 861, 256});

        assertFalse("Fehler: Element 501 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(501));
        theTestList.append(161);
        Tester.verifyListContents(theTestList, new int[]{613, 454, 708, 64, 581, 909, 388, 550, 761, 214, 506, 672, 553, 472, 447, 790, 732, 250, 89, 308, 154, 187, 159, 993, 341, 300, 203, 932, 940, 836, 131, 837, 945, 347, 396, 797, 79, 697, 474, 302, 121, 707, 861, 256, 161});

        assertTrue("Fehler: Element 672 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(672));
    }

    public static void test7Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(151);
        Tester.verifyListContents(theTestList, new int[]{151});

        assertFalse("Fehler: Element 770 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(770));
        assertFalse("Fehler: Element 447 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(447));
        assertTrue("Fehler: Element 151 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(151));
        theTestList.append(313);
        Tester.verifyListContents(theTestList, new int[]{151, 313});

        theTestList.append(964);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964});

        theTestList.append(951);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951});

        assertTrue("Fehler: Element 951 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(951));
        theTestList.append(375);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375});

        assertTrue("Fehler: Element 964 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(964));
        theTestList.append(976);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976});

        assertTrue("Fehler: Element 976 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(976));
        theTestList.append(54);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54});

        assertFalse("Fehler: Element 667 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(667));
        theTestList.append(530);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530});

        theTestList.append(91);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91});

        assertTrue("Fehler: Element 530 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(530));
        theTestList.append(714);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714});

        theTestList.append(979);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979});

        assertTrue("Fehler: Element 964 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(964));
        theTestList.append(918);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918});

        theTestList.append(511);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511});

        assertTrue("Fehler: Element 511 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(511));
        theTestList.append(785);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785});

        theTestList.append(749);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749});

        assertFalse("Fehler: Element 264 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(264));
        theTestList.append(139);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139});

        assertTrue("Fehler: Element 951 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(951));
        assertTrue("Fehler: Element 785 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(785));
        assertFalse("Fehler: Element 260 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(260));
        assertFalse("Fehler: Element 483 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(483));
        theTestList.append(554);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554});

        assertFalse("Fehler: Element 789 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(789));
        assertTrue("Fehler: Element 54 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(54));
        theTestList.append(64);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64});

        assertFalse("Fehler: Element 978 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(978));
        assertTrue("Fehler: Element 151 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(151));
        assertTrue("Fehler: Element 313 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(313));
        assertTrue("Fehler: Element 139 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(139));
        theTestList.append(135);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135});

        assertTrue("Fehler: Element 54 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(54));
        theTestList.append(339);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339});

        theTestList.append(591);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591});

        assertTrue("Fehler: Element 339 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(339));
        assertTrue("Fehler: Element 979 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(979));
        theTestList.append(995);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995});

        assertTrue("Fehler: Element 511 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(511));
        theTestList.append(160);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160});

        theTestList.append(80);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80});

        theTestList.append(767);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767});

        theTestList.append(105);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105});

        theTestList.append(189);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189});

        assertTrue("Fehler: Element 530 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(530));
        theTestList.append(200);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200});

        assertTrue("Fehler: Element 80 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(80));
        assertFalse("Fehler: Element 500 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(500));
        assertTrue("Fehler: Element 105 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(105));
        assertFalse("Fehler: Element 37 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(37));
        assertFalse("Fehler: Element 635 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(635));
        assertTrue("Fehler: Element 995 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(995));
        assertFalse("Fehler: Element 537 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(537));
        theTestList.append(697);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697});

        theTestList.append(592);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592});

        theTestList.append(399);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399});

        assertFalse("Fehler: Element 938 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(938));
        theTestList.append(168);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168});

        assertTrue("Fehler: Element 592 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(592));
        assertTrue("Fehler: Element 64 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(64));
        assertFalse("Fehler: Element 32 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(32));
        theTestList.append(656);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656});

        assertTrue("Fehler: Element 979 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(979));
        theTestList.append(863);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863});

        assertFalse("Fehler: Element 457 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(457));
        theTestList.append(37);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863, 37});

        assertTrue("Fehler: Element 200 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(200));
        assertFalse("Fehler: Element 136 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(136));
        assertTrue("Fehler: Element 964 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(964));
        theTestList.append(842);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863, 37, 842});

        theTestList.append(667);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863, 37, 842, 667});

        theTestList.append(909);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863, 37, 842, 667, 909});

        theTestList.append(927);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863, 37, 842, 667, 909, 927});

        theTestList.append(513);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863, 37, 842, 667, 909, 927, 513});

        assertFalse("Fehler: Element 84 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(84));
        theTestList.append(584);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863, 37, 842, 667, 909, 927, 513, 584});

        theTestList.append(970);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863, 37, 842, 667, 909, 927, 513, 584, 970});

        theTestList.append(754);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863, 37, 842, 667, 909, 927, 513, 584, 970, 754});

        theTestList.append(695);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863, 37, 842, 667, 909, 927, 513, 584, 970, 754, 695});

        theTestList.append(248);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863, 37, 842, 667, 909, 927, 513, 584, 970, 754, 695, 248});

        assertTrue("Fehler: Element 918 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(918));
        assertFalse("Fehler: Element 34 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(34));
        assertTrue("Fehler: Element 667 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(667));
        theTestList.append(378);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863, 37, 842, 667, 909, 927, 513, 584, 970, 754, 695, 248, 378});

        theTestList.append(138);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863, 37, 842, 667, 909, 927, 513, 584, 970, 754, 695, 248, 378, 138});

        assertFalse("Fehler: Element 231 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(231));
        theTestList.append(71);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863, 37, 842, 667, 909, 927, 513, 584, 970, 754, 695, 248, 378, 138, 71});

        assertFalse("Fehler: Element 1 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(1));
        assertFalse("Fehler: Element 721 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(721));
        assertFalse("Fehler: Element 895 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(895));
        theTestList.append(597);
        Tester.verifyListContents(theTestList, new int[]{151, 313, 964, 951, 375, 976, 54, 530, 91, 714, 979, 918, 511, 785, 749, 139, 554, 64, 135, 339, 591, 995, 160, 80, 767, 105, 189, 200, 697, 592, 399, 168, 656, 863, 37, 842, 667, 909, 927, 513, 584, 970, 754, 695, 248, 378, 138, 71, 597});

        assertFalse("Fehler: Element 34 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(34));
    }

    public static void test8Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(239);
        Tester.verifyListContents(theTestList, new int[]{239});

        assertTrue("Fehler: Element 239 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(239));
        theTestList.append(763);
        Tester.verifyListContents(theTestList, new int[]{239, 763});

        theTestList.append(681);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681});

        theTestList.append(49);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49});

        theTestList.append(100);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100});

        theTestList.append(394);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394});

        assertFalse("Fehler: Element 950 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(950));
        assertFalse("Fehler: Element 895 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(895));
        assertFalse("Fehler: Element 757 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(757));
        assertFalse("Fehler: Element 321 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(321));
        theTestList.append(187);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187});

        theTestList.append(763);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763});

        assertTrue("Fehler: Element 100 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(100));
        theTestList.append(3);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3});

        theTestList.append(623);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623});

        theTestList.append(53);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53});

        theTestList.append(468);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468});

        theTestList.append(194);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194});

        theTestList.append(373);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373});

        assertFalse("Fehler: Element 809 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(809));
        assertFalse("Fehler: Element 29 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(29));
        assertFalse("Fehler: Element 456 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(456));
        theTestList.append(633);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633});

        assertTrue("Fehler: Element 681 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(681));
        theTestList.append(875);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875});

        theTestList.append(294);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294});

        theTestList.append(995);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995});

        assertTrue("Fehler: Element 100 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(100));
        theTestList.append(308);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308});

        theTestList.append(354);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354});

        assertFalse("Fehler: Element 900 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(900));
        theTestList.append(783);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783});

        assertTrue("Fehler: Element 633 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(633));
        theTestList.append(387);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387});

        assertTrue("Fehler: Element 239 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(239));
        assertTrue("Fehler: Element 294 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(294));
        theTestList.append(947);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947});

        assertTrue("Fehler: Element 53 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(53));
        theTestList.append(832);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832});

        theTestList.append(689);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689});

        assertTrue("Fehler: Element 468 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(468));
        assertTrue("Fehler: Element 187 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(187));
        theTestList.append(872);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872});

        assertFalse("Fehler: Element 622 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(622));
        theTestList.append(643);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643});

        assertTrue("Fehler: Element 239 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(239));
        theTestList.append(115);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115});

        assertTrue("Fehler: Element 308 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(308));
        theTestList.append(493);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493});

        theTestList.append(118);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118});

        assertTrue("Fehler: Element 689 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(689));
        assertFalse("Fehler: Element 571 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(571));
        theTestList.append(207);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207});

        theTestList.append(610);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610});

        assertFalse("Fehler: Element 599 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(599));
        theTestList.append(694);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694});

        assertTrue("Fehler: Element 194 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(194));
        assertTrue("Fehler: Element 872 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(872));
        theTestList.append(197);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197});

        assertFalse("Fehler: Element 402 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(402));
        theTestList.append(384);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384});

        theTestList.append(471);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471});

        assertFalse("Fehler: Element 498 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(498));
        theTestList.append(434);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434});

        theTestList.append(353);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353});

        theTestList.append(69);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69});

        theTestList.append(263);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263});

        theTestList.append(946);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946});

        theTestList.append(933);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933});

        assertFalse("Fehler: Element 674 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(674));
        theTestList.append(650);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650});

        theTestList.append(183);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183});

        theTestList.append(369);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183, 369});

        theTestList.append(755);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183, 369, 755});

        assertTrue("Fehler: Element 118 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(118));
        theTestList.append(21);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183, 369, 755, 21});

        theTestList.append(122);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183, 369, 755, 21, 122});

        assertFalse("Fehler: Element 417 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(417));
        assertFalse("Fehler: Element 774 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(774));
        theTestList.append(931);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183, 369, 755, 21, 122, 931});

        assertTrue("Fehler: Element 294 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(294));
        theTestList.append(27);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183, 369, 755, 21, 122, 931, 27});

        assertFalse("Fehler: Element 342 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(342));
        assertTrue("Fehler: Element 493 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(493));
        theTestList.append(772);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183, 369, 755, 21, 122, 931, 27, 772});

        theTestList.append(698);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183, 369, 755, 21, 122, 931, 27, 772, 698});

        theTestList.append(912);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183, 369, 755, 21, 122, 931, 27, 772, 698, 912});

        assertFalse("Fehler: Element 464 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(464));
        assertTrue("Fehler: Element 832 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(832));
        theTestList.append(97);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183, 369, 755, 21, 122, 931, 27, 772, 698, 912, 97});

        assertFalse("Fehler: Element 279 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(279));
        theTestList.append(328);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183, 369, 755, 21, 122, 931, 27, 772, 698, 912, 97, 328});

        theTestList.append(463);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183, 369, 755, 21, 122, 931, 27, 772, 698, 912, 97, 328, 463});

        assertFalse("Fehler: Element 615 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(615));
        assertFalse("Fehler: Element 483 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(483));
        assertTrue("Fehler: Element 623 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(623));
        theTestList.append(400);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183, 369, 755, 21, 122, 931, 27, 772, 698, 912, 97, 328, 463, 400});

        theTestList.append(437);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183, 369, 755, 21, 122, 931, 27, 772, 698, 912, 97, 328, 463, 400, 437});

        theTestList.append(435);
        Tester.verifyListContents(theTestList, new int[]{239, 763, 681, 49, 100, 394, 187, 763, 3, 623, 53, 468, 194, 373, 633, 875, 294, 995, 308, 354, 783, 387, 947, 832, 689, 872, 643, 115, 493, 118, 207, 610, 694, 197, 384, 471, 434, 353, 69, 263, 946, 933, 650, 183, 369, 755, 21, 122, 931, 27, 772, 698, 912, 97, 328, 463, 400, 437, 435});

    }

    public static void test9Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(622);
        Tester.verifyListContents(theTestList, new int[]{622});

        assertFalse("Fehler: Element 678 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(678));
        theTestList.append(503);
        Tester.verifyListContents(theTestList, new int[]{622, 503});

        assertTrue("Fehler: Element 503 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(503));
        theTestList.append(940);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940});

        theTestList.append(588);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588});

        assertFalse("Fehler: Element 462 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(462));
        assertTrue("Fehler: Element 503 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(503));
        assertTrue("Fehler: Element 940 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(940));
        assertTrue("Fehler: Element 622 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(622));
        assertFalse("Fehler: Element 490 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(490));
        assertFalse("Fehler: Element 730 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(730));
        assertFalse("Fehler: Element 445 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(445));
        theTestList.append(575);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575});

        theTestList.append(468);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468});

        theTestList.append(283);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283});

        theTestList.append(162);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162});

        theTestList.append(103);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103});

        assertFalse("Fehler: Element 855 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(855));
        theTestList.append(341);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341});

        assertFalse("Fehler: Element 184 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(184));
        theTestList.append(590);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590});

        theTestList.append(940);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940});

        assertFalse("Fehler: Element 51 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(51));
        theTestList.append(389);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389});

        assertTrue("Fehler: Element 622 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(622));
        assertFalse("Fehler: Element 253 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(253));
        assertTrue("Fehler: Element 389 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(389));
        theTestList.append(282);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282});

        theTestList.append(666);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666});

        assertFalse("Fehler: Element 443 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(443));
        theTestList.append(380);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380});

        theTestList.append(659);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659});

        theTestList.append(554);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554});

        theTestList.append(820);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820});

        theTestList.append(508);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508});

        theTestList.append(435);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435});

        theTestList.append(547);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547});

        assertFalse("Fehler: Element 914 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(914));
        assertTrue("Fehler: Element 554 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(554));
        assertTrue("Fehler: Element 503 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(503));
        theTestList.append(825);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825});

        theTestList.append(875);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875});

        theTestList.append(683);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683});

        assertFalse("Fehler: Element 318 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(318));
        assertFalse("Fehler: Element 604 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(604));
        theTestList.append(479);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479});

        theTestList.append(828);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828});

        theTestList.append(111);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111});

        assertFalse("Fehler: Element 293 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(293));
        assertFalse("Fehler: Element 699 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(699));
        assertFalse("Fehler: Element 697 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(697));
        theTestList.append(686);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686});

        assertTrue("Fehler: Element 575 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(575));
        theTestList.append(731);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731});

        theTestList.append(183);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183});

        assertFalse("Fehler: Element 196 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(196));
        theTestList.append(902);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902});

        assertFalse("Fehler: Element 198 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(198));
        theTestList.append(921);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921});

        theTestList.append(22);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22});

        theTestList.append(146);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146});

        theTestList.append(23);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23});

        assertFalse("Fehler: Element 696 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(696));
        theTestList.append(551);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551});

        assertFalse("Fehler: Element 518 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(518));
        theTestList.append(792);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792});

        assertTrue("Fehler: Element 666 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(666));
        theTestList.append(79);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792, 79});

        theTestList.append(658);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792, 79, 658});

        assertFalse("Fehler: Element 873 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(873));
        assertTrue("Fehler: Element 820 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(820));
        theTestList.append(389);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792, 79, 658, 389});

        theTestList.append(799);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792, 79, 658, 389, 799});

        assertTrue("Fehler: Element 103 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(103));
        assertFalse("Fehler: Element 795 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(795));
        assertTrue("Fehler: Element 820 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(820));
        assertTrue("Fehler: Element 341 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(341));
        theTestList.append(627);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792, 79, 658, 389, 799, 627});

        theTestList.append(807);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792, 79, 658, 389, 799, 627, 807});

        assertFalse("Fehler: Element 228 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(228));
        assertTrue("Fehler: Element 468 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(468));
        assertTrue("Fehler: Element 902 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(902));
        theTestList.append(289);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792, 79, 658, 389, 799, 627, 807, 289});

        theTestList.append(771);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792, 79, 658, 389, 799, 627, 807, 289, 771});

        assertFalse("Fehler: Element 994 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(994));
        assertTrue("Fehler: Element 146 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(146));
        theTestList.append(733);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792, 79, 658, 389, 799, 627, 807, 289, 771, 733});

        theTestList.append(357);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792, 79, 658, 389, 799, 627, 807, 289, 771, 733, 357});

        theTestList.append(262);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792, 79, 658, 389, 799, 627, 807, 289, 771, 733, 357, 262});

        assertFalse("Fehler: Element 99 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(99));
        assertTrue("Fehler: Element 554 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(554));
        theTestList.append(121);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792, 79, 658, 389, 799, 627, 807, 289, 771, 733, 357, 262, 121});

        assertTrue("Fehler: Element 590 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(590));
        assertFalse("Fehler: Element 450 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(450));
        theTestList.append(663);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792, 79, 658, 389, 799, 627, 807, 289, 771, 733, 357, 262, 121, 663});

        assertFalse("Fehler: Element 714 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(714));
        theTestList.append(392);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792, 79, 658, 389, 799, 627, 807, 289, 771, 733, 357, 262, 121, 663, 392});

        assertTrue("Fehler: Element 162 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(162));
        theTestList.append(962);
        Tester.verifyListContents(theTestList, new int[]{622, 503, 940, 588, 575, 468, 283, 162, 103, 341, 590, 940, 389, 282, 666, 380, 659, 554, 820, 508, 435, 547, 825, 875, 683, 479, 828, 111, 686, 731, 183, 902, 921, 22, 146, 23, 551, 792, 79, 658, 389, 799, 627, 807, 289, 771, 733, 357, 262, 121, 663, 392, 962});

    }

    public static void test10Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(671);
        Tester.verifyListContents(theTestList, new int[]{671});

        assertFalse("Fehler: Element 572 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(572));
        assertFalse("Fehler: Element 298 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(298));
        theTestList.append(895);
        Tester.verifyListContents(theTestList, new int[]{671, 895});

        theTestList.append(49);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49});

        theTestList.append(542);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542});

        theTestList.append(697);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697});

        assertTrue("Fehler: Element 895 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(895));
        theTestList.append(717);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717});

        theTestList.append(793);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793});

        assertFalse("Fehler: Element 917 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(917));
        theTestList.append(800);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800});

        assertFalse("Fehler: Element 247 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(247));
        theTestList.append(328);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328});

        theTestList.append(20);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20});

        theTestList.append(678);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678});

        theTestList.append(313);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313});

        assertTrue("Fehler: Element 542 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(542));
        assertFalse("Fehler: Element 998 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(998));
        theTestList.append(581);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581});

        theTestList.append(567);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567});

        theTestList.append(51);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51});

        theTestList.append(639);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639});

        theTestList.append(778);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778});

        assertFalse("Fehler: Element 708 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(708));
        assertFalse("Fehler: Element 632 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(632));
        assertFalse("Fehler: Element 406 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(406));
        theTestList.append(141);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141});

        theTestList.append(565);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565});

        assertFalse("Fehler: Element 987 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(987));
        assertFalse("Fehler: Element 499 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(499));
        assertTrue("Fehler: Element 20 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(20));
        theTestList.append(884);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884});

        theTestList.append(643);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643});

        theTestList.append(702);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702});

        assertFalse("Fehler: Element 208 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(208));
        assertTrue("Fehler: Element 313 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(313));
        theTestList.append(785);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785});

        theTestList.append(581);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581});

        theTestList.append(941);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941});

        assertFalse("Fehler: Element 576 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(576));
        theTestList.append(201);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201});

        theTestList.append(478);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478});

        assertTrue("Fehler: Element 565 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(565));
        assertTrue("Fehler: Element 565 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(565));
        assertTrue("Fehler: Element 793 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(793));
        theTestList.append(571);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571});

        theTestList.append(772);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772});

        theTestList.append(2);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2});

        theTestList.append(256);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256});

        theTestList.append(368);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368});

        assertFalse("Fehler: Element 299 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(299));
        assertTrue("Fehler: Element 793 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(793));
        assertTrue("Fehler: Element 581 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(581));
        assertTrue("Fehler: Element 785 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(785));
        assertTrue("Fehler: Element 2 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(2));
        assertFalse("Fehler: Element 937 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(937));
        theTestList.append(444);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444});

        theTestList.append(556);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556});

        assertFalse("Fehler: Element 45 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(45));
        theTestList.append(181);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181});

        theTestList.append(877);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877});

        assertTrue("Fehler: Element 778 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(778));
        theTestList.append(646);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646});

        theTestList.append(188);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188});

        assertTrue("Fehler: Element 556 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(556));
        assertFalse("Fehler: Element 942 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(942));
        assertTrue("Fehler: Element 702 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(702));
        assertTrue("Fehler: Element 51 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(51));
        theTestList.append(153);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188, 153});

        assertTrue("Fehler: Element 2 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(2));
        theTestList.append(115);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188, 153, 115});

        theTestList.append(987);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188, 153, 115, 987});

        theTestList.append(533);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188, 153, 115, 987, 533});

        assertFalse("Fehler: Element 876 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(876));
        theTestList.append(369);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188, 153, 115, 987, 533, 369});

        theTestList.append(774);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188, 153, 115, 987, 533, 369, 774});

        assertTrue("Fehler: Element 141 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(141));
        assertFalse("Fehler: Element 896 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(896));
        theTestList.append(733);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188, 153, 115, 987, 533, 369, 774, 733});

        assertTrue("Fehler: Element 20 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(20));
        theTestList.append(137);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188, 153, 115, 987, 533, 369, 774, 733, 137});

        assertFalse("Fehler: Element 916 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(916));
        theTestList.append(974);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188, 153, 115, 987, 533, 369, 774, 733, 137, 974});

        theTestList.append(961);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188, 153, 115, 987, 533, 369, 774, 733, 137, 974, 961});

        assertTrue("Fehler: Element 137 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(137));
        theTestList.append(944);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188, 153, 115, 987, 533, 369, 774, 733, 137, 974, 961, 944});

        theTestList.append(870);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188, 153, 115, 987, 533, 369, 774, 733, 137, 974, 961, 944, 870});

        assertFalse("Fehler: Element 384 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(384));
        theTestList.append(86);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188, 153, 115, 987, 533, 369, 774, 733, 137, 974, 961, 944, 870, 86});

        assertFalse("Fehler: Element 477 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(477));
        assertTrue("Fehler: Element 671 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(671));
        assertFalse("Fehler: Element 662 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(662));
        assertFalse("Fehler: Element 64 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(64));
        theTestList.append(359);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188, 153, 115, 987, 533, 369, 774, 733, 137, 974, 961, 944, 870, 86, 359});

        assertFalse("Fehler: Element 755 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(755));
        theTestList.append(872);
        Tester.verifyListContents(theTestList, new int[]{671, 895, 49, 542, 697, 717, 793, 800, 328, 20, 678, 313, 581, 567, 51, 639, 778, 141, 565, 884, 643, 702, 785, 581, 941, 201, 478, 571, 772, 2, 256, 368, 444, 556, 181, 877, 646, 188, 153, 115, 987, 533, 369, 774, 733, 137, 974, 961, 944, 870, 86, 359, 872});

        assertTrue("Fehler: Element 201 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(201));
        assertFalse("Fehler: Element 140 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(140));
        assertFalse("Fehler: Element 940 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(940));
    }
}
