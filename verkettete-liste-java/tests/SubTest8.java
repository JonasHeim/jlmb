package tests;

import static org.junit.Assert.*;

import liste.*;

/**
 * Die Test-Klasse SubTest8.
 *
 * @author Rainer Helfrich
 * @version 03.10.2020
 */
public class SubTest8 {


    public static void test1Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(801);
        Tester.verifyListContents(theTestList, new int[]{801});

        theTestList.insertAt(0, 658);
        Tester.verifyListContents(theTestList, new int[]{658, 801});

        theTestList.append(896);
        Tester.verifyListContents(theTestList, new int[]{658, 801, 896});

        theTestList.insertAt(0, 618);
        Tester.verifyListContents(theTestList, new int[]{618, 658, 801, 896});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{618, 658, 801});

        assertTrue("Fehler: Element 658 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(658));
        theTestList.insertAt(1, 715);
        Tester.verifyListContents(theTestList, new int[]{618, 715, 658, 801});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{618, 715, 658});

        theTestList.append(888);
        Tester.verifyListContents(theTestList, new int[]{618, 715, 658, 888});

        theTestList.insertAt(0, 224);
        Tester.verifyListContents(theTestList, new int[]{224, 618, 715, 658, 888});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{224, 618, 715, 888});

        theTestList.insertAt(3, 550);
        Tester.verifyListContents(theTestList, new int[]{224, 618, 715, 550, 888});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{224, 618, 715, 550});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{224, 715, 550});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{715, 550});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{715});

        theTestList.insertAt(0, 109);
        Tester.verifyListContents(theTestList, new int[]{109, 715});

        theTestList.append(687);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687});

        theTestList.append(939);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 939});

        theTestList.append(967);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 939, 967});

        theTestList.append(40);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 939, 967, 40});

        assertFalse("Fehler: Element 879 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(879));
        theTestList.insertAt(6, 55);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 939, 967, 40, 55});

        theTestList.append(80);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 939, 967, 40, 55, 80});

        assertTrue("Fehler: Element 939 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(939));
        theTestList.insertAt(4, 357);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 939, 357, 967, 40, 55, 80});

        theTestList.append(245);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 939, 357, 967, 40, 55, 80, 245});

        assertFalse("Fehler: Element 311 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(311));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 357, 967, 40, 55, 80, 245});

        theTestList.append(395);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 357, 967, 40, 55, 80, 245, 395});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 357, 967, 40, 80, 245, 395});

        theTestList.append(437);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 357, 967, 40, 80, 245, 395, 437});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 357, 40, 80, 245, 395, 437});

        theTestList.append(494);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 357, 40, 80, 245, 395, 437, 494});

        assertFalse("Fehler: Element 762 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(762));
        theTestList.insertAt(8, 282);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 357, 40, 80, 245, 395, 282, 437, 494});

        assertTrue("Fehler: Element 40 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(40));
        assertFalse("Fehler: Element 855 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(855));
        assertTrue("Fehler: Element 40 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(40));
        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 357, 40, 80, 245, 395, 282, 437});

        assertTrue("Fehler: Element 395 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(395));
        theTestList.insertAt(8, 123);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 357, 40, 80, 245, 395, 123, 282, 437});

        theTestList.append(82);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 357, 40, 80, 245, 395, 123, 282, 437, 82});

        theTestList.append(437);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 357, 40, 80, 245, 395, 123, 282, 437, 82, 437});

        theTestList.append(77);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 357, 40, 80, 245, 395, 123, 282, 437, 82, 437, 77});

        assertTrue("Fehler: Element 77 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(77));
        theTestList.insertAt(6, 155);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 357, 40, 80, 155, 245, 395, 123, 282, 437, 82, 437, 77});

        theTestList.append(570);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 687, 357, 40, 80, 155, 245, 395, 123, 282, 437, 82, 437, 77, 570});

        assertFalse("Fehler: Element 456 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(456));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 357, 40, 80, 155, 245, 395, 123, 282, 437, 82, 437, 77, 570});

        assertFalse("Fehler: Element 379 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(379));
        assertTrue("Fehler: Element 40 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(40));
        theTestList.insertAt(11, 585);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 357, 40, 80, 155, 245, 395, 123, 282, 437, 585, 82, 437, 77, 570});

        assertFalse("Fehler: Element 303 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(303));
        assertTrue("Fehler: Element 40 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(40));
        assertFalse("Fehler: Element 306 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(306));
        theTestList.removeAt(15);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 357, 40, 80, 155, 245, 395, 123, 282, 437, 585, 82, 437, 77});

        assertFalse("Fehler: Element 560 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(560));
        theTestList.insertAt(12, 53);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 357, 40, 80, 155, 245, 395, 123, 282, 437, 585, 53, 82, 437, 77});

        theTestList.insertAt(0, 512);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 357, 40, 80, 155, 245, 395, 123, 282, 437, 585, 53, 82, 437, 77});

        assertTrue("Fehler: Element 715 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(715));
        theTestList.insertAt(1, 142);
        Tester.verifyListContents(theTestList, new int[]{512, 142, 109, 715, 357, 40, 80, 155, 245, 395, 123, 282, 437, 585, 53, 82, 437, 77});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 357, 40, 80, 155, 245, 395, 123, 282, 437, 585, 53, 82, 437, 77});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 357, 40, 80, 245, 395, 123, 282, 437, 585, 53, 82, 437, 77});

        theTestList.append(877);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 357, 40, 80, 245, 395, 123, 282, 437, 585, 53, 82, 437, 77, 877});

        theTestList.append(853);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 357, 40, 80, 245, 395, 123, 282, 437, 585, 53, 82, 437, 77, 877, 853});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 357, 40, 80, 395, 123, 282, 437, 585, 53, 82, 437, 77, 877, 853});

        assertTrue("Fehler: Element 437 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(437));
        theTestList.append(797);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 357, 40, 80, 395, 123, 282, 437, 585, 53, 82, 437, 77, 877, 853, 797});

        assertTrue("Fehler: Element 877 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(877));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 40, 80, 395, 123, 282, 437, 585, 53, 82, 437, 77, 877, 853, 797});

        assertTrue("Fehler: Element 40 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(40));
        theTestList.removeAt(13);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 40, 80, 395, 123, 282, 437, 585, 53, 82, 437, 877, 853, 797});

        theTestList.append(405);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 40, 80, 395, 123, 282, 437, 585, 53, 82, 437, 877, 853, 797, 405});

        theTestList.insertAt(3, 454);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 454, 40, 80, 395, 123, 282, 437, 585, 53, 82, 437, 877, 853, 797, 405});

        theTestList.append(642);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 454, 40, 80, 395, 123, 282, 437, 585, 53, 82, 437, 877, 853, 797, 405, 642});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 454, 40, 80, 123, 282, 437, 585, 53, 82, 437, 877, 853, 797, 405, 642});

        theTestList.append(438);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 454, 40, 80, 123, 282, 437, 585, 53, 82, 437, 877, 853, 797, 405, 642, 438});

        theTestList.removeAt(14);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 454, 40, 80, 123, 282, 437, 585, 53, 82, 437, 877, 797, 405, 642, 438});

        assertTrue("Fehler: Element 282 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(282));
        theTestList.removeAt(17);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 454, 40, 80, 123, 282, 437, 585, 53, 82, 437, 877, 797, 405, 642});

        theTestList.insertAt(15, 425);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 454, 40, 80, 123, 282, 437, 585, 53, 82, 437, 877, 797, 425, 405, 642});

        assertFalse("Fehler: Element 61 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(61));
        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 454, 40, 80, 123, 437, 585, 53, 82, 437, 877, 797, 425, 405, 642});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 454, 40, 123, 437, 585, 53, 82, 437, 877, 797, 425, 405, 642});

        theTestList.insertAt(15, 75);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 454, 40, 123, 437, 585, 53, 82, 437, 877, 797, 425, 405, 75, 642});

        theTestList.append(22);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 454, 40, 123, 437, 585, 53, 82, 437, 877, 797, 425, 405, 75, 642, 22});

        theTestList.append(25);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 454, 40, 123, 437, 585, 53, 82, 437, 877, 797, 425, 405, 75, 642, 22, 25});

        assertFalse("Fehler: Element 752 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(752));
        assertTrue("Fehler: Element 405 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(405));
        theTestList.append(591);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 454, 40, 123, 437, 585, 53, 82, 437, 877, 797, 425, 405, 75, 642, 22, 25, 591});

        assertFalse("Fehler: Element 161 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(161));
        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{512, 109, 715, 454, 40, 123, 437, 585, 53, 82, 877, 797, 425, 405, 75, 642, 22, 25, 591});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{109, 715, 454, 40, 123, 437, 585, 53, 82, 877, 797, 425, 405, 75, 642, 22, 25, 591});

        theTestList.insertAt(0, 181);
        Tester.verifyListContents(theTestList, new int[]{181, 109, 715, 454, 40, 123, 437, 585, 53, 82, 877, 797, 425, 405, 75, 642, 22, 25, 591});

        theTestList.append(145);
        Tester.verifyListContents(theTestList, new int[]{181, 109, 715, 454, 40, 123, 437, 585, 53, 82, 877, 797, 425, 405, 75, 642, 22, 25, 591, 145});

        theTestList.insertAt(6, 117);
        Tester.verifyListContents(theTestList, new int[]{181, 109, 715, 454, 40, 123, 117, 437, 585, 53, 82, 877, 797, 425, 405, 75, 642, 22, 25, 591, 145});

        theTestList.insertAt(6, 495);
        Tester.verifyListContents(theTestList, new int[]{181, 109, 715, 454, 40, 123, 495, 117, 437, 585, 53, 82, 877, 797, 425, 405, 75, 642, 22, 25, 591, 145});

        theTestList.insertAt(15, 918);
        Tester.verifyListContents(theTestList, new int[]{181, 109, 715, 454, 40, 123, 495, 117, 437, 585, 53, 82, 877, 797, 425, 918, 405, 75, 642, 22, 25, 591, 145});

        assertFalse("Fehler: Element 39 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(39));
    }

    public static void test2Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(449);
        Tester.verifyListContents(theTestList, new int[]{449});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(136);
        Tester.verifyListContents(theTestList, new int[]{136});

        assertTrue("Fehler: Element 136 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(136));
        theTestList.insertAt(0, 783);
        Tester.verifyListContents(theTestList, new int[]{783, 136});

        theTestList.append(630);
        Tester.verifyListContents(theTestList, new int[]{783, 136, 630});

        assertTrue("Fehler: Element 783 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(783));
        theTestList.insertAt(2, 107);
        Tester.verifyListContents(theTestList, new int[]{783, 136, 107, 630});

        assertFalse("Fehler: Element 697 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(697));
        theTestList.insertAt(0, 986);
        Tester.verifyListContents(theTestList, new int[]{986, 783, 136, 107, 630});

        theTestList.append(441);
        Tester.verifyListContents(theTestList, new int[]{986, 783, 136, 107, 630, 441});

        theTestList.insertAt(1, 403);
        Tester.verifyListContents(theTestList, new int[]{986, 403, 783, 136, 107, 630, 441});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{986, 403, 783, 136, 107, 441});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{986, 403, 783, 107, 441});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{986, 403, 107, 441});

        theTestList.append(758);
        Tester.verifyListContents(theTestList, new int[]{986, 403, 107, 441, 758});

        assertTrue("Fehler: Element 986 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(986));
        theTestList.insertAt(1, 70);
        Tester.verifyListContents(theTestList, new int[]{986, 70, 403, 107, 441, 758});

        theTestList.append(19);
        Tester.verifyListContents(theTestList, new int[]{986, 70, 403, 107, 441, 758, 19});

        theTestList.append(465);
        Tester.verifyListContents(theTestList, new int[]{986, 70, 403, 107, 441, 758, 19, 465});

        theTestList.insertAt(3, 602);
        Tester.verifyListContents(theTestList, new int[]{986, 70, 403, 602, 107, 441, 758, 19, 465});

        assertFalse("Fehler: Element 230 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(230));
        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{986, 70, 403, 602, 107, 441, 758, 465});

        assertTrue("Fehler: Element 758 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(758));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 623);
        Tester.verifyListContents(theTestList, new int[]{623});

        assertFalse("Fehler: Element 98 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(98));
        theTestList.insertAt(0, 96);
        Tester.verifyListContents(theTestList, new int[]{96, 623});

        assertTrue("Fehler: Element 623 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(623));
        theTestList.insertAt(2, 146);
        Tester.verifyListContents(theTestList, new int[]{96, 623, 146});

        theTestList.insertAt(3, 268);
        Tester.verifyListContents(theTestList, new int[]{96, 623, 146, 268});

        theTestList.insertAt(1, 195);
        Tester.verifyListContents(theTestList, new int[]{96, 195, 623, 146, 268});

        assertFalse("Fehler: Element 619 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(619));
        assertFalse("Fehler: Element 835 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(835));
        assertTrue("Fehler: Element 96 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(96));
        assertTrue("Fehler: Element 195 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(195));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{96, 195, 146, 268});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{96, 195, 146});

        theTestList.insertAt(2, 115);
        Tester.verifyListContents(theTestList, new int[]{96, 195, 115, 146});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{96, 115, 146});

        theTestList.insertAt(3, 850);
        Tester.verifyListContents(theTestList, new int[]{96, 115, 146, 850});

        assertFalse("Fehler: Element 944 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(944));
        theTestList.insertAt(4, 532);
        Tester.verifyListContents(theTestList, new int[]{96, 115, 146, 850, 532});

        theTestList.insertAt(1, 95);
        Tester.verifyListContents(theTestList, new int[]{96, 95, 115, 146, 850, 532});

        theTestList.append(6);
        Tester.verifyListContents(theTestList, new int[]{96, 95, 115, 146, 850, 532, 6});

        theTestList.append(631);
        Tester.verifyListContents(theTestList, new int[]{96, 95, 115, 146, 850, 532, 6, 631});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{96, 95, 146, 850, 532, 6, 631});

        assertFalse("Fehler: Element 695 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(695));
        theTestList.append(257);
        Tester.verifyListContents(theTestList, new int[]{96, 95, 146, 850, 532, 6, 631, 257});

        assertFalse("Fehler: Element 772 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(772));
        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{96, 95, 146, 850, 532, 6, 257});

        assertFalse("Fehler: Element 833 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(833));
        assertTrue("Fehler: Element 146 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(146));
        theTestList.insertAt(3, 655);
        Tester.verifyListContents(theTestList, new int[]{96, 95, 146, 655, 850, 532, 6, 257});

        theTestList.append(358);
        Tester.verifyListContents(theTestList, new int[]{96, 95, 146, 655, 850, 532, 6, 257, 358});

        assertFalse("Fehler: Element 249 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(249));
        theTestList.append(154);
        Tester.verifyListContents(theTestList, new int[]{96, 95, 146, 655, 850, 532, 6, 257, 358, 154});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 584);
        Tester.verifyListContents(theTestList, new int[]{584});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(557);
        Tester.verifyListContents(theTestList, new int[]{557});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 520);
        Tester.verifyListContents(theTestList, new int[]{520});

// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 26);
        Tester.verifyListContents(theTestList, new int[]{26});

        assertTrue("Fehler: Element 26 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(26));
        theTestList.insertAt(0, 791);
        Tester.verifyListContents(theTestList, new int[]{791, 26});

        theTestList.insertAt(2, 198);
        Tester.verifyListContents(theTestList, new int[]{791, 26, 198});

        assertFalse("Fehler: Element 29 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(29));
        assertFalse("Fehler: Element 876 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(876));
        theTestList.insertAt(1, 78);
        Tester.verifyListContents(theTestList, new int[]{791, 78, 26, 198});

        theTestList.append(925);
        Tester.verifyListContents(theTestList, new int[]{791, 78, 26, 198, 925});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{791, 26, 198, 925});

        theTestList.append(568);
        Tester.verifyListContents(theTestList, new int[]{791, 26, 198, 925, 568});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{791, 26, 198, 568});

        assertFalse("Fehler: Element 847 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(847));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{791, 198, 568});

        theTestList.insertAt(3, 436);
        Tester.verifyListContents(theTestList, new int[]{791, 198, 568, 436});

        theTestList.append(137);
        Tester.verifyListContents(theTestList, new int[]{791, 198, 568, 436, 137});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 623);
        Tester.verifyListContents(theTestList, new int[]{623});

        theTestList.append(731);
        Tester.verifyListContents(theTestList, new int[]{623, 731});

        theTestList.insertAt(1, 808);
        Tester.verifyListContents(theTestList, new int[]{623, 808, 731});

        theTestList.append(329);
        Tester.verifyListContents(theTestList, new int[]{623, 808, 731, 329});

        theTestList.append(33);
        Tester.verifyListContents(theTestList, new int[]{623, 808, 731, 329, 33});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{623, 808, 329, 33});

        assertFalse("Fehler: Element 617 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(617));
        assertTrue("Fehler: Element 623 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(623));
        theTestList.append(620);
        Tester.verifyListContents(theTestList, new int[]{623, 808, 329, 33, 620});

        theTestList.insertAt(0, 425);
        Tester.verifyListContents(theTestList, new int[]{425, 623, 808, 329, 33, 620});

        theTestList.append(718);
        Tester.verifyListContents(theTestList, new int[]{425, 623, 808, 329, 33, 620, 718});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{623, 808, 329, 33, 620, 718});

        theTestList.append(951);
        Tester.verifyListContents(theTestList, new int[]{623, 808, 329, 33, 620, 718, 951});

        theTestList.insertAt(5, 928);
        Tester.verifyListContents(theTestList, new int[]{623, 808, 329, 33, 620, 928, 718, 951});

        assertFalse("Fehler: Element 390 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(390));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{808, 329, 33, 620, 928, 718, 951});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{329, 33, 620, 928, 718, 951});

        theTestList.insertAt(2, 897);
        Tester.verifyListContents(theTestList, new int[]{329, 33, 897, 620, 928, 718, 951});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{329, 33, 897, 620, 718, 951});

    }

    public static void test3Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(414);
        Tester.verifyListContents(theTestList, new int[]{414});

        theTestList.append(521);
        Tester.verifyListContents(theTestList, new int[]{414, 521});

        theTestList.append(63);
        Tester.verifyListContents(theTestList, new int[]{414, 521, 63});

        assertFalse("Fehler: Element 265 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(265));
        theTestList.insertAt(1, 736);
        Tester.verifyListContents(theTestList, new int[]{414, 736, 521, 63});

        theTestList.append(724);
        Tester.verifyListContents(theTestList, new int[]{414, 736, 521, 63, 724});

        assertFalse("Fehler: Element 316 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(316));
        theTestList.insertAt(5, 890);
        Tester.verifyListContents(theTestList, new int[]{414, 736, 521, 63, 724, 890});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{414, 736, 521, 63, 724});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{414, 521, 63, 724});

        assertTrue("Fehler: Element 724 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(724));
        assertFalse("Fehler: Element 93 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(93));
        theTestList.append(157);
        Tester.verifyListContents(theTestList, new int[]{414, 521, 63, 724, 157});

        assertTrue("Fehler: Element 521 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(521));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{414, 63, 724, 157});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{63, 724, 157});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{63, 157});

        theTestList.insertAt(0, 533);
        Tester.verifyListContents(theTestList, new int[]{533, 63, 157});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{63, 157});

        theTestList.insertAt(2, 647);
        Tester.verifyListContents(theTestList, new int[]{63, 157, 647});

        assertFalse("Fehler: Element 781 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(781));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{157, 647});

        theTestList.append(386);
        Tester.verifyListContents(theTestList, new int[]{157, 647, 386});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{157, 647});

        theTestList.insertAt(2, 247);
        Tester.verifyListContents(theTestList, new int[]{157, 647, 247});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 629);
        Tester.verifyListContents(theTestList, new int[]{629});

        theTestList.insertAt(1, 5);
        Tester.verifyListContents(theTestList, new int[]{629, 5});

        theTestList.insertAt(2, 290);
        Tester.verifyListContents(theTestList, new int[]{629, 5, 290});

        theTestList.insertAt(1, 167);
        Tester.verifyListContents(theTestList, new int[]{629, 167, 5, 290});

        assertTrue("Fehler: Element 629 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(629));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{167, 5, 290});

        assertTrue("Fehler: Element 167 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(167));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{167, 290});

        assertTrue("Fehler: Element 290 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(290));
        assertFalse("Fehler: Element 762 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(762));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{290});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(748);
        Tester.verifyListContents(theTestList, new int[]{748});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(967);
        Tester.verifyListContents(theTestList, new int[]{967});

        assertTrue("Fehler: Element 967 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(967));
        theTestList.insertAt(1, 797);
        Tester.verifyListContents(theTestList, new int[]{967, 797});

        theTestList.append(445);
        Tester.verifyListContents(theTestList, new int[]{967, 797, 445});

        assertFalse("Fehler: Element 996 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(996));
        assertFalse("Fehler: Element 708 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(708));
        theTestList.append(328);
        Tester.verifyListContents(theTestList, new int[]{967, 797, 445, 328});

        theTestList.append(434);
        Tester.verifyListContents(theTestList, new int[]{967, 797, 445, 328, 434});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{967, 797, 328, 434});

        theTestList.insertAt(3, 183);
        Tester.verifyListContents(theTestList, new int[]{967, 797, 328, 183, 434});

        assertTrue("Fehler: Element 434 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(434));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{967, 797, 328, 183});

        theTestList.insertAt(2, 154);
        Tester.verifyListContents(theTestList, new int[]{967, 797, 154, 328, 183});

        theTestList.insertAt(1, 361);
        Tester.verifyListContents(theTestList, new int[]{967, 361, 797, 154, 328, 183});

        theTestList.append(171);
        Tester.verifyListContents(theTestList, new int[]{967, 361, 797, 154, 328, 183, 171});

        theTestList.insertAt(1, 153);
        Tester.verifyListContents(theTestList, new int[]{967, 153, 361, 797, 154, 328, 183, 171});

        assertFalse("Fehler: Element 966 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(966));
        assertTrue("Fehler: Element 967 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(967));
        theTestList.insertAt(0, 842);
        Tester.verifyListContents(theTestList, new int[]{842, 967, 153, 361, 797, 154, 328, 183, 171});

        assertTrue("Fehler: Element 154 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(154));
        theTestList.append(86);
        Tester.verifyListContents(theTestList, new int[]{842, 967, 153, 361, 797, 154, 328, 183, 171, 86});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{967, 153, 361, 797, 154, 328, 183, 171, 86});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{967, 153, 361, 797, 328, 183, 171, 86});

        theTestList.append(745);
        Tester.verifyListContents(theTestList, new int[]{967, 153, 361, 797, 328, 183, 171, 86, 745});

        assertFalse("Fehler: Element 283 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(283));
        theTestList.insertAt(7, 945);
        Tester.verifyListContents(theTestList, new int[]{967, 153, 361, 797, 328, 183, 171, 945, 86, 745});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{967, 153, 797, 328, 183, 171, 945, 86, 745});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{967, 797, 328, 183, 171, 945, 86, 745});

        assertTrue("Fehler: Element 171 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(171));
        assertTrue("Fehler: Element 171 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(171));
        assertTrue("Fehler: Element 171 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(171));
        assertFalse("Fehler: Element 772 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(772));
        assertTrue("Fehler: Element 745 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(745));
        theTestList.insertAt(5, 926);
        Tester.verifyListContents(theTestList, new int[]{967, 797, 328, 183, 171, 926, 945, 86, 745});

        theTestList.insertAt(0, 374);
        Tester.verifyListContents(theTestList, new int[]{374, 967, 797, 328, 183, 171, 926, 945, 86, 745});

        theTestList.insertAt(7, 68);
        Tester.verifyListContents(theTestList, new int[]{374, 967, 797, 328, 183, 171, 926, 68, 945, 86, 745});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{374, 967, 328, 183, 171, 926, 68, 945, 86, 745});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{374, 967, 183, 171, 926, 68, 945, 86, 745});

        assertFalse("Fehler: Element 695 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(695));
        assertTrue("Fehler: Element 86 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(86));
        assertFalse("Fehler: Element 378 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(378));
        theTestList.append(71);
        Tester.verifyListContents(theTestList, new int[]{374, 967, 183, 171, 926, 68, 945, 86, 745, 71});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{374, 183, 171, 926, 68, 945, 86, 745, 71});

        theTestList.append(615);
        Tester.verifyListContents(theTestList, new int[]{374, 183, 171, 926, 68, 945, 86, 745, 71, 615});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{374, 183, 171, 926, 68, 945, 745, 71, 615});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{374, 183, 171, 926, 945, 745, 71, 615});

        theTestList.insertAt(2, 529);
        Tester.verifyListContents(theTestList, new int[]{374, 183, 529, 171, 926, 945, 745, 71, 615});

        assertFalse("Fehler: Element 617 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(617));
        assertTrue("Fehler: Element 926 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(926));
        theTestList.append(335);
        Tester.verifyListContents(theTestList, new int[]{374, 183, 529, 171, 926, 945, 745, 71, 615, 335});

        assertTrue("Fehler: Element 945 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(945));
        theTestList.append(857);
        Tester.verifyListContents(theTestList, new int[]{374, 183, 529, 171, 926, 945, 745, 71, 615, 335, 857});

        theTestList.append(784);
        Tester.verifyListContents(theTestList, new int[]{374, 183, 529, 171, 926, 945, 745, 71, 615, 335, 857, 784});

        theTestList.append(248);
        Tester.verifyListContents(theTestList, new int[]{374, 183, 529, 171, 926, 945, 745, 71, 615, 335, 857, 784, 248});

        theTestList.removeAt(12);
        Tester.verifyListContents(theTestList, new int[]{374, 183, 529, 171, 926, 945, 745, 71, 615, 335, 857, 784});

        assertTrue("Fehler: Element 857 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(857));
        theTestList.append(717);
        Tester.verifyListContents(theTestList, new int[]{374, 183, 529, 171, 926, 945, 745, 71, 615, 335, 857, 784, 717});

        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{374, 183, 529, 171, 926, 945, 745, 71, 615, 335, 784, 717});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{374, 529, 171, 926, 945, 745, 71, 615, 335, 784, 717});

    }

    public static void test4Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 481);
        Tester.verifyListContents(theTestList, new int[]{481});

        theTestList.insertAt(0, 263);
        Tester.verifyListContents(theTestList, new int[]{263, 481});

        theTestList.insertAt(0, 85);
        Tester.verifyListContents(theTestList, new int[]{85, 263, 481});

        theTestList.insertAt(0, 424);
        Tester.verifyListContents(theTestList, new int[]{424, 85, 263, 481});

        theTestList.insertAt(0, 565);
        Tester.verifyListContents(theTestList, new int[]{565, 424, 85, 263, 481});

        assertTrue("Fehler: Element 481 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(481));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{565, 424, 263, 481});

        theTestList.insertAt(4, 272);
        Tester.verifyListContents(theTestList, new int[]{565, 424, 263, 481, 272});

        theTestList.append(3);
        Tester.verifyListContents(theTestList, new int[]{565, 424, 263, 481, 272, 3});

        theTestList.insertAt(3, 803);
        Tester.verifyListContents(theTestList, new int[]{565, 424, 263, 803, 481, 272, 3});

        theTestList.insertAt(2, 980);
        Tester.verifyListContents(theTestList, new int[]{565, 424, 980, 263, 803, 481, 272, 3});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{565, 424, 980, 803, 481, 272, 3});

        theTestList.append(658);
        Tester.verifyListContents(theTestList, new int[]{565, 424, 980, 803, 481, 272, 3, 658});

        theTestList.insertAt(0, 893);
        Tester.verifyListContents(theTestList, new int[]{893, 565, 424, 980, 803, 481, 272, 3, 658});

        theTestList.append(572);
        Tester.verifyListContents(theTestList, new int[]{893, 565, 424, 980, 803, 481, 272, 3, 658, 572});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{893, 565, 424, 803, 481, 272, 3, 658, 572});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{893, 565, 803, 481, 272, 3, 658, 572});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{893, 565, 803, 481, 272, 3, 658});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(388);
        Tester.verifyListContents(theTestList, new int[]{388});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(204);
        Tester.verifyListContents(theTestList, new int[]{204});

        theTestList.append(935);
        Tester.verifyListContents(theTestList, new int[]{204, 935});

        assertFalse("Fehler: Element 524 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(524));
        theTestList.append(19);
        Tester.verifyListContents(theTestList, new int[]{204, 935, 19});

        theTestList.append(458);
        Tester.verifyListContents(theTestList, new int[]{204, 935, 19, 458});

        theTestList.append(379);
        Tester.verifyListContents(theTestList, new int[]{204, 935, 19, 458, 379});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{935, 19, 458, 379});

        assertTrue("Fehler: Element 935 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(935));
        theTestList.insertAt(0, 127);
        Tester.verifyListContents(theTestList, new int[]{127, 935, 19, 458, 379});

        assertTrue("Fehler: Element 935 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(935));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 828);
        Tester.verifyListContents(theTestList, new int[]{828});

        theTestList.insertAt(1, 714);
        Tester.verifyListContents(theTestList, new int[]{828, 714});

        theTestList.append(863);
        Tester.verifyListContents(theTestList, new int[]{828, 714, 863});

        theTestList.insertAt(2, 333);
        Tester.verifyListContents(theTestList, new int[]{828, 714, 333, 863});

        theTestList.insertAt(0, 203);
        Tester.verifyListContents(theTestList, new int[]{203, 828, 714, 333, 863});

        theTestList.append(266);
        Tester.verifyListContents(theTestList, new int[]{203, 828, 714, 333, 863, 266});

        assertTrue("Fehler: Element 203 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(203));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{828, 714, 333, 863, 266});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{828, 333, 863, 266});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{828, 333, 266});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{828, 333});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 876);
        Tester.verifyListContents(theTestList, new int[]{876});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(484);
        Tester.verifyListContents(theTestList, new int[]{484});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 24);
        Tester.verifyListContents(theTestList, new int[]{24});

        theTestList.insertAt(1, 810);
        Tester.verifyListContents(theTestList, new int[]{24, 810});

        assertFalse("Fehler: Element 303 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(303));
        theTestList.insertAt(1, 723);
        Tester.verifyListContents(theTestList, new int[]{24, 723, 810});

        assertFalse("Fehler: Element 727 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(727));
        theTestList.append(693);
        Tester.verifyListContents(theTestList, new int[]{24, 723, 810, 693});

        theTestList.append(340);
        Tester.verifyListContents(theTestList, new int[]{24, 723, 810, 693, 340});

        theTestList.append(86);
        Tester.verifyListContents(theTestList, new int[]{24, 723, 810, 693, 340, 86});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{24, 810, 693, 340, 86});

        theTestList.append(796);
        Tester.verifyListContents(theTestList, new int[]{24, 810, 693, 340, 86, 796});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{24, 810, 693, 340, 796});

        assertTrue("Fehler: Element 340 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(340));
        theTestList.insertAt(5, 218);
        Tester.verifyListContents(theTestList, new int[]{24, 810, 693, 340, 796, 218});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{24, 810, 693, 340, 218});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{24, 810, 340, 218});

        assertFalse("Fehler: Element 732 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(732));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{24, 340, 218});

        theTestList.insertAt(0, 395);
        Tester.verifyListContents(theTestList, new int[]{395, 24, 340, 218});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{395, 24, 218});

        theTestList.insertAt(0, 279);
        Tester.verifyListContents(theTestList, new int[]{279, 395, 24, 218});

        theTestList.insertAt(0, 604);
        Tester.verifyListContents(theTestList, new int[]{604, 279, 395, 24, 218});

        theTestList.insertAt(4, 798);
        Tester.verifyListContents(theTestList, new int[]{604, 279, 395, 24, 798, 218});

        assertFalse("Fehler: Element 836 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(836));
        theTestList.insertAt(0, 85);
        Tester.verifyListContents(theTestList, new int[]{85, 604, 279, 395, 24, 798, 218});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{85, 604, 395, 24, 798, 218});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{85, 604, 395, 24, 798});

        theTestList.append(217);
        Tester.verifyListContents(theTestList, new int[]{85, 604, 395, 24, 798, 217});

        assertFalse("Fehler: Element 597 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(597));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{85, 604, 395, 24, 217});

        assertTrue("Fehler: Element 85 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(85));
        assertFalse("Fehler: Element 16 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(16));
        theTestList.insertAt(3, 553);
        Tester.verifyListContents(theTestList, new int[]{85, 604, 395, 553, 24, 217});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{604, 395, 553, 24, 217});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{604, 395, 24, 217});

        theTestList.append(429);
        Tester.verifyListContents(theTestList, new int[]{604, 395, 24, 217, 429});

        theTestList.append(956);
        Tester.verifyListContents(theTestList, new int[]{604, 395, 24, 217, 429, 956});

        theTestList.append(334);
        Tester.verifyListContents(theTestList, new int[]{604, 395, 24, 217, 429, 956, 334});

        assertTrue("Fehler: Element 334 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(334));
        theTestList.append(461);
        Tester.verifyListContents(theTestList, new int[]{604, 395, 24, 217, 429, 956, 334, 461});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{604, 395, 24, 217, 429, 334, 461});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{604, 395, 24, 429, 334, 461});

        theTestList.append(635);
        Tester.verifyListContents(theTestList, new int[]{604, 395, 24, 429, 334, 461, 635});

        theTestList.append(718);
        Tester.verifyListContents(theTestList, new int[]{604, 395, 24, 429, 334, 461, 635, 718});

        assertTrue("Fehler: Element 461 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(461));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{604, 395, 24, 334, 461, 635, 718});

        theTestList.append(651);
        Tester.verifyListContents(theTestList, new int[]{604, 395, 24, 334, 461, 635, 718, 651});

        theTestList.insertAt(1, 482);
        Tester.verifyListContents(theTestList, new int[]{604, 482, 395, 24, 334, 461, 635, 718, 651});

        theTestList.append(108);
        Tester.verifyListContents(theTestList, new int[]{604, 482, 395, 24, 334, 461, 635, 718, 651, 108});

        theTestList.insertAt(8, 568);
        Tester.verifyListContents(theTestList, new int[]{604, 482, 395, 24, 334, 461, 635, 718, 568, 651, 108});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{604, 482, 395, 24, 334, 461, 718, 568, 651, 108});

        theTestList.append(563);
        Tester.verifyListContents(theTestList, new int[]{604, 482, 395, 24, 334, 461, 718, 568, 651, 108, 563});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{604, 482, 395, 24, 334, 461, 568, 651, 108, 563});

    }

    public static void test5Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 665);
        Tester.verifyListContents(theTestList, new int[]{665});

        theTestList.append(262);
        Tester.verifyListContents(theTestList, new int[]{665, 262});

        theTestList.insertAt(0, 41);
        Tester.verifyListContents(theTestList, new int[]{41, 665, 262});

        theTestList.insertAt(2, 78);
        Tester.verifyListContents(theTestList, new int[]{41, 665, 78, 262});

        theTestList.insertAt(0, 647);
        Tester.verifyListContents(theTestList, new int[]{647, 41, 665, 78, 262});

        assertTrue("Fehler: Element 262 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(262));
        theTestList.append(340);
        Tester.verifyListContents(theTestList, new int[]{647, 41, 665, 78, 262, 340});

        theTestList.insertAt(5, 648);
        Tester.verifyListContents(theTestList, new int[]{647, 41, 665, 78, 262, 648, 340});

        theTestList.insertAt(3, 421);
        Tester.verifyListContents(theTestList, new int[]{647, 41, 665, 421, 78, 262, 648, 340});

        theTestList.insertAt(4, 829);
        Tester.verifyListContents(theTestList, new int[]{647, 41, 665, 421, 829, 78, 262, 648, 340});

        theTestList.insertAt(5, 757);
        Tester.verifyListContents(theTestList, new int[]{647, 41, 665, 421, 829, 757, 78, 262, 648, 340});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{41, 665, 421, 829, 757, 78, 262, 648, 340});

        assertTrue("Fehler: Element 829 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(829));
        theTestList.append(870);
        Tester.verifyListContents(theTestList, new int[]{41, 665, 421, 829, 757, 78, 262, 648, 340, 870});

        theTestList.insertAt(0, 720);
        Tester.verifyListContents(theTestList, new int[]{720, 41, 665, 421, 829, 757, 78, 262, 648, 340, 870});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{720, 41, 665, 421, 829, 757, 78, 648, 340, 870});

        theTestList.insertAt(10, 995);
        Tester.verifyListContents(theTestList, new int[]{720, 41, 665, 421, 829, 757, 78, 648, 340, 870, 995});

        theTestList.append(736);
        Tester.verifyListContents(theTestList, new int[]{720, 41, 665, 421, 829, 757, 78, 648, 340, 870, 995, 736});

        assertTrue("Fehler: Element 648 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(648));
        theTestList.append(119);
        Tester.verifyListContents(theTestList, new int[]{720, 41, 665, 421, 829, 757, 78, 648, 340, 870, 995, 736, 119});

        assertTrue("Fehler: Element 41 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(41));
        theTestList.append(291);
        Tester.verifyListContents(theTestList, new int[]{720, 41, 665, 421, 829, 757, 78, 648, 340, 870, 995, 736, 119, 291});

        assertFalse("Fehler: Element 394 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(394));
        theTestList.append(196);
        Tester.verifyListContents(theTestList, new int[]{720, 41, 665, 421, 829, 757, 78, 648, 340, 870, 995, 736, 119, 291, 196});

        assertTrue("Fehler: Element 119 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(119));
        theTestList.removeAt(13);
        Tester.verifyListContents(theTestList, new int[]{720, 41, 665, 421, 829, 757, 78, 648, 340, 870, 995, 736, 119, 196});

        theTestList.append(135);
        Tester.verifyListContents(theTestList, new int[]{720, 41, 665, 421, 829, 757, 78, 648, 340, 870, 995, 736, 119, 196, 135});

        theTestList.insertAt(7, 712);
        Tester.verifyListContents(theTestList, new int[]{720, 41, 665, 421, 829, 757, 78, 712, 648, 340, 870, 995, 736, 119, 196, 135});

        theTestList.insertAt(12, 137);
        Tester.verifyListContents(theTestList, new int[]{720, 41, 665, 421, 829, 757, 78, 712, 648, 340, 870, 995, 137, 736, 119, 196, 135});

        theTestList.removeAt(12);
        Tester.verifyListContents(theTestList, new int[]{720, 41, 665, 421, 829, 757, 78, 712, 648, 340, 870, 995, 736, 119, 196, 135});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{720, 665, 421, 829, 757, 78, 712, 648, 340, 870, 995, 736, 119, 196, 135});

        theTestList.insertAt(4, 867);
        Tester.verifyListContents(theTestList, new int[]{720, 665, 421, 829, 867, 757, 78, 712, 648, 340, 870, 995, 736, 119, 196, 135});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{720, 665, 829, 867, 757, 78, 712, 648, 340, 870, 995, 736, 119, 196, 135});

        theTestList.append(335);
        Tester.verifyListContents(theTestList, new int[]{720, 665, 829, 867, 757, 78, 712, 648, 340, 870, 995, 736, 119, 196, 135, 335});

        theTestList.insertAt(5, 757);
        Tester.verifyListContents(theTestList, new int[]{720, 665, 829, 867, 757, 757, 78, 712, 648, 340, 870, 995, 736, 119, 196, 135, 335});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{720, 665, 829, 867, 757, 757, 78, 712, 648, 870, 995, 736, 119, 196, 135, 335});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{720, 665, 829, 867, 757, 78, 712, 648, 870, 995, 736, 119, 196, 135, 335});

        theTestList.removeAt(12);
        Tester.verifyListContents(theTestList, new int[]{720, 665, 829, 867, 757, 78, 712, 648, 870, 995, 736, 119, 135, 335});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{720, 665, 829, 867, 757, 78, 648, 870, 995, 736, 119, 135, 335});

        theTestList.insertAt(13, 42);
        Tester.verifyListContents(theTestList, new int[]{720, 665, 829, 867, 757, 78, 648, 870, 995, 736, 119, 135, 335, 42});

        theTestList.append(461);
        Tester.verifyListContents(theTestList, new int[]{720, 665, 829, 867, 757, 78, 648, 870, 995, 736, 119, 135, 335, 42, 461});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{720, 829, 867, 757, 78, 648, 870, 995, 736, 119, 135, 335, 42, 461});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{720, 829, 867, 757, 78, 648, 995, 736, 119, 135, 335, 42, 461});

        assertTrue("Fehler: Element 135 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(135));
        theTestList.append(408);
        Tester.verifyListContents(theTestList, new int[]{720, 829, 867, 757, 78, 648, 995, 736, 119, 135, 335, 42, 461, 408});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{720, 867, 757, 78, 648, 995, 736, 119, 135, 335, 42, 461, 408});

        theTestList.insertAt(3, 984);
        Tester.verifyListContents(theTestList, new int[]{720, 867, 757, 984, 78, 648, 995, 736, 119, 135, 335, 42, 461, 408});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{720, 867, 757, 984, 78, 995, 736, 119, 135, 335, 42, 461, 408});

        assertFalse("Fehler: Element 449 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(449));
        assertTrue("Fehler: Element 984 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(984));
        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{720, 867, 757, 984, 78, 995, 119, 135, 335, 42, 461, 408});

        assertFalse("Fehler: Element 239 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(239));
        theTestList.insertAt(7, 790);
        Tester.verifyListContents(theTestList, new int[]{720, 867, 757, 984, 78, 995, 119, 790, 135, 335, 42, 461, 408});

        assertFalse("Fehler: Element 131 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(131));
        theTestList.removeAt(12);
        Tester.verifyListContents(theTestList, new int[]{720, 867, 757, 984, 78, 995, 119, 790, 135, 335, 42, 461});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 896);
        Tester.verifyListContents(theTestList, new int[]{896});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(146);
        Tester.verifyListContents(theTestList, new int[]{146});

        assertTrue("Fehler: Element 146 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(146));
        theTestList.append(380);
        Tester.verifyListContents(theTestList, new int[]{146, 380});

        assertFalse("Fehler: Element 852 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(852));
        theTestList.insertAt(0, 510);
        Tester.verifyListContents(theTestList, new int[]{510, 146, 380});

        assertFalse("Fehler: Element 996 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(996));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{510, 146});

        theTestList.insertAt(2, 535);
        Tester.verifyListContents(theTestList, new int[]{510, 146, 535});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 749);
        Tester.verifyListContents(theTestList, new int[]{749});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(826);
        Tester.verifyListContents(theTestList, new int[]{826});

        theTestList.append(923);
        Tester.verifyListContents(theTestList, new int[]{826, 923});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{826});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 702);
        Tester.verifyListContents(theTestList, new int[]{702});

        theTestList.insertAt(0, 754);
        Tester.verifyListContents(theTestList, new int[]{754, 702});

        theTestList.append(539);
        Tester.verifyListContents(theTestList, new int[]{754, 702, 539});

        theTestList.insertAt(0, 638);
        Tester.verifyListContents(theTestList, new int[]{638, 754, 702, 539});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{754, 702, 539});

        assertTrue("Fehler: Element 702 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(702));
        theTestList.append(411);
        Tester.verifyListContents(theTestList, new int[]{754, 702, 539, 411});

        theTestList.insertAt(3, 911);
        Tester.verifyListContents(theTestList, new int[]{754, 702, 539, 911, 411});

        theTestList.insertAt(5, 138);
        Tester.verifyListContents(theTestList, new int[]{754, 702, 539, 911, 411, 138});

        theTestList.insertAt(1, 485);
        Tester.verifyListContents(theTestList, new int[]{754, 485, 702, 539, 911, 411, 138});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(869);
        Tester.verifyListContents(theTestList, new int[]{869});

        theTestList.insertAt(0, 281);
        Tester.verifyListContents(theTestList, new int[]{281, 869});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{869});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 436);
        Tester.verifyListContents(theTestList, new int[]{436});

        assertTrue("Fehler: Element 436 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(436));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 938);
        Tester.verifyListContents(theTestList, new int[]{938});

        theTestList.insertAt(1, 397);
        Tester.verifyListContents(theTestList, new int[]{938, 397});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{397});

        assertFalse("Fehler: Element 274 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(274));
        theTestList.insertAt(0, 502);
        Tester.verifyListContents(theTestList, new int[]{502, 397});

        theTestList.append(315);
        Tester.verifyListContents(theTestList, new int[]{502, 397, 315});

        assertFalse("Fehler: Element 140 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(140));
        theTestList.append(383);
        Tester.verifyListContents(theTestList, new int[]{502, 397, 315, 383});

        assertTrue("Fehler: Element 315 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(315));
    }

    public static void test6Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(429);
        Tester.verifyListContents(theTestList, new int[]{429});

        theTestList.insertAt(1, 324);
        Tester.verifyListContents(theTestList, new int[]{429, 324});

        assertFalse("Fehler: Element 24 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(24));
        theTestList.insertAt(2, 840);
        Tester.verifyListContents(theTestList, new int[]{429, 324, 840});

        theTestList.insertAt(0, 173);
        Tester.verifyListContents(theTestList, new int[]{173, 429, 324, 840});

        assertTrue("Fehler: Element 840 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(840));
        assertFalse("Fehler: Element 984 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(984));
        assertFalse("Fehler: Element 641 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(641));
        assertTrue("Fehler: Element 324 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(324));
        theTestList.insertAt(4, 457);
        Tester.verifyListContents(theTestList, new int[]{173, 429, 324, 840, 457});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{173, 429, 324, 840});

        theTestList.insertAt(4, 95);
        Tester.verifyListContents(theTestList, new int[]{173, 429, 324, 840, 95});

        assertFalse("Fehler: Element 52 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(52));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{173, 429, 840, 95});

        assertFalse("Fehler: Element 669 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(669));
        theTestList.append(708);
        Tester.verifyListContents(theTestList, new int[]{173, 429, 840, 95, 708});

        theTestList.insertAt(4, 707);
        Tester.verifyListContents(theTestList, new int[]{173, 429, 840, 95, 707, 708});

        theTestList.insertAt(3, 33);
        Tester.verifyListContents(theTestList, new int[]{173, 429, 840, 33, 95, 707, 708});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{173, 429, 840, 33, 95, 708});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{173, 429, 840, 33, 95});

        theTestList.insertAt(2, 128);
        Tester.verifyListContents(theTestList, new int[]{173, 429, 128, 840, 33, 95});

        theTestList.insertAt(1, 920);
        Tester.verifyListContents(theTestList, new int[]{173, 920, 429, 128, 840, 33, 95});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{173, 920, 128, 840, 33, 95});

        theTestList.append(154);
        Tester.verifyListContents(theTestList, new int[]{173, 920, 128, 840, 33, 95, 154});

        theTestList.append(640);
        Tester.verifyListContents(theTestList, new int[]{173, 920, 128, 840, 33, 95, 154, 640});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{173, 920, 128, 840, 33, 154, 640});

        assertTrue("Fehler: Element 128 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(128));
        assertTrue("Fehler: Element 920 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(920));
        assertTrue("Fehler: Element 920 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(920));
        theTestList.append(738);
        Tester.verifyListContents(theTestList, new int[]{173, 920, 128, 840, 33, 154, 640, 738});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{173, 920, 128, 840, 154, 640, 738});

        theTestList.insertAt(7, 637);
        Tester.verifyListContents(theTestList, new int[]{173, 920, 128, 840, 154, 640, 738, 637});

        theTestList.insertAt(8, 532);
        Tester.verifyListContents(theTestList, new int[]{173, 920, 128, 840, 154, 640, 738, 637, 532});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{173, 920, 128, 840, 154, 640, 738, 637});

        assertFalse("Fehler: Element 511 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(511));
        theTestList.insertAt(7, 742);
        Tester.verifyListContents(theTestList, new int[]{173, 920, 128, 840, 154, 640, 738, 742, 637});

        theTestList.insertAt(4, 500);
        Tester.verifyListContents(theTestList, new int[]{173, 920, 128, 840, 500, 154, 640, 738, 742, 637});

        theTestList.append(392);
        Tester.verifyListContents(theTestList, new int[]{173, 920, 128, 840, 500, 154, 640, 738, 742, 637, 392});

        theTestList.append(550);
        Tester.verifyListContents(theTestList, new int[]{173, 920, 128, 840, 500, 154, 640, 738, 742, 637, 392, 550});

        assertFalse("Fehler: Element 847 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(847));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(426);
        Tester.verifyListContents(theTestList, new int[]{426});

        theTestList.insertAt(1, 255);
        Tester.verifyListContents(theTestList, new int[]{426, 255});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{426});

        theTestList.append(149);
        Tester.verifyListContents(theTestList, new int[]{426, 149});

        theTestList.append(387);
        Tester.verifyListContents(theTestList, new int[]{426, 149, 387});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{426, 149});

        theTestList.insertAt(2, 716);
        Tester.verifyListContents(theTestList, new int[]{426, 149, 716});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{426, 149});

        theTestList.append(446);
        Tester.verifyListContents(theTestList, new int[]{426, 149, 446});

        theTestList.insertAt(0, 349);
        Tester.verifyListContents(theTestList, new int[]{349, 426, 149, 446});

        theTestList.insertAt(3, 271);
        Tester.verifyListContents(theTestList, new int[]{349, 426, 149, 271, 446});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{426, 149, 271, 446});

        theTestList.insertAt(0, 838);
        Tester.verifyListContents(theTestList, new int[]{838, 426, 149, 271, 446});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{838, 149, 271, 446});

        assertTrue("Fehler: Element 271 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(271));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{149, 271, 446});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{149, 271});

        theTestList.insertAt(1, 496);
        Tester.verifyListContents(theTestList, new int[]{149, 496, 271});

        theTestList.insertAt(2, 921);
        Tester.verifyListContents(theTestList, new int[]{149, 496, 921, 271});

        theTestList.insertAt(1, 488);
        Tester.verifyListContents(theTestList, new int[]{149, 488, 496, 921, 271});

        theTestList.append(871);
        Tester.verifyListContents(theTestList, new int[]{149, 488, 496, 921, 271, 871});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{149, 496, 921, 271, 871});

        assertTrue("Fehler: Element 871 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(871));
        assertTrue("Fehler: Element 871 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(871));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{149, 496, 921, 871});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{149, 921, 871});

        theTestList.append(105);
        Tester.verifyListContents(theTestList, new int[]{149, 921, 871, 105});

        theTestList.insertAt(0, 588);
        Tester.verifyListContents(theTestList, new int[]{588, 149, 921, 871, 105});

        theTestList.insertAt(0, 733);
        Tester.verifyListContents(theTestList, new int[]{733, 588, 149, 921, 871, 105});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{588, 149, 921, 871, 105});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{588, 921, 871, 105});

        theTestList.insertAt(0, 971);
        Tester.verifyListContents(theTestList, new int[]{971, 588, 921, 871, 105});

        theTestList.insertAt(2, 738);
        Tester.verifyListContents(theTestList, new int[]{971, 588, 738, 921, 871, 105});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{971, 588, 921, 871, 105});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{971, 921, 871, 105});

        theTestList.insertAt(0, 959);
        Tester.verifyListContents(theTestList, new int[]{959, 971, 921, 871, 105});

        assertTrue("Fehler: Element 921 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(921));
        assertTrue("Fehler: Element 871 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(871));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{959, 971, 871, 105});

        theTestList.insertAt(3, 313);
        Tester.verifyListContents(theTestList, new int[]{959, 971, 871, 313, 105});

        theTestList.insertAt(3, 611);
        Tester.verifyListContents(theTestList, new int[]{959, 971, 871, 611, 313, 105});

        theTestList.insertAt(5, 970);
        Tester.verifyListContents(theTestList, new int[]{959, 971, 871, 611, 313, 970, 105});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{959, 971, 871, 611, 313, 105});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{959, 971, 871, 611, 313});

        theTestList.append(628);
        Tester.verifyListContents(theTestList, new int[]{959, 971, 871, 611, 313, 628});

        theTestList.insertAt(0, 214);
        Tester.verifyListContents(theTestList, new int[]{214, 959, 971, 871, 611, 313, 628});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{214, 959, 971, 871, 611, 313});

        theTestList.append(860);
        Tester.verifyListContents(theTestList, new int[]{214, 959, 971, 871, 611, 313, 860});

        assertFalse("Fehler: Element 242 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(242));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{959, 971, 871, 611, 313, 860});

        theTestList.insertAt(3, 508);
        Tester.verifyListContents(theTestList, new int[]{959, 971, 871, 508, 611, 313, 860});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{959, 871, 508, 611, 313, 860});

        theTestList.append(446);
        Tester.verifyListContents(theTestList, new int[]{959, 871, 508, 611, 313, 860, 446});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{959, 871, 611, 313, 860, 446});

        theTestList.insertAt(6, 546);
        Tester.verifyListContents(theTestList, new int[]{959, 871, 611, 313, 860, 446, 546});

        theTestList.insertAt(4, 44);
        Tester.verifyListContents(theTestList, new int[]{959, 871, 611, 313, 44, 860, 446, 546});

        assertTrue("Fehler: Element 871 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(871));
        theTestList.append(120);
        Tester.verifyListContents(theTestList, new int[]{959, 871, 611, 313, 44, 860, 446, 546, 120});

        theTestList.append(43);
        Tester.verifyListContents(theTestList, new int[]{959, 871, 611, 313, 44, 860, 446, 546, 120, 43});

    }

    public static void test7Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(555);
        Tester.verifyListContents(theTestList, new int[]{555});

        theTestList.append(450);
        Tester.verifyListContents(theTestList, new int[]{555, 450});

        assertTrue("Fehler: Element 450 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(450));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(608);
        Tester.verifyListContents(theTestList, new int[]{608});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(409);
        Tester.verifyListContents(theTestList, new int[]{409});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 884);
        Tester.verifyListContents(theTestList, new int[]{884});

        theTestList.append(559);
        Tester.verifyListContents(theTestList, new int[]{884, 559});

        theTestList.append(329);
        Tester.verifyListContents(theTestList, new int[]{884, 559, 329});

        theTestList.append(65);
        Tester.verifyListContents(theTestList, new int[]{884, 559, 329, 65});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{884, 559, 329});

        theTestList.insertAt(2, 218);
        Tester.verifyListContents(theTestList, new int[]{884, 559, 218, 329});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{884, 559, 329});

        assertFalse("Fehler: Element 326 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(326));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{884, 559});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{559});

        theTestList.insertAt(0, 651);
        Tester.verifyListContents(theTestList, new int[]{651, 559});

        assertTrue("Fehler: Element 651 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(651));
        theTestList.append(399);
        Tester.verifyListContents(theTestList, new int[]{651, 559, 399});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{559, 399});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(757);
        Tester.verifyListContents(theTestList, new int[]{757});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(105);
        Tester.verifyListContents(theTestList, new int[]{105});

        theTestList.insertAt(0, 16);
        Tester.verifyListContents(theTestList, new int[]{16, 105});

        theTestList.append(121);
        Tester.verifyListContents(theTestList, new int[]{16, 105, 121});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{16, 105});

        theTestList.insertAt(1, 245);
        Tester.verifyListContents(theTestList, new int[]{16, 245, 105});

        theTestList.append(950);
        Tester.verifyListContents(theTestList, new int[]{16, 245, 105, 950});

        theTestList.append(161);
        Tester.verifyListContents(theTestList, new int[]{16, 245, 105, 950, 161});

        assertFalse("Fehler: Element 130 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(130));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{16, 245, 950, 161});

        theTestList.insertAt(1, 562);
        Tester.verifyListContents(theTestList, new int[]{16, 562, 245, 950, 161});

        theTestList.insertAt(0, 352);
        Tester.verifyListContents(theTestList, new int[]{352, 16, 562, 245, 950, 161});

        assertTrue("Fehler: Element 16 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(16));
        assertFalse("Fehler: Element 466 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(466));
        assertTrue("Fehler: Element 245 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(245));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 77);
        Tester.verifyListContents(theTestList, new int[]{77});

        theTestList.append(312);
        Tester.verifyListContents(theTestList, new int[]{77, 312});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{77});

        assertFalse("Fehler: Element 940 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(940));
        assertTrue("Fehler: Element 77 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(77));
        theTestList.insertAt(1, 806);
        Tester.verifyListContents(theTestList, new int[]{77, 806});

        theTestList.insertAt(2, 398);
        Tester.verifyListContents(theTestList, new int[]{77, 806, 398});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{806, 398});

        theTestList.append(285);
        Tester.verifyListContents(theTestList, new int[]{806, 398, 285});

        theTestList.append(565);
        Tester.verifyListContents(theTestList, new int[]{806, 398, 285, 565});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{806, 398, 285});

        assertFalse("Fehler: Element 446 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(446));
        theTestList.append(297);
        Tester.verifyListContents(theTestList, new int[]{806, 398, 285, 297});

        assertTrue("Fehler: Element 806 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(806));
        assertFalse("Fehler: Element 328 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(328));
        theTestList.append(171);
        Tester.verifyListContents(theTestList, new int[]{806, 398, 285, 297, 171});

        theTestList.insertAt(0, 39);
        Tester.verifyListContents(theTestList, new int[]{39, 806, 398, 285, 297, 171});

        theTestList.insertAt(3, 77);
        Tester.verifyListContents(theTestList, new int[]{39, 806, 398, 77, 285, 297, 171});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{39, 806, 398, 77, 297, 171});

        theTestList.append(675);
        Tester.verifyListContents(theTestList, new int[]{39, 806, 398, 77, 297, 171, 675});

        assertFalse("Fehler: Element 301 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(301));
        theTestList.append(871);
        Tester.verifyListContents(theTestList, new int[]{39, 806, 398, 77, 297, 171, 675, 871});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{39, 806, 398, 77, 297, 171, 675});

        theTestList.append(474);
        Tester.verifyListContents(theTestList, new int[]{39, 806, 398, 77, 297, 171, 675, 474});

        theTestList.append(351);
        Tester.verifyListContents(theTestList, new int[]{39, 806, 398, 77, 297, 171, 675, 474, 351});

        theTestList.insertAt(9, 773);
        Tester.verifyListContents(theTestList, new int[]{39, 806, 398, 77, 297, 171, 675, 474, 351, 773});

        theTestList.append(780);
        Tester.verifyListContents(theTestList, new int[]{39, 806, 398, 77, 297, 171, 675, 474, 351, 773, 780});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{806, 398, 77, 297, 171, 675, 474, 351, 773, 780});

        theTestList.insertAt(2, 353);
        Tester.verifyListContents(theTestList, new int[]{806, 398, 353, 77, 297, 171, 675, 474, 351, 773, 780});

        theTestList.append(152);
        Tester.verifyListContents(theTestList, new int[]{806, 398, 353, 77, 297, 171, 675, 474, 351, 773, 780, 152});

        assertFalse("Fehler: Element 824 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(824));
        theTestList.insertAt(1, 133);
        Tester.verifyListContents(theTestList, new int[]{806, 133, 398, 353, 77, 297, 171, 675, 474, 351, 773, 780, 152});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{806, 133, 398, 353, 77, 297, 171, 675, 474, 773, 780, 152});

        theTestList.insertAt(11, 82);
        Tester.verifyListContents(theTestList, new int[]{806, 133, 398, 353, 77, 297, 171, 675, 474, 773, 780, 82, 152});

        theTestList.append(879);
        Tester.verifyListContents(theTestList, new int[]{806, 133, 398, 353, 77, 297, 171, 675, 474, 773, 780, 82, 152, 879});

        theTestList.append(526);
        Tester.verifyListContents(theTestList, new int[]{806, 133, 398, 353, 77, 297, 171, 675, 474, 773, 780, 82, 152, 879, 526});

        theTestList.insertAt(6, 174);
        Tester.verifyListContents(theTestList, new int[]{806, 133, 398, 353, 77, 297, 174, 171, 675, 474, 773, 780, 82, 152, 879, 526});

        theTestList.append(586);
        Tester.verifyListContents(theTestList, new int[]{806, 133, 398, 353, 77, 297, 174, 171, 675, 474, 773, 780, 82, 152, 879, 526, 586});

        theTestList.append(919);
        Tester.verifyListContents(theTestList, new int[]{806, 133, 398, 353, 77, 297, 174, 171, 675, 474, 773, 780, 82, 152, 879, 526, 586, 919});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{806, 133, 398, 353, 77, 297, 174, 171, 675, 773, 780, 82, 152, 879, 526, 586, 919});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(602);
        Tester.verifyListContents(theTestList, new int[]{602});

        theTestList.insertAt(0, 135);
        Tester.verifyListContents(theTestList, new int[]{135, 602});

        assertTrue("Fehler: Element 135 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(135));
        assertTrue("Fehler: Element 602 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(602));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{135});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 43);
        Tester.verifyListContents(theTestList, new int[]{43});

        assertTrue("Fehler: Element 43 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(43));
        theTestList.append(440);
        Tester.verifyListContents(theTestList, new int[]{43, 440});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{440});

        theTestList.insertAt(1, 512);
        Tester.verifyListContents(theTestList, new int[]{440, 512});

        theTestList.insertAt(0, 483);
        Tester.verifyListContents(theTestList, new int[]{483, 440, 512});

        assertFalse("Fehler: Element 68 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(68));
        theTestList.append(80);
        Tester.verifyListContents(theTestList, new int[]{483, 440, 512, 80});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{440, 512, 80});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{440, 512});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{512});

        assertFalse("Fehler: Element 965 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(965));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

    }

    public static void test8Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(796);
        Tester.verifyListContents(theTestList, new int[]{796});

        theTestList.append(55);
        Tester.verifyListContents(theTestList, new int[]{796, 55});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{796});

        theTestList.append(499);
        Tester.verifyListContents(theTestList, new int[]{796, 499});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{499});

        assertTrue("Fehler: Element 499 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(499));
        theTestList.append(263);
        Tester.verifyListContents(theTestList, new int[]{499, 263});

        assertTrue("Fehler: Element 263 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(263));
        theTestList.append(539);
        Tester.verifyListContents(theTestList, new int[]{499, 263, 539});

        assertFalse("Fehler: Element 212 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(212));
        assertFalse("Fehler: Element 722 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(722));
        assertTrue("Fehler: Element 539 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(539));
        assertTrue("Fehler: Element 499 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(499));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{499, 263});

        theTestList.append(368);
        Tester.verifyListContents(theTestList, new int[]{499, 263, 368});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{499, 263});

        theTestList.insertAt(1, 166);
        Tester.verifyListContents(theTestList, new int[]{499, 166, 263});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 686);
        Tester.verifyListContents(theTestList, new int[]{686});

        theTestList.append(998);
        Tester.verifyListContents(theTestList, new int[]{686, 998});

        theTestList.insertAt(0, 506);
        Tester.verifyListContents(theTestList, new int[]{506, 686, 998});

        theTestList.append(562);
        Tester.verifyListContents(theTestList, new int[]{506, 686, 998, 562});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{686, 998, 562});

        theTestList.append(57);
        Tester.verifyListContents(theTestList, new int[]{686, 998, 562, 57});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{686, 998, 57});

        assertFalse("Fehler: Element 22 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(22));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{686, 998});

        theTestList.append(7);
        Tester.verifyListContents(theTestList, new int[]{686, 998, 7});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{686, 998});

        theTestList.append(994);
        Tester.verifyListContents(theTestList, new int[]{686, 998, 994});

        theTestList.append(699);
        Tester.verifyListContents(theTestList, new int[]{686, 998, 994, 699});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{686, 998, 699});

        theTestList.append(653);
        Tester.verifyListContents(theTestList, new int[]{686, 998, 699, 653});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{686, 699, 653});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{686, 653});

        assertFalse("Fehler: Element 740 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(740));
        theTestList.insertAt(0, 307);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 653});

        theTestList.insertAt(3, 633);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 653, 633});

        theTestList.append(163);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 653, 633, 163});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 653, 633});

        theTestList.insertAt(3, 695);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 653, 695, 633});

        assertTrue("Fehler: Element 653 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(653));
        assertFalse("Fehler: Element 358 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(358));
        theTestList.insertAt(2, 985);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 653, 695, 633});

        theTestList.append(243);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 653, 695, 633, 243});

        theTestList.append(912);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 653, 695, 633, 243, 912});

        theTestList.append(923);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 653, 695, 633, 243, 912, 923});

        theTestList.insertAt(2, 527);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 527, 985, 653, 695, 633, 243, 912, 923});

        theTestList.append(305);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 527, 985, 653, 695, 633, 243, 912, 923, 305});

        theTestList.insertAt(4, 364);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 527, 985, 364, 653, 695, 633, 243, 912, 923, 305});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 364, 653, 695, 633, 243, 912, 923, 305});

        theTestList.append(110);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 364, 653, 695, 633, 243, 912, 923, 305, 110});

        theTestList.append(896);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 364, 653, 695, 633, 243, 912, 923, 305, 110, 896});

        theTestList.insertAt(8, 554);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 364, 653, 695, 633, 243, 554, 912, 923, 305, 110, 896});

        assertTrue("Fehler: Element 110 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(110));
        theTestList.append(615);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 364, 653, 695, 633, 243, 554, 912, 923, 305, 110, 896, 615});

        theTestList.insertAt(14, 220);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 364, 653, 695, 633, 243, 554, 912, 923, 305, 110, 896, 220, 615});

        theTestList.insertAt(9, 537);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 364, 653, 695, 633, 243, 554, 537, 912, 923, 305, 110, 896, 220, 615});

        theTestList.append(360);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 364, 653, 695, 633, 243, 554, 537, 912, 923, 305, 110, 896, 220, 615, 360});

        theTestList.insertAt(18, 447);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 364, 653, 695, 633, 243, 554, 537, 912, 923, 305, 110, 896, 220, 615, 360, 447});

        theTestList.append(326);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 364, 653, 695, 633, 243, 554, 537, 912, 923, 305, 110, 896, 220, 615, 360, 447, 326});

        assertTrue("Fehler: Element 360 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(360));
        theTestList.insertAt(17, 17);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 364, 653, 695, 633, 243, 554, 537, 912, 923, 305, 110, 896, 220, 615, 17, 360, 447, 326});

        assertTrue("Fehler: Element 985 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(985));
        theTestList.removeAt(12);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 364, 653, 695, 633, 243, 554, 537, 912, 923, 110, 896, 220, 615, 17, 360, 447, 326});

        theTestList.append(593);
        Tester.verifyListContents(theTestList, new int[]{307, 686, 985, 364, 653, 695, 633, 243, 554, 537, 912, 923, 110, 896, 220, 615, 17, 360, 447, 326, 593});

        theTestList.insertAt(0, 716);
        Tester.verifyListContents(theTestList, new int[]{716, 307, 686, 985, 364, 653, 695, 633, 243, 554, 537, 912, 923, 110, 896, 220, 615, 17, 360, 447, 326, 593});

        theTestList.append(679);
        Tester.verifyListContents(theTestList, new int[]{716, 307, 686, 985, 364, 653, 695, 633, 243, 554, 537, 912, 923, 110, 896, 220, 615, 17, 360, 447, 326, 593, 679});

        theTestList.insertAt(2, 243);
        Tester.verifyListContents(theTestList, new int[]{716, 307, 243, 686, 985, 364, 653, 695, 633, 243, 554, 537, 912, 923, 110, 896, 220, 615, 17, 360, 447, 326, 593, 679});

        theTestList.append(84);
        Tester.verifyListContents(theTestList, new int[]{716, 307, 243, 686, 985, 364, 653, 695, 633, 243, 554, 537, 912, 923, 110, 896, 220, 615, 17, 360, 447, 326, 593, 679, 84});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 70);
        Tester.verifyListContents(theTestList, new int[]{70});

        theTestList.insertAt(0, 364);
        Tester.verifyListContents(theTestList, new int[]{364, 70});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{70});

        theTestList.insertAt(1, 998);
        Tester.verifyListContents(theTestList, new int[]{70, 998});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{70});

// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(143);
        Tester.verifyListContents(theTestList, new int[]{143});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 468);
        Tester.verifyListContents(theTestList, new int[]{468});

        theTestList.append(565);
        Tester.verifyListContents(theTestList, new int[]{468, 565});

        theTestList.insertAt(2, 740);
        Tester.verifyListContents(theTestList, new int[]{468, 565, 740});

        assertTrue("Fehler: Element 740 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(740));
        assertFalse("Fehler: Element 951 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(951));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{565, 740});

        assertTrue("Fehler: Element 565 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(565));
        assertFalse("Fehler: Element 793 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(793));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{565});

        theTestList.insertAt(1, 477);
        Tester.verifyListContents(theTestList, new int[]{565, 477});

        assertFalse("Fehler: Element 117 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(117));
        assertTrue("Fehler: Element 477 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(477));
        theTestList.insertAt(2, 666);
        Tester.verifyListContents(theTestList, new int[]{565, 477, 666});

        theTestList.insertAt(0, 255);
        Tester.verifyListContents(theTestList, new int[]{255, 565, 477, 666});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{255, 565, 477});

        theTestList.append(965);
        Tester.verifyListContents(theTestList, new int[]{255, 565, 477, 965});

        theTestList.append(503);
        Tester.verifyListContents(theTestList, new int[]{255, 565, 477, 965, 503});

        assertTrue("Fehler: Element 255 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(255));
        assertTrue("Fehler: Element 965 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(965));
        theTestList.insertAt(5, 614);
        Tester.verifyListContents(theTestList, new int[]{255, 565, 477, 965, 503, 614});

        theTestList.insertAt(3, 105);
        Tester.verifyListContents(theTestList, new int[]{255, 565, 477, 105, 965, 503, 614});

    }

    public static void test9Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(435);
        Tester.verifyListContents(theTestList, new int[]{435});

        theTestList.insertAt(1, 445);
        Tester.verifyListContents(theTestList, new int[]{435, 445});

        assertFalse("Fehler: Element 996 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(996));
        theTestList.append(728);
        Tester.verifyListContents(theTestList, new int[]{435, 445, 728});

        theTestList.append(47);
        Tester.verifyListContents(theTestList, new int[]{435, 445, 728, 47});

        theTestList.insertAt(0, 416);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 47});

        assertTrue("Fehler: Element 47 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(47));
        theTestList.append(191);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 47, 191});

        theTestList.append(774);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 47, 191, 774});

        theTestList.append(779);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 47, 191, 774, 779});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 47, 774, 779});

        theTestList.append(838);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 47, 774, 779, 838});

        theTestList.append(223);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 47, 774, 779, 838, 223});

        theTestList.append(96);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 47, 774, 779, 838, 223, 96});

        theTestList.insertAt(8, 159);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 47, 774, 779, 838, 159, 223, 96});

        theTestList.append(815);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 47, 774, 779, 838, 159, 223, 96, 815});

        theTestList.insertAt(7, 816);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 47, 774, 779, 816, 838, 159, 223, 96, 815});

        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 47, 774, 779, 816, 838, 159, 223, 815});

        theTestList.append(305);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 47, 774, 779, 816, 838, 159, 223, 815, 305});

        assertFalse("Fehler: Element 300 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(300));
        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 47, 774, 816, 838, 159, 223, 815, 305});

        assertFalse("Fehler: Element 297 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(297));
        theTestList.append(932);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 47, 774, 816, 838, 159, 223, 815, 305, 932});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 774, 816, 838, 159, 223, 815, 305, 932});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 774, 838, 159, 223, 815, 305, 932});

        theTestList.append(396);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 774, 838, 159, 223, 815, 305, 932, 396});

        theTestList.append(351);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 774, 838, 159, 223, 815, 305, 932, 396, 351});

        theTestList.append(267);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 774, 838, 159, 223, 815, 305, 932, 396, 351, 267});

        theTestList.append(722);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 774, 838, 159, 223, 815, 305, 932, 396, 351, 267, 722});

        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 774, 838, 159, 223, 815, 305, 396, 351, 267, 722});

        theTestList.insertAt(9, 129);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 774, 838, 159, 223, 815, 129, 305, 396, 351, 267, 722});

        assertTrue("Fehler: Element 267 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(267));
        theTestList.append(743);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 774, 838, 159, 223, 815, 129, 305, 396, 351, 267, 722, 743});

        theTestList.append(737);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 774, 838, 159, 223, 815, 129, 305, 396, 351, 267, 722, 743, 737});

        theTestList.append(579);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 774, 838, 159, 223, 815, 129, 305, 396, 351, 267, 722, 743, 737, 579});

        theTestList.removeAt(16);
        Tester.verifyListContents(theTestList, new int[]{416, 435, 445, 728, 774, 838, 159, 223, 815, 129, 305, 396, 351, 267, 722, 743, 579});

        assertTrue("Fehler: Element 815 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(815));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(993);
        Tester.verifyListContents(theTestList, new int[]{993});

        assertFalse("Fehler: Element 89 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(89));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(604);
        Tester.verifyListContents(theTestList, new int[]{604});

        theTestList.append(228);
        Tester.verifyListContents(theTestList, new int[]{604, 228});

        theTestList.append(344);
        Tester.verifyListContents(theTestList, new int[]{604, 228, 344});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{228, 344});

        assertFalse("Fehler: Element 380 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(380));
        theTestList.append(146);
        Tester.verifyListContents(theTestList, new int[]{228, 344, 146});

        theTestList.append(232);
        Tester.verifyListContents(theTestList, new int[]{228, 344, 146, 232});

        theTestList.insertAt(0, 956);
        Tester.verifyListContents(theTestList, new int[]{956, 228, 344, 146, 232});

        theTestList.append(446);
        Tester.verifyListContents(theTestList, new int[]{956, 228, 344, 146, 232, 446});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{956, 228, 146, 232, 446});

        theTestList.append(646);
        Tester.verifyListContents(theTestList, new int[]{956, 228, 146, 232, 446, 646});

        assertTrue("Fehler: Element 232 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(232));
        theTestList.append(226);
        Tester.verifyListContents(theTestList, new int[]{956, 228, 146, 232, 446, 646, 226});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{956, 228, 232, 446, 646, 226});

        assertFalse("Fehler: Element 658 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(658));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{228, 232, 446, 646, 226});

        theTestList.append(670);
        Tester.verifyListContents(theTestList, new int[]{228, 232, 446, 646, 226, 670});

        theTestList.insertAt(0, 294);
        Tester.verifyListContents(theTestList, new int[]{294, 228, 232, 446, 646, 226, 670});

        theTestList.insertAt(0, 633);
        Tester.verifyListContents(theTestList, new int[]{633, 294, 228, 232, 446, 646, 226, 670});

        assertTrue("Fehler: Element 646 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(646));
        theTestList.insertAt(3, 927);
        Tester.verifyListContents(theTestList, new int[]{633, 294, 228, 927, 232, 446, 646, 226, 670});

        assertFalse("Fehler: Element 815 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(815));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{633, 294, 927, 232, 446, 646, 226, 670});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{633, 294, 927, 232, 446, 226, 670});

        theTestList.insertAt(0, 389);
        Tester.verifyListContents(theTestList, new int[]{389, 633, 294, 927, 232, 446, 226, 670});

        theTestList.append(736);
        Tester.verifyListContents(theTestList, new int[]{389, 633, 294, 927, 232, 446, 226, 670, 736});

        assertTrue("Fehler: Element 389 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(389));
        theTestList.append(369);
        Tester.verifyListContents(theTestList, new int[]{389, 633, 294, 927, 232, 446, 226, 670, 736, 369});

        theTestList.insertAt(8, 292);
        Tester.verifyListContents(theTestList, new int[]{389, 633, 294, 927, 232, 446, 226, 670, 292, 736, 369});

        theTestList.insertAt(1, 445);
        Tester.verifyListContents(theTestList, new int[]{389, 445, 633, 294, 927, 232, 446, 226, 670, 292, 736, 369});

        theTestList.append(554);
        Tester.verifyListContents(theTestList, new int[]{389, 445, 633, 294, 927, 232, 446, 226, 670, 292, 736, 369, 554});

        theTestList.append(861);
        Tester.verifyListContents(theTestList, new int[]{389, 445, 633, 294, 927, 232, 446, 226, 670, 292, 736, 369, 554, 861});

        theTestList.removeAt(13);
        Tester.verifyListContents(theTestList, new int[]{389, 445, 633, 294, 927, 232, 446, 226, 670, 292, 736, 369, 554});

        assertFalse("Fehler: Element 91 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(91));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(153);
        Tester.verifyListContents(theTestList, new int[]{153});

        theTestList.append(145);
        Tester.verifyListContents(theTestList, new int[]{153, 145});

        theTestList.append(309);
        Tester.verifyListContents(theTestList, new int[]{153, 145, 309});

        theTestList.insertAt(2, 106);
        Tester.verifyListContents(theTestList, new int[]{153, 145, 106, 309});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{153, 106, 309});

        assertFalse("Fehler: Element 140 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(140));
        assertTrue("Fehler: Element 309 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(309));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{153, 106});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{153});

        assertTrue("Fehler: Element 153 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(153));
        assertFalse("Fehler: Element 316 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(316));
        theTestList.insertAt(1, 462);
        Tester.verifyListContents(theTestList, new int[]{153, 462});

        theTestList.append(988);
        Tester.verifyListContents(theTestList, new int[]{153, 462, 988});

        theTestList.insertAt(1, 123);
        Tester.verifyListContents(theTestList, new int[]{153, 123, 462, 988});

        theTestList.insertAt(2, 704);
        Tester.verifyListContents(theTestList, new int[]{153, 123, 704, 462, 988});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 736);
        Tester.verifyListContents(theTestList, new int[]{736});

        theTestList.append(100);
        Tester.verifyListContents(theTestList, new int[]{736, 100});

        theTestList.insertAt(1, 169);
        Tester.verifyListContents(theTestList, new int[]{736, 169, 100});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{736, 100});

        theTestList.append(540);
        Tester.verifyListContents(theTestList, new int[]{736, 100, 540});

        theTestList.append(486);
        Tester.verifyListContents(theTestList, new int[]{736, 100, 540, 486});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{736, 100, 486});

        theTestList.insertAt(0, 866);
        Tester.verifyListContents(theTestList, new int[]{866, 736, 100, 486});

    }

    public static void test10Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 482);
        Tester.verifyListContents(theTestList, new int[]{482});

        assertTrue("Fehler: Element 482 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(482));
        theTestList.append(651);
        Tester.verifyListContents(theTestList, new int[]{482, 651});

        assertFalse("Fehler: Element 459 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(459));
        assertTrue("Fehler: Element 482 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(482));
        theTestList.insertAt(2, 842);
        Tester.verifyListContents(theTestList, new int[]{482, 651, 842});

        theTestList.insertAt(0, 447);
        Tester.verifyListContents(theTestList, new int[]{447, 482, 651, 842});

        theTestList.append(414);
        Tester.verifyListContents(theTestList, new int[]{447, 482, 651, 842, 414});

        theTestList.insertAt(1, 310);
        Tester.verifyListContents(theTestList, new int[]{447, 310, 482, 651, 842, 414});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{447, 310, 482, 651, 414});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{447, 310, 482, 414});

        assertFalse("Fehler: Element 415 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(415));
        assertTrue("Fehler: Element 310 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(310));
        theTestList.insertAt(1, 33);
        Tester.verifyListContents(theTestList, new int[]{447, 33, 310, 482, 414});

        assertFalse("Fehler: Element 439 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(439));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{33, 310, 482, 414});

        assertTrue("Fehler: Element 414 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(414));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{310, 482, 414});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{310, 414});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{414});

        theTestList.insertAt(0, 474);
        Tester.verifyListContents(theTestList, new int[]{474, 414});

        assertFalse("Fehler: Element 644 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(644));
        theTestList.append(545);
        Tester.verifyListContents(theTestList, new int[]{474, 414, 545});

        theTestList.append(510);
        Tester.verifyListContents(theTestList, new int[]{474, 414, 545, 510});

        theTestList.insertAt(0, 5);
        Tester.verifyListContents(theTestList, new int[]{5, 474, 414, 545, 510});

        theTestList.insertAt(0, 745);
        Tester.verifyListContents(theTestList, new int[]{745, 5, 474, 414, 545, 510});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{745, 5, 414, 545, 510});

        theTestList.append(962);
        Tester.verifyListContents(theTestList, new int[]{745, 5, 414, 545, 510, 962});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{745, 414, 545, 510, 962});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 510, 962});

        theTestList.insertAt(2, 700);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 700, 510, 962});

        theTestList.append(440);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 700, 510, 962, 440});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 700, 510, 962});

        theTestList.append(127);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 700, 510, 962, 127});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 700, 962, 127});

        theTestList.insertAt(2, 569);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 569, 700, 962, 127});

        theTestList.append(217);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 569, 700, 962, 127, 217});

        assertFalse("Fehler: Element 357 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(357));
        assertFalse("Fehler: Element 166 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(166));
        theTestList.insertAt(5, 503);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 569, 700, 962, 503, 127, 217});

        theTestList.append(22);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 569, 700, 962, 503, 127, 217, 22});

        theTestList.insertAt(9, 435);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 569, 700, 962, 503, 127, 217, 22, 435});

        theTestList.append(85);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 569, 700, 962, 503, 127, 217, 22, 435, 85});

        theTestList.insertAt(11, 870);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 569, 700, 962, 503, 127, 217, 22, 435, 85, 870});

        assertFalse("Fehler: Element 49 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(49));
        assertFalse("Fehler: Element 425 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(425));
        theTestList.append(878);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 569, 700, 962, 503, 127, 217, 22, 435, 85, 870, 878});

        theTestList.append(49);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 569, 700, 962, 503, 127, 217, 22, 435, 85, 870, 878, 49});

        assertTrue("Fehler: Element 569 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(569));
        assertTrue("Fehler: Element 569 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(569));
        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 569, 700, 962, 503, 127, 217, 22, 435, 870, 878, 49});

        assertFalse("Fehler: Element 493 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(493));
        theTestList.append(421);
        Tester.verifyListContents(theTestList, new int[]{745, 545, 569, 700, 962, 503, 127, 217, 22, 435, 870, 878, 49, 421});

        assertTrue("Fehler: Element 421 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(421));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(659);
        Tester.verifyListContents(theTestList, new int[]{659});

        assertFalse("Fehler: Element 32 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(32));
        assertFalse("Fehler: Element 899 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(899));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(155);
        Tester.verifyListContents(theTestList, new int[]{155});

        theTestList.insertAt(0, 205);
        Tester.verifyListContents(theTestList, new int[]{205, 155});

        assertFalse("Fehler: Element 994 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(994));
        assertFalse("Fehler: Element 146 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(146));
        theTestList.append(31);
        Tester.verifyListContents(theTestList, new int[]{205, 155, 31});

        theTestList.append(557);
        Tester.verifyListContents(theTestList, new int[]{205, 155, 31, 557});

        assertFalse("Fehler: Element 980 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(980));
        assertTrue("Fehler: Element 205 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(205));
        assertFalse("Fehler: Element 438 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(438));
        theTestList.append(55);
        Tester.verifyListContents(theTestList, new int[]{205, 155, 31, 557, 55});

        theTestList.append(463);
        Tester.verifyListContents(theTestList, new int[]{205, 155, 31, 557, 55, 463});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{205, 155, 557, 55, 463});

        assertFalse("Fehler: Element 750 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(750));
        theTestList.append(709);
        Tester.verifyListContents(theTestList, new int[]{205, 155, 557, 55, 463, 709});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{205, 155, 557, 55, 463});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{155, 557, 55, 463});

        theTestList.append(624);
        Tester.verifyListContents(theTestList, new int[]{155, 557, 55, 463, 624});

        assertFalse("Fehler: Element 842 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(842));
        theTestList.append(518);
        Tester.verifyListContents(theTestList, new int[]{155, 557, 55, 463, 624, 518});

        assertTrue("Fehler: Element 463 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(463));
        assertTrue("Fehler: Element 557 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(557));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{155, 557, 55, 624, 518});

        assertTrue("Fehler: Element 624 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(624));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{155, 557, 55, 624});

        theTestList.append(217);
        Tester.verifyListContents(theTestList, new int[]{155, 557, 55, 624, 217});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{155, 557, 55, 624});

        assertTrue("Fehler: Element 557 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(557));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{155, 55, 624});

        theTestList.insertAt(0, 363);
        Tester.verifyListContents(theTestList, new int[]{363, 155, 55, 624});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{363, 155, 624});

        theTestList.append(719);
        Tester.verifyListContents(theTestList, new int[]{363, 155, 624, 719});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 73);
        Tester.verifyListContents(theTestList, new int[]{73});

        theTestList.append(957);
        Tester.verifyListContents(theTestList, new int[]{73, 957});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{957});

        theTestList.append(854);
        Tester.verifyListContents(theTestList, new int[]{957, 854});

        theTestList.insertAt(2, 741);
        Tester.verifyListContents(theTestList, new int[]{957, 854, 741});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{854, 741});

        theTestList.insertAt(0, 598);
        Tester.verifyListContents(theTestList, new int[]{598, 854, 741});

        theTestList.insertAt(0, 607);
        Tester.verifyListContents(theTestList, new int[]{607, 598, 854, 741});

        theTestList.append(874);
        Tester.verifyListContents(theTestList, new int[]{607, 598, 854, 741, 874});

    }
}
