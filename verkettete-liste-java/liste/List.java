package liste;

/**
 * Klasse List 
 * 
 * @author Rainer Helfrich
 * @author Frank Schiebel
 * @version Oktober 2021
 */
public class List<T>
{

    /**
     * Der erste Knoten der Liste
     */
    private Node<T> first;

    /**
     * Konstruktor
     * 
     */
    public List() {
        first = null;
    }

    /**
     * Gibt die Anzahl der Elemente der Liste zurück
     */
    public int length() {
        int length =  0;
        Node<T> n = first;
        while(n != null){
            n = n.getNext();
            length++;
        }
        return length;
    }

    /**
     * Gibt den n-ten Wert (0-basierte Zählweise) der Liste zurück.
     * @param n Der Index des gewünschten Elements
     * @return Den n-ten Wert
     */
    public T getValueAtN(int n) {
        Node<T> k = getNode(n);
        return k.getContent();
    }
    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y    (Beschreibung des Parameters)
     * @return        (Beschreibung des Rückgabewertes)
     */
    private Node getNode(int index)
    {
        if (index >= length() || index < 0){
            System.out.println("n ist kein passender index");
            return null;
        }
        Node<T> k = first;
        for(int i = 0; i < index;i++){
            k = k.getNext();
        }
        return k;
    }

    /**
     * Hängt einen neuen Wert hinten an die Liste an.
     * @param val Der anzuhängende Wert
     */
    public void append(T val) {
        Node<T> neu = new Node(val, null);
        if(isEmpty()){
            first = neu;
        }else{
            Node<T> n = first;
            while(n.getNext() != null){
                n = n.getNext();
            }
            n.setNext(neu);
        }
    }

    /**
     * Fügt einen neuen Wert an einer gewünschten Stelle in der Liste ein.
     * @param index Die Stelle, an der der neue Wert stehen soll (0 <= index <= laenge())
     * @param val Der einzufügende Wert
     */
    public void insertAt(int index, T val) {
        Node<T> neu = new Node(val, null);
        if(index == 0){
            neu.setNext(first);
            first = neu;
        }else{
            Node<T> n = getNode(index-1);
            neu.setNext(n.getNext());
            n.setNext(neu);
        }
    }

    /**
     * Gibt zurück, ob ein Wert sich in der Liste befindet
     * @param val Der zu suchende Wert
     * @return true, wenn der Wert enthalten ist; false sonst
     */
    public boolean hasValue(T val) {
        Node<T> n = first;
        while(n != null){
            if(n.getContent().equals(val)){
                return true;
            }
            n = n.getNext();
        }
        return false;
    }

    /**
     * Entfernt das Element, das am gegebenen Index steht, aus der Liste.
     * @param index Die Stelle, von der der Wert entfernt werden soll.
     */
    public void removeAt(int index) {
        if(index == 0){
            Node<T> a = getNode(index);
            first = a.getNext();
        }else{
            Node<T> v = getNode(index-1);
            Node<T> a = getNode(index);
            v.setNext(a.getNext());
        }
    }

    /**
     * Gibt zurück, ob die Liste leer ist.
     * @return true, wenn die Liste keine Elemente enthält; false sonst
     */
    public boolean isEmpty() {
        if (first == null){
            return true;
        }else{
            return false;
        }
    }

}
