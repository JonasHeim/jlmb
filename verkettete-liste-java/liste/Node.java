package liste;

/**
 * Klasse Node
 * 
 * @author Frank Schiebel
 * @version Oktober 2021
 */
public class Node<T>
{
    /**
     * Der Datenwert des Listenknotens
     */
    private T content;

    /**
     * Der Nachfolger des Listenknotens
     */
    private Node<T> nextNode;

    /**
     * Erzeugt einen neuen Listenknoten
     * 
     * @param daten Der Datenwert des Knotens
     * @param nachfolger Der Nachfolger des Knotens
     */
    public Node(T content, Node<T> next)
    {
        this.content = content;
        this.nextNode = next;
    }

    /**
     * Setzt das Attribut nextNode eines Node
     * 
     * @param   next	Naechster Knoten, auf den nextNode zeigen soll
     */
    public void setNext(Node<T> next)
    {
        this.nextNode = next;
    }
    
    /**
     * Gibt die Refernz auf den Nachfolgenden Node zurück
     * 
     * @return   Nachfolgender Node
     */
    public Node<T> getNext()
    {
        return this.nextNode;
    }
    
    /**
     * Gibt den Inhat eines Knotenobjekts zurück
     * 
     * @return   Inhalt des Knotens
     */
    public T getContent()
    {
        return this.content;
    }

}
