PROJEKTBEZEICHNUNG: Stack
PROJEKTZWECK: Zwei Implementationen des ADTs Stack
VERSION oder DATUM: Oktober 2020
WIE IST DAS PROJEKT ZU STARTEN: Implementieren Sie den ArrayStack oder den LinkedStack. Führen Sie dann bei den entsprechenden Tester-Klassen "Alles testen" aus.
AUTOR(EN): Rainer Helfrich, ZPG Informatik
