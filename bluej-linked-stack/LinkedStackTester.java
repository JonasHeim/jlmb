import org.junit.Before;

/**
 * Testklasse für den LinkedStack
 * 
 * @author Rainer Helfrich
 * @version Oktober 2020
 */
public class LinkedStackTester extends StackTester
{
    @Before
    public void setUp()
    {
        theStack = new LinkedStack<Integer>();
    }
}