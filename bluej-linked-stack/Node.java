 

/**
 * Klasse Node
 * 
 * @author Rainer Helfrich
 * @version Oktober 2020
 */
public class Node<T>
{
    /**
     * Der Datenwert des Listenknotens
     */
    private T data;
    
    /**
     * Der Nachfolger des Listenknotens
     */
    private Node<T> next;

    /**
     * Erzeugt einen neuen Listenknoten
     * 
     * @param daten Der Datenwert des Knotens
     * @param nachfolger Der Nachfolger des Knotens
     */
    public Node(T daten, Node<T> nachfolger)
    {
        this.data = daten;
        this.next = nachfolger;
    }
    /**
     * Setzt das Attribut nextNode eines Node
     * 
     * @param   next    Naechster Knoten, auf den nextNode zeigen soll
     */
    public void setNext(Node<T> next)
    {
        this.next = next;
    }
    
    /**
     * Gibt die Refernz auf den Nachfolgenden Node zurück
     * 
     * @return   Nachfolgender Node
     */
    public Node<T> getNext()
    {
        return this.next;
    }
    
    /**
     * Gibt den Inhat eines Knotenobjekts zurück
     * 
     * @return   Inhalt des Knotens
     */
    public T getData()
    {
        return this.data;
    }
}
